<?php
//キャッシュしたEntryをObjectとして扱うためのInterfaceを提供するために
//パターンを用いてクラス化している。
class Cache_Handler_Item {
	
	private $id;
	private $handler;//このアイテムを管理しているHandlerへの参照
	public function __construct($id, &$handler){
		$this->id = $id;
		$this->handler = $handler;
	}
	//HTTP/1.1メソッドを意識して名付けてみた
	public function get(){
		return $this->handler->getCache($this->id);
	}
	public function put($data){
		return $this->handler->setCache($this->id, $data);
	}
	public function set($data){
		//alias
		return $this->put($data);
	}
	public function delete(){
		return $this->handler->clear($this->id);
	}
	public function head(){
		return $this->handler->CacheModifiedSince($this->id);
	}
	public function isExists(){
		return $this->handler->isExists($this->id);
	}
	//alias to isExists()
	//この名前の方がソースの可読性が上がると思う
	public function available(){
		return $this->isExists();
	}
	//グループの扱い
	public function join( $group_name ){
		return $this->handler->setCacheGroupName( $this->id, $group_name );
	}
	
	//IDEAとして書いておく
	//public function touch(){
		////このIDの更新日付を最新化
	//}
}



