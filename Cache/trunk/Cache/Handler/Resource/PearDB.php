<?php
/** 
 *  Save cache data by Pear::DB
 * @package     Cache_Handler
 * @category    Cache
 */

require_once 'Cache/Handler/Resource/Interface.php';
require_once 'Cache/Handler/Resource/PDO.php';
require_once 'PEAR.php';
require_once 'DB.php';

class Cache_Handler_Resource_PearDB extends Cache_Handler_Resource_PDO  {

    /** 
    * constructor 
    * @access public 
    * @param array $ini array( "property name" => "value " );
    * @return void
    */
    public function __construct( $ini= array() )
    {
       $this->setDsn();
       foreach( $ini as $key => $val ){
            $this->$key = $val;
       }
       $this->initTable();
    }
    
    /** 
    * 
    * @access public 
    * @param  String dsn, used for DB::connect().
    * @return void
    */
    public function setDsn( $dsn = null )
    {
        if( $this->dsn == null &&  $dsn == null ){
            $temp = rtrim( str_replace( "\\", "/", $_SERVER["TEMP"] ), "/" );
            $dsn  = "sqlite:///".$temp."/cache_handler.sqlite3.db";
        }
        $this->dsn = $dsn;
    }
    /** 
    * initialize DB::common object
    * @access protected 
    * @param  void
    * @return void
    */
    protected function initDB()
    {
        if($this->db == null && $this->dsn != null && $this->dsn != "" ){
            $this->db = DB::connect($this->dsn, $this->options);
            if( PEAR::isError( $this->db ) ) {
                die( $this->db->getMessage());
            }
        }
        ////
        if( $this->db != null ){
            $this->db->setFetchMode( DB_FETCHMODE_ASSOC );
            return true;
        }else{
            return false;
        }
    }
    /** 
    * send sql to server and get response
    * @access private
    * @param  String SQL
    * @return boolean  true (succeeded) / false (faild)
    */
    protected function execSQL( $sql )
    {
        $this->initDB();
        if( $this->db != null ){
            $sql = str_replace( "\n", " ", $sql );
            $sql = str_replace( "\t", " ", $sql );
            $ret =  $this->db->query( $sql );
        }
        return $ret === DB_OK;
    }
    /** 
    * quote sql special chars
    * @access private
    */
    protected function escape( $data )
    {
        $data = $this->db->quoteSmart( $data );
        return $data;
    }

    /** 
    * get Cache data from DB
    * @access protected
    * @param $id    stirng cache id
    * @return string cached data, if cache does not exist, returns null
    */
    protected function _select( $id )
    {
        $id  = $this->escape( $id );
        $sql = "SELECT * FROM cache_data WHERE id = $id";
        $res = $this->db->query( $sql );
        if( PEAR::isError( $res ) ) {
            die( $this->db->getMessage());
        }else{
            return $res->fetchRow();
        }
    }
    /** 
    * return array of id , whtch included group
    * @access protected
    * @param  String $groupname Cache group name
    * @return array array of string.
    */
    protected function getIdsInGroup( $groupname )
    {
        $groupname = $this->escape( $groupname );
        //sql
        $sql .= "SELECT * FROM cache_group 
                    WHERE group_name = $groupname ;";
        $array = array();
        $res   = $this->db->query( $sql );
        while( $res->fetchInto( $row ) ){
            $array[] = $row["id"];
        }
        return $array;
    }

}


?>