<?php

/** 
 * manage multiple cache resource 
 * This Class select resource to store/retrive cache_data by cache_id
 *<code>
 * //example for multiple cache
 * $multi = new Cache_Handler_Resource_RoundRobin();
 * $multi->addResource( new Pear_Cache_Handler_Resource_MemCached($ini[0]),1 );
 * $multi->addResource( new Pear_Cache_Handler_Resource_MemCached($ini[1]),2 );
 * $multi->addResource( new Pear_Cache_Handler_Resource_MemCached($ini[2]),1 );
 * $multi->addResource( new Pear_Cache_Handler_Resource_MemCached($ini[3]),3 );
 * $multi->addResource( new Pear_Cache_Handler_Resource_MemCached($ini[4]),1 );
 * //add multile to handler
 * Cache_Handler::setResource( $multi );
 * //stores data
 * $handler = new Cache_Handler($id);
 * $handler->setCache( $data );
 *<code>
 * @package     Cache_Handler
 * @category    Cache
 */
require_once 'Cache/Handler/Resource/Interface.php';
require_once 'Cache/Handler/Resource/Multiple.php';


class Cache_Handler_Resource_RoundRobin implements Cache_Handler_Resource_Interface
{
	private $mapping   = array();
	private $resources = array();
    /** 
    * constructor
    * @access public
    * @param $ini array of string for config.
    */
    public function __construct( $ini )
    {
        foreach( $ini as $key => $val ){
            $this->$key = $val;
        }
    }
    /** 
    * add resource instance
    * @access public 
    * @param Cache_Handler_Resource resource
    * @param int Weigth 
    * @return
    */
    public function addResource( $resourceObj, $weight=1 )
    {
        $this->resources[] = $resourceObj;
        for( $i=0;$i<$weight;$i++){
        	$this->mapping[] = sizeof($this->resources);
        }
    }
    public function setResource( $resource = null )
    {
        if( is_array($resource) ){
            $this->resources = $resource;
        }else{
            $this->resources   = array();
            $this->addResource( $resource );
        }
    }
    public function getResource( $idx )
    {
        return $this->resources[$idx];
    }
    /**
    * use getTarget instead.
    */
    protected function _target( $id = null )
    {
	     $i = intval(rand( 0, sizeof($this->mapping)));
	     return $this->resource[$this->mapping[$i]];
    }
    /** 
    * Call by hander
    * @access public
    * @param $data  object cache data
    * @param $id    stirng cache id
    * @param $group array  Array of String. group name of this cache 
    * @return boolean  true (succeeded) / false (faild)
    */
    public function save( $data, $id, $group = null )
    {
    	foreach( $this->resources as $idx => $res ){
        	$ret[] = $this->resources[$idx]->save( $data, $id, $group );
    	}
    	return !in_array( $ret, false, true );
    }
    /** 
    * Call by hander
    * @access public
    * @param $id    stirng cache id
    * @return boolean  true (succeeded) / false (faild)
    */
    public function remove( $id )
    {
    	foreach( $this->resources as $idx => $res ){
        	$ret[] = $this->resources[$idx]->remove( $id );
    	}
    	return !in_array( $ret, false, true );
    }
    /** 
    * Call by hander
    * without group name, clean all chache
    * @access public
    * @param  String $group    stirng cache group name
    * @param  Array  $ids array of id removed.
    * @return boolean  true (succeeded) / false (faild)
    */
    public function clean( $group =null, &$ids = null )
    {
    	foreach( $this->resources as $idx => $res ){
            $ret[] = $this->resources[$idx]->clean( $group, &$ids );
    	}
    	return !in_array( $ret, false, true );
    }
    /** 
    * Call by hander
    * definition of get method
    * @access public
    * @param $id    stirng cache id
    * @return string cached data, if cache does not exist, returns null
    */
    public function get( $id )
    {
        return $this->resources[$this->_target()]->get( $id );
    }
    /** 
    * Call by hander.
    * get Cache created time from this resource.
    * @access public
    * @param $id    stirng cache id
    * @return string cache created time
    */
    public function getLastModified( $id )
    {
        return $this->resources[$this->_target()]->getLastModified( $id );
    }
    /** 
    * Call by hander.
    * Set or update  Cache created(modified) time.
    * @access public
    * @param $id    stirng cache id
    * @param $date  stirng cache created time. default value date("r")
    * @return String date of cache create or modified time
    */
    public function setLastModified( $id, $date )
    {
    	foreach( $this->resources as $idx => $res ){
            $ret[] = $this->resources[$idx]->setLastModified( $id, $date );
    	}
    	return !in_array( $ret, false, true );
    }
    /** 
    * Call by hander.
    * set or update cache TTL for checking expiration.
    * @access public
    * @param $id    stirng cache id
    * @param $int   int    cache lifetime , default -1
    * @return boolean  true (succeeded) / false (faild)
    */
    public function setTimeToLive( $id, $int = null )
    {
    	foreach( $this->resources as $idx => $res ){
            $ret[] = $this->resources[$idx]->setTimeToLive( $id, $int );
    	}
    	return !in_array( $ret, false, true );
    }
    /** 
    * Call by hander.
    * get cache Life time for checking expiration.
    * @access public
    * @param $id    stirng cache id
    * @return int   cache lifetime. if value does not exits , this method return -1.
    */
    public function getTimeToLive( $id )
    {
        return $this->resources[$this->_target()]->getTimeToLive( $id );
    }
    /** 
    * remove cache id ( arg $id ) from group index
    * @access public
    * @param  String $id cache id
    * @return boolean  true (succeeded) / false (faild)
    */
    public function removeFromGroup( $id, $group = null )
    {
    	foreach( $this->resources as $idx => $res ){
            $ret[] = $this->resources[$idx]->removeFromGroup( $id, $group);
    	}
    	return !in_array( $ret, false, true );
    }
    /**
    * Call by hander.
    * definition of joinGroup method.
    * Add $id to cache group $group
    * @access public
    * @param $id    stirng cache id
    * @param $group stirng cache group name
    * @return boolean  true (succeeded) / false (faild)
    */
    public function addIntoGroup( $id, $group )
    {
    	foreach( $this->resources as $idx => $res ){
            $ret[] = $this->resources[$idx]->addIntoGroup( $id, $group );
    	}
    	return !in_array( $ret, false, true );
    }
	
}

//1,2,3,4,5 = 15
//
//5/15 = 1/3
//3/15 = 1/5
//
//優先度が低いほど順番が回ってこないようにする
//
//$id = roundRobin();//
//	
//	function roundRobin(){
//		return ;
//	}
//	
/////////////////////////////
////10個に均等に配布する場合
/////////////////////////////
//$num = intval( rand() % 10 );
//
//////////////////////////////
////2個に重みを１：２で配布する
/////////////////////////////
//$num = intval( rand() % ( 1 + 2 ) );// 0,1,2
//
//
//比例配分とは、一定のVolumeを指定された比率に配分する機能である。
//一定のVolumenが無限大の場合、各要素の比率配分の集合と考える
//
//
//ある関数は9回Callされると、1/3の確率で 0　になり、 2/3の確率で1になる。
//
//	
//
//
//もしくは一定のvolumeを仮定する
//
//
//
?>

