<?php

/** 
 * manage multiple cache resource for load balanced caching system.
 * it will be useful for session save handler.
 * This Class select resource to store/retrive cache_data by cache_id
 *<code>
 * //example for multiple cache
 * $multi = new Pear_Cache_Handler_Resource_Multiple();
 * $multi->addResource( new Pear_Cache_Handler_Resource_MemCached($ini[0]) );
 * $multi->addResource( new Pear_Cache_Handler_Resource_MemCached($ini[1]) );
 * $multi->addResource( new Pear_Cache_Handler_Resource_MemCached($ini[2]) );
 * $multi->addResource( new Pear_Cache_Handler_Resource_MemCached($ini[3]) );
 * $multi->addResource( new Pear_Cache_Handler_Resource_MemCached($ini[4]) );
 * //add multile to handler
 * $handler = new Pear_Cache_Handler($id);
 * $handler->setResource( $multi );
 * $handler->setCache( $data );
 *<code>
 * @package     Cache_Handler
 * @category    Cache
 */

require_once 'Cache/Handler/Resource/Interface.php';

class Cache_Handler_Resource_Multiple implements Cache_Handler_Resource_Interface {
    
    /** 
    * constructor
    * @access public
    * @param $ini array of string for config.
    */
    public function __construct( $ini )
    {
        foreach( $ini as $key => $val ){
            $this->$key = $val;
        }
    }
    /** 
    * add resource instance
    * @access public 
    * @param Cache_Handler_Resource resource
    * @return
    */
    public function addResource( $resource )
    {
        $this->resources[] = $resource;
    }
    public function setResource( $resource = null )
    {
        if( is_array($resource) ){
            $this->resources = $resource;
        }else{
            $this->resources   = array();
            $this->addResource( $resource );
        }
    }
    public function getResource( $idx )
    {
        return $this->resources[$idx];
    }
    /**
    * Desperated 
    * md5化されたIDを綺麗に分散できない致命的なバグ
    * use getTarget instead.
    */
    protected function _target( $id = null )
    {
        //新しい実装
        $r   = 1;
        $str = strval(hexdec(md5($id)));
        $str = substr( $str , 0, strpos( $str, "+" )-1 );
        $str = str_replace ( ".", "", $str );
        $r   = intval($str) / sizeof($this->resources);
        return $r;
        //古い実装
        //if( $this->resource_id == null ){
        //    $hash = 0;
        //    for( $i=0; $i < sizeof( $id ); $i++ ){
        //        $hash = (int)(($hash*33)+ord($id[$i])) & 0x7fffffff;
        //    }
        //    $this->resource_id = $hash % sizeof($this->resources);
        //}
        //return $this->resource_id;
    }

    /** 
    * Call by hander
    * @access public
    * @param $data  object cache data
    * @param $id    stirng cache id
    * @param $group array  Array of String. group name of this cache 
    * @return boolean  true (succeeded) / false (faild)
    */
    public function save( $data, $id, $group = null )
    {
        return $this->resources[$this->_target($id)]->save( $data, $id, $group );
    }
    /** 
    * Call by hander
    * @access public
    * @param $id    stirng cache id
    * @return boolean  true (succeeded) / false (faild)
    */
    public function remove( $id )
    {
        return $this->resources[$this->_target($id)]->remove( $id );
    }
    /** 
    * Call by hander
    * without group name, clean all chache
    * @access public
    * @param  String $group    stirng cache group name
    * @param  Array  $ids array of id removed.
    * @return boolean  true (succeeded) / false (faild)
    */
    public function clean( $group =null, &$ids = null )
    {
        foreach( $this->resources as $resource ){
            $ret[] = $resource->clean( $group, &$ids );
        }
        $bool = false;
        foreach( $ret as $b ){
            $bool = $bool || $b;
        }
        return $bool;
    }
    /** 
    * Call by hander
    * definition of get method
    * @access public
    * @param $id    stirng cache id
    * @return string cached data, if cache does not exist, returns null
    */
    public function get( $id )
    {
        return $this->resources[$this->_target($id)]->get( $id );
    }
    /** 
    * Call by hander.
    * get Cache created time from this resource.
    * @access public
    * @param $id    stirng cache id
    * @return string cache created time
    */
    public function getLastModified( $id )
    {
        return $this->resources[$this->_target($id)]->getLastModified( $id );
    }
    /** 
    * Call by hander.
    * Set or update  Cache created(modified) time.
    * @access public
    * @param $id    stirng cache id
    * @param $date  stirng cache created time. default value date("r")
    * @return String date of cache create or modified time
    */
    public function setLastModified( $id, $date )
    {
        return $this->resources[$this->_target($id)]->setLastModified($id, $date);
    }
    /** 
    * Call by hander.
    * set or update cache TTL for checking expiration.
    * @access public
    * @param $id    stirng cache id
    * @param $int   int    cache lifetime , default -1
    * @return boolean  true (succeeded) / false (faild)
    */
    public function setTimeToLive( $id, $int = null )
    {
        return $this->resources[$this->_target($id)]->setTimeToLive( $id, $int );
    }
    /** 
    * Call by hander.
    * get cache Life time for checking expiration.
    * @access public
    * @param $id    stirng cache id
    * @return int   cache lifetime. if value does not exits , this method return -1.
    */
    public function getTimeToLive( $id )
    {
        return $this->resources[$this->_target($id)]->getTimeToLive( $id );
    }
    /** 
    * remove cache id ( arg $id ) from group index
    * @access public
    * @param  String $id cache id
    * @return boolean  true (succeeded) / false (faild)
    */
    public function removeFromGroup( $id, $group = null ){
        return $this->resources[$this->_taeget( $id )]->removeFromGroup( $id );
    }
    /**
    * Call by hander.
    * definition of joinGroup method.
    * Add $id to cache group $group
    * @access public
    * @param $id    stirng cache id
    * @param $group stirng cache group name
    * @return boolean  true (succeeded) / false (faild)
    */
    public function addIntoGroup( $id, $group ){
        return $this->resources[$this->_target( $id )]->addIntoGroup( $id, $group );
    }
}
