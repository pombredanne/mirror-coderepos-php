<?php
/** 
 *  Save cache data by MDB2
 * @package     Cache_Handler
 * @category    Cache
 */

require_once 'Cache/Handler/Resource/Interface.php';
require_once 'Cache/Handler/Resource/PDO.php';
require_once 'PEAR.php';
require_once 'MDB2.php';

class Cache_Handler_Resource_MDB2 extends Cache_Handler_Resource_PDO {

    /** 
    * constructor 
    * @access public 
    * @param array $ini array( "property name" => "value " );
    * @return void
    */
    public function __construct( $ini= array() )
    {
        $this->setDsn();
        foreach( $ini as $key => $val ){
            $this->$key = $val;
        }
        $this->initTable();
    }
    
    /** 
    * 
    * @access public 
    * @param  String dsn, used for new MDB().
    * @return void
    */
    public function setDsn( $dsn = null )
    {
        if( $this->dsn == null &&  $dsn == null ){
            $temp = rtrim( str_replace( "\\", "/", $_SERVER["TEMP"] ), "/" );
            $dsn  = "sqlite:///".$temp."/cache_handler.sqlite3.mdb2";
        }
        $this->dsn = $dsn;
    }
    
    /** 
    * initialize PDO object
    * @access protected 
    * @param  void
    * @return void
    */
    protected function initDB()
    {
        if($this->mdb2 == null && $this->dsn != null && $this->dsn != "" ){
            $this->mdb2 = MDB2::factory($this->dsn, $this->options);
            if( PEAR::isError( $this->mdb2 ) ) {
                die( $this->mdb2->getMessage() );
            }
        }
        ////
        if( $this->mdb2 != null ){
            $this->mdb2->setFetchMode( MDB2_FETCHMODE_ASSOC );
            return true;
        }else{
            return false;
        }
    }
   
    /** 
    * send sql to server and get response
    * @access private
    * @param  String SQL
    * @return boolean  true (succeeded) / false (faild)
    */
    protected function execSQL( $sql )
    {
        $this->initDB();
        if( $this->mdb2 != null ){
            $sql = str_replace( "\r", " ", $sql );
            $sql = str_replace( "\n", " ", $sql );
            $sql = str_replace( "\t", " ", $sql );
            $ret =  $this->mdb2->exec( $sql );
        }
        if (PEAR::isError($ret)) {
            return false;
        }
        return $ret > 0;
    }
    /** 
    * quote sql special chars
    * @access private
    */
    protected function escape( $data )
    {
        $data = $this->mdb2->quote( $data );
        return $data;
    }
    /** 
    * get Cache data from PDO
    * @access protected
    * @param $id    stirng cache id
    * @return string cached data, if cache does not exist, returns null
    */
    protected function _select( $id )
    {
        $_id = $this->escape( $id );
        $sql = "SELECT * FROM cache_data WHERE id = $_id";
        $res = $this->mdb2->query( $sql );
        if (PEAR::isError($res)) {
            return null;
        }else{
            return $row = $res->fetchRow();
        }
    }
    /** 
    * return array of id , whtch included group
    * @access protected
    * @param  String $groupname Cache group name
    * @return array array of string.
    */
    protected function getIdsInGroup( $groupname )
    {
        $groupname = $this->escape( $groupname );
        $sql   .= "SELECT * FROM cache_group WHERE group_name = $groupname ;";
        $array  = array();
        $res    = $this->mdb2->query( $sql );
        while( ( $row = $res->fetchRow() )!= null ){
            $array[] = $row["id"];
        }
        return $array;
    }
}
?>