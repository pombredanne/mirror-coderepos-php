<?php

/** 
 * Resource Interface definition for Cache_Handler
 * ここに書いているメソッドはどうやっても実装しておく。
 * メソッドの動作定義がわからなくなったときのため
 * なにかメソッドを追加したら必ずここを更新する。
 * @package     Cache_Handler
 * @category    Cache
 */

interface Cache_Handler_Resource_Interface
{
    
    /** 
    * Call by hander
    * definition of save method
    * @access public
    * @param $data  object cache data
    * @param $id    stirng cache id
    * @param $group array  Array of String. group name of this cache 
    * @return boolean  true (succeeded) / false (faild)
    */
    public function save(  $data, $id, $group = null );
    /** 
    * Call by hander
    * definition of remove method
    * @access public
    * @param $id    stirng cache id
    * @return boolean  true (succeeded) / false (faild)
    */
    public function remove( $id );
    /** 
    * Call by hander
    * definition of clean method
    * without group name, clean all chache
    * @access public
    * @param  String $group    stirng cache group name
    * @param  Array  $ids array of id removed.
    * @return boolean  true (succeeded) / false (faild)
    */
    public function clean( $group =null, &$ids = null );
    /** 
    * Call by hander
    * definition of get method
    * get Cache data from this resource
    * @access public
    * @param $id    stirng cache id
    * @return string cached data, if cache does not exist, returns null
    */
    public function get( $id );
    /** 
    * Call by hander.
    * definition of getLastModified method.
    * get Cache created time from this resource.
    * @access public
    * @param $id    stirng cache id
    * @return string cache created time
    */
    public function getLastModified( $id );
    /** 
    * Call by hander.
    * Definition of getLastModified method.
    * Set or update  Cache created(modified) time.
    * @access public
    * @param $id    stirng cache id
    * @param $date  stirng cache created time. default value date("r")
    * @return String date of cache create or modified time
    */
    public function setLastModified( $id, $date );
  ///** 
  //* Call by hander.
  //* definition of isExists method.
  //* check cache exists.
  //* @access public
  //* @param $id    stirng cache id
  //* @return boolean  true (exists) / false (not exists)
  //*/
  //public function isExists( $id );
 // 
    /** 
    * Call by hander.
    * definition of setTimeToLive method.
    * set or update cache TTL for checking expiration.
    * @access public
    * @param $id    stirng cache id
    * @param $int   int    cache lifetime , default -1
    * @return boolean  true (succeeded) / false (faild)
    */
    public function setTimeToLive( $id, $int = -1 );
    /** 
    * Call by hander.
    * definition of getTimeToLive method.
    * get cache Life time for checking expiration.
    * @access public
    * @param $id    stirng cache id
    * @return int   cache lifetime. if value does not exits , this method return -1.
    */
    public function getTimeToLive( $id );
    /** 
    * Call by hander.
    * definition of leaveGroup method.
    * this function make id leave from Cache group.
    * @access public
    * @param $id    stirng cache id
    * @param $group stirng cache group name
    * @return boolean  true (succeeded) / false (faild)
    */
    public function removeFromGroup( $id, $group = null );
    /** 
    * Call by hander.
    * definition of joinGroup method.
    * Add $id to cache group $group
    * @access public
    * @param $id    stirng cache id
    * @param $group stirng cache group name
    * @return boolean  true (succeeded) / false (faild)
    */
    public function addIntoGroup( $id, $group );
    
    /**
     * Call by Handler.
     * definition function.
     * @access public
     * @param null
     * @return Array of String 
     */
    public function getGroupNameList();
}
