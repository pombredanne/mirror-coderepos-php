<?php
/**
 * Plugin for Cache_Handler 
 * this would be useful for object to cache
 * @package     Cache_Handler
 * @category    Cache
 */

require_once 'Cache/Handler/PlugIn/Interface.php';

//Serializeする代わりにソースコードで保存する
class Cache_Handler_PlugIn_SrcCode implements Cache_Handler_PlugIn_Interface {
    /** 
    * @access public
    */
    public function __construct()
    {
        //do nothing
    }
    /** 
    * call by Handler
    * @access plubic 
    * @param  Object cache data object to store
    * @return Stirng serialized  cache data
    */
    public function beforeWrite( $data )
    {
		return var_export($data,true);
    }
    /** 
    * call by Handler
    * @access plubic 
    * @param  String Stirng serialized  cache data
    * @return Object deserialized cache data object
    */
    public function afterRead( $data )
    {
		return $this->un_var_export($data);
    }
    /**
     * クラスのインスタンスはvar_exportできれいにExportされないので、追加処理
     * PHPで動的にメソッド追加するには、php_runkitモジュールを使うことになる。
     * モジュールがない場合はクラスのvar_exportはできない
     * 連想配列と配列はきれいにソースコード似なるので実行が出来る
     * Classの再実行を検討中
     */
	public function un_var_export($str){
		try{
			eval( '$aa= '.$str.';');
			return $aa;
		}catch( Exception $e){
			@dl ( "php_classkit" );
			if( function_exists("classkit_method_add")){
				$_buff = explode ( ":" , $str, 2 );
				$class_name = $_buff[0];
				$obj = new $class_name();
				$vars_str = explode("\n",$str);
				array_shift($vars_str);
				array_pop($vars_str);
				classkit_method_add( $class_name, "__set_state", '$props','
					foreach($props as $key=>$val){
					$this->$key = $val;
					}');
				return $obj;
			}else{
				return $str;
			}
		}

	}
}


?>