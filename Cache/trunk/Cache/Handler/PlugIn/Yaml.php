<?php
/**
 * Plugin for Cache_Handler 
 * this would be useful for object to cache
 * @package     Cache_Handler
 * @category    Cache
 */

require_once 'Cache/Handler/PlugIn/Interface.php';

//Serializeする代わりにYAMLで保存する
class Cache_Handler_PlugIn_Yaml implements Cache_Handler_PlugIn_Interface {
    /** 
    * @access public
    */
    public function __construct()
    {
    	//Yamlパーサーを選択する
    	//TODO:YAMLのパーサーを選択するロジック
    }
    /** 
    * call by Handler
    * @access plubic 
    * @param  Object cache data object to store
    * @return Stirng serialized  cache data
    */
    public function beforeWrite( $data )
    {
		return  Spyc::YAMLDump($array);
    }
    /** 
    * call by Handler
    * @access plubic 
    * @param  String Stirng serialized  cache data
    * @return Object deserialized cache data object
    */
    public function afterRead( $data )
    {
		return  Spyc::YAMLLoad($data);
    }

}


?>