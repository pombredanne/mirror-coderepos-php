<?php
/**
 * Plugin for Cache_Handler
 * This would be useful for saving strage size
 * 
 * @package     Cache_Handler
 * @category    Cache
 */

require_once 'Cache/Handler/PlugIn/Interface.php';

class Cache_Handler_PlugIn_Deflate implements Cache_Handler_PlugIn_Interface{
    
    /** 
    * compression level
    * @access public 
    */
    public $level;
    /** 
    * Deflate compresion plugin
    * @access public
    * @param int $level compression lebel�@default 9
    */
    public function __construct( $level = 9 )
    {
        $this->level = $level;
    }
    /** 
    * call by Handler
    * @access plubic 
    * @param  String cache data
    * @return Stirng compressed cache data
    */
    public function beforeWrite( $data )
    {
        return gzdeflate( $data, $this->level );
    }
    /** 
    * call by Handler
    * @access plubic 
    * @param  String compressed cache data
    * @return Stirng cache data
    */
    public function afterRead( $data )
    {
        return gzinflate( $data );
    }


}