<?php
//2008/01/25 3:42 by takuya_1st
//関数の実行結果をキャッシュする
//メソッドの結果をキャッシュするクラス
//動作の遅い関数の結果を保存しておく。
//たとえばfile_get_contentsやSpycの結果をキャッシュするのに使うと良い
//
//TODO:オブジェクトのメソッドのキャッシュに未対応
//オブジェクトのメソッドを実行するとオブジェクトの状態そのものが変化する可能性があるので
class Cache_Handler_CachedMethod
{
	protected	$handler;
	public function __construct(){
		$this->handler = $this->handler();
	}
	public function call_func($methodname, $args)
	{
		//TODO:ID_generatorを使うように変更する。
		$str = $methodname.var_export($args,true);
		$id  = md5($str);
		$val = $this->load($id,$methodname);
		if($val){
			return $val;
		}else{
			$val = call_user_func_array($methodname, $args);
			$this->save($id,$methodname,$val);
			return $val;
		}
	}
	//delegation
	public function handler(){
		if($this->handler){
			return $this->$handler;
		}else{
			return $this->defaultHandler();
		}
	}
	public function defaultHandler(){
		require_once "Cache/Handler.php";
		require_once "Cache/Handler/Resource/File.php";
		$handler = new Cache_Handler();
		$handler->setResource(new Cache_Handler_Resource_File());
		return $handler;
	}
	public function setHandler($handler_obj){
		$this->$handler = $handler_obj;
	}
	protected function save($id,$methodname,$return_value){
		return $this->handler->setCache($id,$return_value,$methodname);
	}
	protected function load($id,$methodname){
		return $this->handler->getCache($id,$methodname);
	}
	public function __call($name,$args){
		if( !method_exists( $this, $name ) ){
			return $this->call_func($name, $args);
		}
	}
	//public function addObj( $obj, $name=null ){
		//$this->obj = $this->obj ? $this->obj : array();
		//if( isset($name) ){
			//$this->obj[$name] &= $obj;
		//}else{
			//$this->obj[] &= $obj;
		//}
		//return sizeof( $this->obj )-1;
	//}
}
/////////TEST
if (  __FILE__ == $_SERVER["PHP_SELF"] ){
//include_pathの解決
$ini_name = "include_path";
ini_set ( $ini_name, implode(";", array_merge(array("../",""), explode( ";", ini_get($ini_name)))));
//利用テスト
$cached = new Cache_Handler_CachedMethod();
$ret[] = $cached->date("Y/m/d");//date関数の結果をキャッシュに保存
$ret[] = $cached->uniqid();//uniqid関数の結果をキャッシュに保存
$ret[] = $cached->file_get_contents("http://www.yahoo.co.jp");//file_get_contentsの結果をキャッシュに保存
var_export($ret);
echo "\n";
echo "EOF";

}
?>