<?php
///
///for simple usage 
///
///APIが複雑化してきたので簡単に使えるキャッシュ関数を作成
///
///2008/01/25 3:43 by takuya_1st
///
/// 特にオブジェクトの構造を知らなくても、
/// 関数を呼び出すだけでキャッシュできる。
/// オブジェクト生成が面倒臭いときに便利
///
///	cache_put_contents関数：キャッシュを保存する関数
///	cache_get_contents関数：キャッシュを取り出す関数
///
//require_once "Cache/Handler.php";
//require_once "Cache/Handler/Resource/File.php";
//TODO:File.php以外も使えるようにする。
function cache_get_contents($id, $path=null){
	//モジュールを確認
	require_once "Cache/Handler.php";
	require_once "Cache/Handler/Resource/File.php";
	//処理
	$path = $path ? $path : $_SERVER["TMP"];
	$handler = new Cache_Handler();
	$handler->setResource(new Cache_Handler_Resource_File( array("path"=>$path)));
	return $handler->getCache($id);;
}

function cache_put_contents($id, $data, $path=null){
	//モジュールを確認
	require_once "Cache/Handler.php";
	require_once "Cache/Handler/Resource/File.php";
	//処理
	$path = $path ? $path : $_SERVER["TMP"];
	$handler = new Cache_Handler();
	$handler->setResource(new Cache_Handler_Resource_File(array("path"=>$path)));
	return $handler->setCache($id,$data);;
}

/////////TEST
if (  __FILE__ == $_SERVER["PHP_SELF"] ){
//include_pathの解決
$ini_name = "include_path";
ini_set ( $ini_name, implode(";", array_merge(array("..",""), explode( ";", ini_get($ini_name)))));
//使用例
cache_put_contents( "sample_id", "test_data" );
$ret = cache_get_contents("sample_id");
echo $ret;
echo "\n";
echo "EOF";
}
