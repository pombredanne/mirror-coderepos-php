<?php
$path = 'C:\ほにゃらら'; //Firefox　sqliteへのパス
require_once 'Zend/Loader.php';
Zend_Loader::registerAutoload();

$cache = Zend_Cache::factory('Core', 'File',
                            array('lifetime' => 86400,
    							  'automatic_serialization' => true),
                            array('cache_dir' => '.'));

function twitterid($var)
{
    return trim(parse_url($var, PHP_URL_PATH), '/');
}

try {
    if(!$following = $cache->load('following')) {
        $client = new Zend_Http_Client('http://twitter.com/sasezaki');
        $following = $client->request();
        $cache->save($following, 'following');
    }
    
    $scraper = new Diggin_Scraper();
    $scraper->process('//div[@id="friends"]//a', 'list[] => @href, twitterid')
            ->scrape($following, 'http://twitter.com/sasezaki');
    //Zend_Debug::dump($scraper->list);

      //wassr
      $url = 'http://wassr.jp/';
      $cookieJar = Diggin_Http_CookieJar_Loader_Firefox3::load($path, $url);
      $client = new Zend_Http_Client();
      $client->setCookieJar($cookieJar);
      
      $hidden = new Diggin_Scraper_Process();
      $hidden->addProcess('.//input[@name="CSRFPROTECT"]', 'CSRFPROTECT => @value')
             ->addProcess('.//input[@name="user_rid"]', 'user => @value');
      
      foreach ($scraper->list as $list) {
          
          try {
              $scraper = new Diggin_Scraper();
              $scraper->setHttpClient($client);
              $scraper->process('//form[@class="followbutton"]', array('post' => $hidden))
                      ->scrape("http://wassr.jp/user/$list");
                                 
                  
              $client->setParameterPost('CSRFPROTECT', $scraper->post['CSRFPROTECT']);
              $client->setParameterPost('user_rid', $scraper->post['user']);
          
              $client->setUri('http://wassr.jp/friend/add');
              $client->request('POST');
          } catch (Diggin_Scraper_Strategy_Exception $e){
              echo $list .' is not join wassr or you had KOUDOKU';
              
          }
      }

} catch (Diggin_Http_Exception $e) {
    die($e->getMessage());
} catch (Zend_Exception $e) {
    die($e->getMessage());    
}
