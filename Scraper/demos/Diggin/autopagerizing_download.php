<?php
/**
 * Diggin_Service_Wedata::getItems('AutoPagerize')とDiggin_Scraperで
 * 複数ページにまたがるサイトを保存する。
 * 
 * 関連：
 * WWW::Mechanize::AutoPager Web::Scraperで複数ページにまたがるデータを扱う 
 * http://labs.gmo.jp/blog/ku/2008/02/wwwmechanizeautopagerwebscraper.html
 */

//ターゲットのURL
$url = 'http://d.hatena.ne.jp/sasezaki';

$pageNum = 20; //最大ページ保存数
$cahe_dir = './temp/'; // wedataのキャッシュファイルを書き込むディレクトリ
$file_dir = './file/'; //ページを保存するディレクトリ

//////////////
require_once 'Diggin/Service/Wedata.php';
require_once 'Diggin/Scraper.php';
require_once 'Zend/Cache.php';
require_once 'Zend/Debug.php';

$frontendOptions = array(
    'lifetime' => 86400, // キャッシュの有効期限を 24 時間とします
    'automatic_serialization' => true,
);
$backendOptions = array(
    'cache_dir' => $cahe_dir // キャッシュファイルを書き込むディレクトリ
);

/**
 * Get next url from wedata('AutoPagerize')
 *
 * @param array $items
 * @param string $url base url
 * @return void
 */
function getNextlink($items, $url) {
    foreach ($items as $item) {
    $pattern = '#'.$item['data']['url'].'#i';
        //hAtom 対策
        if ('^https?://.' != $item['data']['url'] && (preg_match($pattern, $url) == 1)) {
            $nextLink = $item['data']['nextLink'];
            return $nextLink;
        }
    }
    
    return false;
}


//main
$cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
if(!$items = $cache->load('wedata_items')) {
    $items = Diggin_Service_Wedata::getItems('AutoPagerize');
    $cache->save($items, 'wedata_items');
}
$nextLink = getNextlink($items, $url);
if($nextLink === false) {
    Zend_Debug::dump('not found from wedata with url:'.$url);
    exit;
}

for ($i = 1; $i <= $pageNum; $i++) {

    Zend_Debug::dump('dowload start url:'.$url);
    
    $client = new Zend_Http_Client();
    $client->setUri($url);
    $client->setConfig(array('storeresponse' => false));

    $response = $client->request();
    file_put_contents($file_dir.rawurlencode($url).'_'.$i.'.html', $response->getBody());

    Zend_Debug::dump('dowload end url:'.$url);
    
    $scraper = new Diggin_Scraper();
    $scraper->setUrl($url);
    $scraper->process($nextLink, 'nextLink => "@href"')
            ->scrape($response);

    if(count($scraper->results['nextLink']) === 0) {
        Zend_Debug::dump('next page not found');
        exit;
    }
    $url = array_shift($scraper->results['nextLink']);
    Zend_Debug::dump('next link is found! scraper found:'.$url);

    sleep(5);
}