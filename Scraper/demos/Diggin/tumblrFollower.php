<?php
require_once 'Zend/Http/Client.php';
$client = new Zend_Http_Client();
$client->setCookieJar();
$client->setUri('http://www.tumblr.com/login');
$client->setParameterPost('email','youraccount@gmail.com');
$client->setParameterPost('password','yourpass');
$client->request('POST'); 
$client->resetParameters();

require_once 'Diggin/Scraper.php';
$scraper = new Diggin_Scraper();
$scraper->setHttpClient($client);
$scraper->process('//a[@href="/followers"]', 'follwers => ["TEXT", "Int"]')
        ->scrape('http://www.tumblr.com/following');
print_r($scraper->results);


/*
Array
(
    [follwers] => 10
)
 * /