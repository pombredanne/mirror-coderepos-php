<?php
// フィルタ名の前のプレフィクスで
//「*」でフィルタ後と一致するもの　
//「！」でフィルタ後と一致しないもの

///////////////////////////////////////////////////
//function getusername($var)
//{
//    return trim(parse_url($var, PHP_URL_PATH), '/');
//}
//require_once 'Diggin/Scraper.php';
//$scraper = new Diggin_Scraper();
//$scraper->process('//div[@class="post-content"]/ul/li/a', 'phper => ["@href", "getusername", "!Alpha"]') //
//        ->scrape('http://www.1x1.jp/blog/2008/05/twitter_japanese_phper.html');
//var_dump($scraper->results['phper']);
//array(5) {
//  [0]=>
//  string(6) "hiro_y"
//  [1]=>
//  string(8) "ichii386"
//  [2]=>
//  string(15) "masaki_fujimoto"
//  [3]=>
//  string(6) "p4life"
//  [4]=>
//  string(7) "shin1x1"
//}

function getusername($var)
{
    return trim(parse_url($var, PHP_URL_PATH), '/');
}
require_once 'Diggin/Scraper.php';
$scraper = new Diggin_Scraper();
$scraper->process('//div[@class="post-content"]/ul/li/a', 'phper[] => ["@href", "getusername", "*Alpha"]')
        ->scrape('http://www.1x1.jp/blog/2008/05/twitter_japanese_phper.html');
var_dump($scraper->results['phper']);

//array(33) {
//  [0]=>
//  string(4) "LIND"
//  [1]=>
//  string(6) "akiyan"
//  [2]=>
//  string(3) "bto"
//  [3]=>
//  string(10) "cocoitiban"
//  [4]=>
//  string(3) "elf"
//  [5]=>
//  string(4) "halt"
//  [6]=>
//  string(3) "hnw"
//  [7]=>
//  string(4) "iogi"
//  [8]=>
//  string(6) "iteman"
//  [9]=>
//  string(5) "junya"
//  [10]=>
//  string(6) "kensuu"
//  [11]=>
//  string(8) "komagata"
//  [12]=>
//  string(7) "koyhoge"
//  [13]=>
//  string(7) "kumatch"
//  [14]=>
//  string(5) "kunit"
//  [15]=>
//  string(8) "masugata"
//  [16]=>
//  string(8) "memokami"
//  [17]=>
//  string(9) "moriyoshi"
//  [18]=>
//  string(6) "mumumu"
//  [19]=>
//  string(8) "nowelium"
//  [20]=>
//  string(5) "rhaco"
//  [21]=>
//  string(4) "rsky"
//  [22]=>
//  string(8) "shigepon"
//  [23]=>
//  string(8) "shimooka"
//  [24]=>
//  string(5) "shoma"
//  [25]=>
//  string(7) "sotarok"
//  [26]=>
//  string(6) "suzuki"
//  [27]=>
//  string(6) "takagi"
//  [28]=>
//  string(9) "tsukimiya"
//  [29]=>
//  string(5) "yando"
//  [30]=>
//  string(7) "yohgaki"
//  [31]=>
//  string(8) "yonekawa"
//  [32]=>
//  string(7) "yudoufu"
//}




