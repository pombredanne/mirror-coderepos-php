<?php
class Diggin_Scraper_FindHelper extends Diggin_Scraper_FindHelper_Abstract
{
    public function __call($name, $args)
    {
        if (substr($name, -1) === 's') {
            parent::__call($name, $args);
        }
    }
}