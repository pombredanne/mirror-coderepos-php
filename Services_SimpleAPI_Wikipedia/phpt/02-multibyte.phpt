--TEST--
verify test 2
--FILE--
<?php
require_once dirname(dirname(__FILE__)) . '/Services/SimpleAPI/Wikipedia.php';

$wikipedia = new Services_SimpleAPI_Wikipedia();
$results = $wikipedia->api('鳥肌実');
if (count($results)) {
    print('ok');
} else {
    print('fail');
}
?>
--EXPECT--
ok
