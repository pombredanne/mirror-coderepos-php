<?php
/**
 * @see http://labs.uechoco.com/blog/2008/05/phppeardate_holidays_driversetyear%e3%81%8c%e5%a4%89.html
 */
/**
 * Date_Holidays_Driver_Japanseを用いて
 * 祝日名を取得する(挙動がおかしい)
 *
 * 2008/05/11
 *
 * Author:   uechoco
 * 動作確認: PHP 5.2.5
 * require:  Date_Holidays (推奨バージョン:0.18.0)
 * require:  Date_Holidays_Japan 0.2.0
 *
 */
// Date_Holidaysクラスの読み込み
require_once "Date/Holidays.php";
// 言語ファイルの読み込み(PEARのdataディレクトリ内)
define('LANG_FILE', '/usr/local/lib/php5/pear/data/Date_Holidays_Japan/lang');
// Date_Holidaysオブジェクトの生成(西暦を省略すると現在の西暦になる)
$obj =& Date_Holidays::factory('Japan');
if (Date_Holidays::isError($obj)) {
    die('Factory was unable to produce driver-object');
}
// 翻訳ファイルをロケール=ja_JPとして登録
$obj->addTranslationFile(LANG_FILE . '/Japan/ja_JP.xml', 'ja_JP');
if (Date_Holidays::isError($obj)) {
    die($obj->getMessage());
}
$lastYear = 0;
$beginTime = mktime(0, 0, 0, 1, 1, 2008);
for ($i = 0; $i <731; $i++) {
    $t = $beginTime + $i * 86400;
    $d = strftime('%Y-%m-%d', $t);
    // 西暦が更新されたら祝日の再構成
    $y = strftime('%Y', $t);
    if ($lastYear != $y) {
        $obj->setYear(intval($y));
        $lastYear = $y;
        //デバッグ用にオブジェクトを直接吐く
        //echo "<hr /><font color='red'>\n\n";
        //print_r($obj->_internalNames);
        //echo "</font><hr />\n\n";
    }
    $holiday = $obj->getHolidayForDate($d, 'ja_JP');
    if (!is_null($holiday))
        printHolidayInfo($holiday, $d);
}
function printHolidayInfo($holiday, $date = '')
{
    echo $date . " =========<br />\n";
    printf("名前：%s<br />\n", $holiday->getTitle());
    //printf("内部名：%s<br />\n", $holiday->getInternalName());
    //printf("プロパティ：%s<br />\n", print_r($holiday->getProperties(), true));
    //printf("Dateクラス：%s<br />\n", print_r($holiday->getDate(), true));
}
exit;