<?php
require_once 'XML/Unserializer.php';

$file = 'src/lang/Japan/ja_JP.xml';
$options = array('parseAttributes' => false,
                 'attributesArray' => false,
                 'keyAttribute'    => array('property' => 'id'),
                 'forceEnum'       => array('holiday'));

$unserializer = new XML_Unserializer($options);
$status       = $unserializer->unserialize($file, true);

if (PEAR::isError($status)) {
    return Date_Holidays::raiseError($status->getCode(),
                                     $status->getMessage());
}

$ser_file = dirname($file) . '/' . basename($file, '.xml') . '.ser';
file_put_contents($ser_file, serialize($unserializer->getUnserializedData()));
