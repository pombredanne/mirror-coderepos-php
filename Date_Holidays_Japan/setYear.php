<?php
ini_set("include_path", dirname(__FILE__)."/src/" . PATH_SEPARATOR . ini_get("include_path"));

require_once 'Date/Holidays.php';
define('LANG_FILE', '/usr/local/lib/php5/pear/data/Date_Holidays_Japan/lang');

$obj = Date_Holidays::factory('Japan', 2008);
if (Date_Holidays::isError($obj)) {
    $this->fail('Factory was unable to produce driver-object');
}
$obj->addTranslationFile(LANG_FILE . '/Japan/ja_JP.xml', 'ja_JP');
if (Date_Holidays::isError($obj)) {
    $this->fail('fail to add translation file');
}

foreach ($obj->getHolidayDates() as $holiday) {
    echo $holiday->getDate() . "\n";
}
var_dump($obj->getHolidayForDate(new Date('2008-05-06')));
var_dump($obj->getHolidayForDate(new Date('2008-05-06'), 'ja_JP'));

$obj->setYear(2009);
foreach ($obj->getHolidayDates() as $holiday) {
    echo $holiday->getDate() . "\n";
}
var_dump($obj->getHolidayForDate(new Date('2009-05-06')));
var_dump($obj->getHolidayForDate(new Date('2009-05-06'), 'ja_JP'));

$obj->setYear(2010);
foreach ($obj->getHolidayDates() as $holiday) {
    echo $holiday->getDate() . "\n";
}
var_dump($obj->getHolidayForDate(new Date('2010-05-06')));
var_dump($obj->getHolidayForDate(new Date('2010-05-06'), 'ja_JP'));
