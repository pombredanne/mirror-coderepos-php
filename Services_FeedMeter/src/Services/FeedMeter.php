<?php

/**
 * Webservices for FeedMeter
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_FeedMeter
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.1.0
 * @link      http://d.hatena.ne.jp/shimooka/
 * @see       References to other sections (if any)...
 */

require_once 'PEAR.php';
require_once 'HTTP/Request.php';

/**
 * the version number of this package
 */
define('SERVICES_FEEDMETER_VERSION',    '0.1.1');

/**
 * User-Agent for the request
 */
define('SERVICES_FEEDMETER_USER_AGENT', 'Services_FeedMeter/'.SERVICES_FEEDMETER_VERSION);

/**
 * the request URL
 */
define('SERVICES_FEEDMETER_URL', 'http://feedmeter.net/detail.php?r=');

/**
 * Webservices for FeedMeter
 *
 * @category  Services
 * @package   Services_FeedMeter
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.1.0
 * @link      http://d.hatena.ne.jp/shimooka/
 * @see       References to other sections (if any)...
 */
class Services_FeedMeter {

    /**
     * parsed data
     * @var    array
     * @access private
     */
    private $description;

    /**
     * the User-Agent you use
     * @var    string
     * @access private
     */
    private $user_agent;

    /**
     * the given URL for a feed
     * @var    unknown
     * @access private
     */
    private $feed_url;

    /**
     * Constructor
     *
     * @param  string    $feed_url the URL for a feed
     * @return void
     * @access public
     * @throws Exception
     */
    public function __construct($feed_url) {
        if (is_null($feed_url) || $feed_url === '') {
            throw new Exception('Invalid feed URL : [' . $feed_url . ']');
        }
        $this->feed_url = $feed_url;
        $this->user_agent = SERVICES_FEEDMETER_USER_AGENT;
        $this->description = null;
    }

    /**
     * return the version number of this package
     *
     * @return string the version number
     * @access public
     */
    public function getVersion() {
        return SERVICES_FEEDMETER_VERSION;
    }

    /**
     * return the User-Agent you set
     *
     * @return string the User-Agent
     * @access public
     */
    public function getUserAgent() {
        return $this->user_agent;
    }

    /**
     * set the User-Agent you want
     *
     * @param  string $user_agent the User-Agent
     * @return void
     * @access public
     */
    public function setUserAgent($user_agent = null) {
        if (is_null($user_agent) || $user_agent === '') {
            $this->user_agent = SERVICES_FEEDMETER_USER_AGENT;
        } else {
            $this->user_agent = $user_agent;
        }
    }

    /**
     * return the feed URL
     *
     * @return string the feed URL
     * @access public
     */
    public function getFeedUrl() {
        return $this->feed_url;
    }

    /**
     * return the URL that will request to feedmeter.net
     *
     * @return string Return description (if any) ...
     * @access public
     */
    public function getRequestUrl() {
        return SERVICES_FEEDMETER_URL . urlencode($this->feed_url);
    }

    /**
     * return a current rank
     *
     * @return mixed  a current rank
     * @access public
     */
    public function getRank() {
        if (is_null($this->description)) {
            $ret = $this->fetchDescription();
        }
        return $this->description['ranking'];
    }

    /**
     * return a current popularity
     *
     * @return mixed  a current popularity
     * @access public
     */
    public function getPopularity() {
        if (is_null($this->description)) {
            $ret = $this->fetchDescription();
        }
        return $this->description['popularity'];
    }

    /**
     * return a current update frequency
     *
     * @return mixed  a current update frequency
     * @access public
     */
    public function getUpdateFrequency() {
        if (is_null($this->description)) {
            $ret = $this->fetchDescription();
        }
        return $this->description['update_frequency'];
    }

    /**
     * return a current update frequency per day
     *
     * @return mixed  a current update frequency per day
     * @access public
     */
    public function getUpdateItemsPerDay() {
        if (is_null($this->description)) {
            $ret = $this->fetchDescription();
        }
        return $this->description['items_per_day'];
    }

    /**
     * execute a request, fetch and parse
     *
     * @return boolean true
     * @access private
     * @throws Exception throws Exception if any errors occur
     */
    private function fetchDescription() {
        $req = new HTTP_Request($this->getRequestUrl());
        $req->addHeader('User-Agent', SERVICES_FEEDMETER_USER_AGENT);
        $ret = $req->sendRequest();
        if (PEAR::isError($ret)) {
            throw new Exception('FeedMeter: failed to send request');
        }
        switch ($req->getResponseCode()) {
        case 200:
            break;
        default:
            throw new Exception('FeedMeter: return HTTP ' . $req->getResponseCode());
        }

        $this->parseDescription($req->getResponseBody());

        return true;
    }

    /**
     * parse a response body
     *
     * @param  string $body a response body
     * @return boolean true if success. false if failed
     * @access private
     */
    private function parseDescription($body) {
        $this->description = array();
        $regexs = array('ranking' => '/<span class="ranking">\[.* (\d{1,3})/m',
                        'popularity' => '/<td nowrap width="75%">(\d\.\d)<\/td>/m',
                        'update_frequency' => '/<td nowrap>(\d+\.\d)\t\(\d+\.\d+ /m',
                        'items_per_day' => '/<td nowrap>\d+\.\d\t\((\d+\.\d+) /m'
                  );

        foreach ($regexs as $key => $regex) {
	        if (preg_match($regex, $body, $matches) > 0) {
		        $this->description[$key] = $matches[1];
	        } else {
		        $this->description[$key] = false;
			}
        }
    }

}
