<?php
//ini_set("include_path", dirname(__FILE__)."/src/" . PATH_SEPARATOR . ini_get("include_path"));
require_once "Services/Hatena/Star.php";

// Test
$obj = new Services_Hatena_Star('http://d.hatena.ne.jp/shimooka/20070712/1184222100');
var_dump($obj->getVersion());
var_dump($obj->getUserAgent());
var_dump($obj->getUrl());
echo '<hr>';

var_dump($obj->getRequestMode());
var_dump($obj->getRequestUrl());
$entry = $obj->execute();
var_dump(get_class($entry));
var_dump($entry->canComment());
var_dump($entry->getStars());
var_dump($entry->getStarsAsArray());
var_dump($entry->getUri());
var_dump($entry->getRaw());
echo '<hr>';
echo '<hr>';

$obj->setRequestMode(Services_Hatena_Star::REQUEST_ENTRIES);
var_dump($obj->getRequestMode());
var_dump($obj->getRequestUrl());
$entries = $obj->execute();
var_dump(get_class($entry));
var_dump($entries->canComment());
var_dump($entries->getStars());
var_dump($entries->getUri());
var_dump($entries->getRaw());
var_dump($entries->getFirst());
var_dump($entries->getCount());
var_dump($entries->getLast());
