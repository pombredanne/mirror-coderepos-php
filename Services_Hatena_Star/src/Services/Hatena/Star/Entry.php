<?php

/**
 * Webservices for Hatena::Star
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Hatena_Star
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.2
 * @link      http://d.hatena.ne.jp/shimooka/
 */

/**
 * a interface for 'Bean classes'
 *
 * @category  Services
 * @package   Services_Hatena_Star
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.2
 * @link      http://d.hatena.ne.jp/shimooka/
 */
interface Services_Hatena_Star_Entry {
    /**
     * return either you can comment or not
     *
     * @return boolean true if you can comment, false if not
     * @access public
     */
	public  function canComment();

    /**
     * return the 'stars' data
     *
     * @return array stars data
     * @access public
     */
	public function getStars();

    /**
     * return the 'stared' URI
     *
     * @return string the 'stared' URI
     * @access public
     */
	public function getUri();

    /**
     * return the raw JSON data
     *
     * @return object the raw JSON data
     * @access public
     */
	public function getRaw();
}