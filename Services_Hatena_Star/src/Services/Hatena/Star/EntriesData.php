<?php

/**
 * Webservices for Hatena::Star
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Hatena_Star
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.2
 * @link      http://d.hatena.ne.jp/shimooka/
 */

require_once 'Services/Hatena/Star/Entry.php';
require_once 'Services/Hatena/Star/EntryData.php';

/**
 * 'Bean class' for entries
 *
 * @category  Services
 * @package   Services_Hatena_Star
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.2
 * @link      http://d.hatena.ne.jp/shimooka/
 */
final class Services_Hatena_Star_EntriesData implements Services_Hatena_Star_Entry {

    /**
     * entry object
     * @var    an object of Service_Hatena_Star_Entry
     * @access private
     */
	private $entry;

    /**
     * Constructor
     *
     * @param  the raw JSON data that returned from hatena::star
     * @access public
     */
	public function __construct($raw_data) {
		$this->entry = new Services_Hatena_Star_EntryData($raw_data);
	}

    /**
     * return either you can comment or not
     *
     * @return boolean true if you can comment, false if not
     * @access public
     */
	public function canComment() {
		return $this->entry->canComment();
	}

    /**
     * return the 'stars' data
     *
     * @return array stars data
     * @access public
     */
	public function getStars() {
		return $this->entry->getStars();
	}

    /**
     * return the 'stared' URI
     *
     * @return string the 'stared' URI
     * @access public
     */
	public function getUri() {
		return $this->entry->getUri();
	}

    /**
     * return the raw JSON data
     *
     * @return object the raw JSON data
     * @access public
     */
	public function getRaw() {
		return $this->entry->getRaw();
	}

    /**
     * return a user name 'stared' first
     *
     * @return string a user name 'stared' first
     * @access public
     */
	public function getFirst() {
		$arr = $this->entry->getStars();
		return isset($arr[0]->name) ? $arr[0]->name : null;
	}

    /**
     * return the number of stars
     *
     * @return int the number of stars
     * @access public
     */
	public function getCount() {
		$arr = $this->entry->getStars();
		return isset($arr[1]) ? $arr[1] : null;
	}

    /**
     * return a user name 'stared' last
     *
     * @return string a user name 'stared' first
     * @access public
     */
	public function getLast() {
		$arr = $this->entry->getStars();
		return isset($arr[2]->name) ? $arr[2]->name : null;
	}
}