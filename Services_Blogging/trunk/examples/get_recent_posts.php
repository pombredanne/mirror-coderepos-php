<?php
ini_set('include_path', ini_get('include_path') . ':' . dirname(dirname(__FILE__)) . '/src');

require_once 'Services/Blogging.php';

$driver   = 'FC2';
$username = '*****';
$password = '*****';

$client = Services_Blogging::factory($driver, $username, $password, null, null);

try {
    $posts = $client->getRecentPosts();
} catch (Exception $e) {
    die($e->getMessage()."\n");
}

foreach ($posts as $post) {
    printf("%-10s: %s\n", 'Id',    $post->id);
    printf("%-10s: %s\n", 'Date',  date('Y-m-d H:i:s', $post->date));
    printf("%-10s: %s\n", 'Url',   $post->url);
    printf("%-10s: %s\n", 'Title', $post->title);
    echo "-----------------------------------\n";
}

