dnl $Id$
dnl config.m4 for extension hyperestraier

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary. This file will not work
dnl without editing.

dnl If your extension references something external, use with:

dnl PHP_ARG_WITH(hyperestraier, for hyperestraier support,
dnl Make sure that the comment is aligned:
dnl [  --with-hyperestraier             Include hyperestraier support])

dnl Otherwise use enable:

PHP_ARG_ENABLE(hyperestraier, whether to enable hyperestraier support,
Make sure that the comment is aligned:
[  --enable-hyperestraier           Enable hyperestraier support])

dnl if test "$PHP_HYPERESTRAIER" != "no"; then
  dnl Write more examples of tests here...

  dnl # --with-hyperestraier -> check with-path
  ESTRAIER_SEARCH_PATH="/usr/local /usr"
  ESTRAIER_SEARCH_FOR="include include/estraier"
  if test -r $PHP_HYPERESTRAIER/$ESTRAIER_SEARCH_FOR; then # path given as parameter
     HYPERESTRAIER_DIR=$PHP_HYPERESTRAIER
  else # search default path list
     AC_MSG_CHECKING([for hyperestraier files in default path])
     for i in $ESTRAIER_SEARCH_PATH ; do
       for j in $ESTRAIER_SEARCH_FOR ; do
         if test -r $i/$j/estraier.h; then
           HYPERESTRAIER_DIR=$i
           HYPERESTRAIER_INCLUDE_DIR=$i/$j
           AC_MSG_RESULT(found in $i/$j) 
         fi
       done
     done
  fi
  
  if test -z "$HYPERESTRAIER_DIR"; then
     AC_MSG_RESULT([not found])
     AC_MSG_ERROR([Please reinstall the hyperestraier distribution])
  fi

  QDBM_SEARCH_PATH="/usr/local /usr"
  QDBM_SEARCH_FOR="include include/qdbm"
  if test -r $PHP_QDBM/$QDBM_SEARCH_FOR; then # path given as parameter
     QDBM_DIR=$PHP_QDBM
  else # search default path list
     AC_MSG_CHECKING([for qdbm files in default path])
     for i in $QDBM_SEARCH_PATH ; do
       for j in $QDBM_SEARCH_FOR ; do
         if test -r $i/$j/cabin.h; then
           QDBM_DIR=$i
           QDBM_INCLUDE_DIR=$i/$j
           AC_MSG_RESULT(found in $i/$j) 
         fi
       done
     done
  fi
  
  if test -z "$QDBM_DIR"; then
     AC_MSG_RESULT([not found])
     AC_MSG_ERROR([Please reinstall the qdbm distribution])
  fi

  dnl # --with-hyperestraier -> add include path
  PHP_ADD_INCLUDE($HYPERESTRAIER_INCLUDE_DIR)
  PHP_ADD_INCLUDE($QDBM_INCLUDE_DIR)

  dnl # --with-hyperestraier -> check for lib and symbol presence
  LIBNAME=estraier
  LIBSYMBOL=est_doc_new

  PHP_CHECK_LIBRARY($LIBNAME, $LIBSYMBOL,
  [
     LDFLAGS=`estconfig --libs`
     AC_DEFINE(HAVE_HYPERESTRAIERLIB,1,[ ])
  ],[
     AC_MSG_ERROR([wrong hyperestraier lib version or lib not found])
  ],[
     -L$HYPERESTRAIER_DIR/lib `estconfig --libs` `estconfig --ldflags`
  ])
  
  PHP_NEW_EXTENSION(hyperestraier, hyperestraier.c, $ext_shared)

dnl fi
