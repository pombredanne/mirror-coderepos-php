--TEST--
est_doc_new_from_draft() function
--FILE--
<?php
error_reporting(E_ERROR);
$modname = 'hyperestraier.so';
if (extension_loaded($modname)) {
    dl($modname);
}
$doc = est_doc_new_from_draft("hoge");
var_dump($doc);
?>
--EXPECT--
resource(4) of type (ESTDOC)
