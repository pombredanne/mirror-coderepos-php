/*
  +----------------------------------------------------------------------+
  | PHP Version 5                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2007 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

#ifndef PHP_HYPERESTRAIER_H
#define PHP_HYPERESTRAIER_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "ext/standard/info.h"

extern zend_module_entry hyperestraier_module_entry;
#define phpext_hyperestraier_ptr &hyperestraier_module_entry

#ifdef PHP_WIN32
#define PHP_HYPERESTRAIER_API __declspec(dllexport)
#else
#define PHP_HYPERESTRAIER_API
#endif

#ifdef ZTS
#include "TSRM.h"
#endif

PHP_MINIT_FUNCTION(hyperestraier);
PHP_MSHUTDOWN_FUNCTION(hyperestraier);
PHP_RINIT_FUNCTION(hyperestraier);
PHP_RSHUTDOWN_FUNCTION(hyperestraier);
PHP_MINFO_FUNCTION(hyperestraier);

//    documentation API Prototype.
PHP_FUNCTION(est_doc_new);
PHP_FUNCTION(est_doc_new_from_draft);

#ifdef ZTS
#define HYPERESTRAIER_G(v) TSRMG(hyperestraier_globals_id, zend_hyperestraier_globals *, v)
#else
#define HYPERESTRAIER_G(v) (hyperestraier_globals.v)
#endif

#endif	/* PHP_HYPERESTRAIER_H */
