<?PHP
/**
* DataValidator/Simple/Plugin/Standard.class.php
* @author ittetsu miyazaki<ittetsu.miyazaki _at_ gmail.com>
* @package DataValidator_Simple_Plugin
*/

require_once 'DataValidator/Simple/Plugin.class.php';

/** 
* DataValidator_Simple_Plugin_Standard
* DataValidator_Simpleのスタンダードなチェック処理を定義したプラグインです。
* 
* 概要
* <pre>
* class HOGE_Validator_Simple_Profile extends DataValidator_Simple {
*     function HOGE_Validator_Simple_Profile () {
*         $this->load_plugin(array('Standard'));
*         $this->add_check_table(array(
*             'name'    , array('NOT_NULL',array('LENGTH',1,50)),
*             'mail'    , array('NOT_NULL','MOBILE_MAIL',array('LENGTH',1,128)),
*             'pass'    , array('NOT_NULL',array('ALPHANUMS',1,8)),
*             're_pass' , array('NOT_NULL',array('ALPHANUMS',1,8)),
*             array( 'pass_eq'  , array('pass','re_pass')     ) , array('DUPLICATION'),
*             array( 'birthday' , array('year','month','day') ) , array('DATE')
*         ));
*     }
* }
* </pre>
* 
* チェック定義の一覧
* 
* NOT_NULL
*     空文字のチェックをします。
*     
*     'hoge' , array('NOT_NULL')
* 
* NUM
*     [0-9]の数値かどうかチェックします。
*     
*     'hoge' , array('NUM')
* 
* NUMS
*     数値を桁数指定でチェックします。
*     
*     // 4桁の数値であればOK
*     'hoge' , array(array('NUMS',4))
*    　
*     // 4～9桁の数値であればOK
*     'hoge' , array(array('NUMS',4,9))
* 
* RANGE_NUMS
*     数値の範囲をチェックします。
*     
*     // 5,6,7,8,9という数値であればOK
*     'hoge' , array(array('RANGE_NUMS',5,9))
* 
* LE_NUMS
*     ある数値以下の数値かどうかのチェック
*     
*     // 5以下の数値であればOK
*     'hoge' , array(array('LE_NUMS',5))
* 
* GE_NUMS
*     ある数値以上の数値かどうかのチェック
*     
*     // 5以上の数値であればOK
*     'hoge' , array(array('GE_NUMS',5))
* 
* BOOLEAN
*     真偽値のチェックをします。
*     
*     'hoge' , array('BOOLEAN')
* 
* TEL
*     電話番号っぽいかどうかチェックします
*     
*     'hoge' , array('TEL')
* 
* ZIP
*     郵便番号っぽいかチェックします
*     
*     'hoge' , array('ZIP')
* 
* MAIL
*     かなり適当なメアドをチェックします。
*     
*     'hoge' , array('MAIL')
* 
* MOBILE_MAIL
*     かなり適当な携帯のメアドをチェックします
*     
*     'hoge' , array('MOBILE_MAIL')
* 
* URI
*     URIに許されている文字列(RFC 2396)かチェックします。
*     
*     'hoge' , array('URI')
* 
* ASCII
*     アスキー文字かチェックします。
*     
*     'hoge' , array('ASCII')
* 
* ALPHANUM
*     英数字かチェックします。
*     
*     'hoge' , array('ALPHANUM')
* 
* ALPHANUMS
*     英数字の文字数をチェックします。
*     
*     // 4文字の英数字であればOK
*     'hoge' , array(array('ALPHANUMS',4))
*     
*     // 4～9文字の英数字であればOK
*     'hoge' , array(array('ALPHANUMS',4,9))
* 
* DATE
*     YYYYMMDD形式の日付かチェックします
*     
*     'hoge' , array('DATE')
*     
*     また、下記のようにリスト形式でもチェックが可能です
*     
*     // 年月日でチェック
*     array( 'date' , array('year','month','day') ) , array('DATE')
*     
*     // 年月でチェック
*     array( 'birthday' , array('year','month') ) , array('DATE')
* 
* TIME
*     HH24MISS形式の時間かチェックします
*     
*     'hoge' , array('TIME')
*     
*     また、下記のようにリスト形式でもチェックが可能です
*     
*     // 時分秒でチェック
*     array( 'time' , array('hour','min','sec') ) , array('TIME')
*     
*     // 時分でチェック
*     array( 'time' , array('hour','min') ) , array('TIME')
*     
*     // 時でチェック
*     array( 'time' , array('hour') ) , array('TIME')
* 
* DATETIME
*     YYYYMMDDHH24MISS形式の時間かチェックします
*     
*     'hoge' , array('DATETIME')
*     
*     また、下記のようにリスト形式でもチェックが可能です
*     
*     // 年月日時分秒でチェック
*     array( 'date' , array('year','month','day','hour','min','sec') ) , array('DATETIME')
*     
*     // 年月日時分でチェック
*     array( 'date' , array('year','month','day','hour','min') ) , array('DATETIME')
*     
*     // 年月日時でチェック
*     array( 'date' , array('year','month','day','hour') ) , array('DATETIME')
*     
*     // 年月日でチェック
*     array( 'date' , array('year','month','day') ) , array('DATETIME')
*     
*     // 年月でチェック
*     array( 'date' , array('year','month') ) , array('DATETIME')
* 
* SEP_DATE
*     YYYY/MM/DDのようなセパレート付き日付の文字列をチェックします
*     
*     'hoge' , array('SEP_DATE')
* 
* SEP_DATETIME
*     YYYY/MM/DD HH24:MI:SSのようなセパレート付き日付の文字列をチェックします
*     
*     'hoge' , array('SEP_DATETIME')
* 
* DATE_CROSSING
*     date_from(開始日付)よりもdate_to(終了日付)の方が未来になっているかをチェックします。
*     
*     // date_from,date_toにはYYYYMMDD(＋HH24MISS)形式の文字列を指定してください。
*     array( 'date_crossing' , array('date_from','date_to') ) , array('DATE_CROSSING')
* 
* DUPLICATION
*     mugeとmageの値が一致しているかをチェックします。
*     
*     array( 'hoge' , array('muge','fuga) ) , array('DUPLICATION')
*     
*     メアドの再入力チェックなど他のデータと一致しているかチェックしたい場合に便利です。
*      'mail'         , array('MAIL'),
*      'mail_kakunin' , array('MAIL'),
*      array( 'mail_eq' , array('mail','mail_kakunin') ) , array('DUPLICATION')
* 
* EQUAL
*     $STRと同じ文字列かどうかチェックします
*     
*     'hoge' , array(array('EQUAL',$STR))
* 
* NOT_EQUAL
*     $STRと違う文字列かどうかチェックします
*     
*     'hoge' , array(array('NOT_EQUAL',$STR))
* 
* LENGTH
*     文字列の長さ(byte)チェック
*     
*     // 4文字であればOK
*     'hoge' , array(array('LENGTH',4))
*     
*     // 4～9文字であればOK
*     'hoge' , array(array('LENGTH',4,9))
* 
* IN
*     与えられた値のうち、どれかと同じ文字列であればOK
*     
*     // 1か2か3ならOK
*     'hoge' , array(array('IN',1,2,3))
*    　
*     // AかBかCかDならOK
*     'hoge' , array(array('IN','A','B','C','D'))
* 
* REGEX
*     正規表現にマッチすればOK
*     
*     // 先頭がaで始まるかチェック
*     'hoge' , array('REGEX','/^a/'))
* 
* 履歴
* 2007/07/27 ver 0.01
* 2008/03/27 ver 0.02 文字コードをUTF8に変更
* 
* @version 0.02
* @package DataValidator_Simple_Plugin
* @access public
*/
class DataValidator_Simple_Plugin_Standard extends DataValidator_Simple_Plugin {
    
    function NOT_NULL (&$valid,&$name,&$data) {
        return $data !== '' ? true : false;
    }
    
    function NUM (&$valid,&$name,&$data) {
        return preg_match('/^\d+$/',$data) ? true : false;
    }
    
    function BOOLEAN (&$valid,&$name,&$data) {
        return is_bool($data) ? true : false;
    }
    
    function TEL (&$valid,&$name,&$data) {
        return preg_match('/^[0-9]+(?:(?:-?[0-9]+)?)+$/',$data) ? true : false;
    }
    
    function ZIP (&$valid,&$name,&$data) {
        return preg_match('/^\d\d\d-\d\d\d\d$/',$data) ? true : false;
    }
    
    function MAIL (&$valid,&$name,&$data) {
        return preg_match('/^[\x21-\x3F\x41-\x7E]+@[\x21-\x3F\x41-\x7E]+$/',$data) ? true : false;
    }
    
    function MOBILE_MAIL (&$valid,&$name,&$data) {
        if( preg_match('/^[\x21-\x3F\x41-\x7E]+@[\x21-\x3F\x41-\x7E]+$/',$data) ) {
            if( preg_match('/.*\@docomo\.ne\.jp$/',$data)      ) return true;
            if( preg_match('/.*ezweb\.ne\.jp$/',$data)         ) return true;
            if( preg_match('/.*\@.\.vodafone\.ne\.jp$/',$data) ) return true;
            if( preg_match('/.*ez.*\.ido\.ne\.jp$/',$data)     ) return true;
            if( preg_match('/.*\@jp-.\.ne\.jp$/',$data)        ) return true;
        }
        return false;
    }
    
    function URI (&$valid,&$name,&$data) {
        return preg_match('/^[_0-9A-Za-z;\/?:\@&=+\$,-.!~*\'()#%]+$/',$data) ? true : false;
    }
    
    function ASCII (&$valid,&$name,&$data) {
        return preg_match('/^[\x21-\x7E]+$/',$data) ? true : false;
    }
    
    function ALPHANUM (&$valid,&$name,&$data) {
        return preg_match('/^[A-Za-z\d]+$/',$data) ? true : false;
    }
    
    function DATE (&$valid,&$name,&$data) {
        return DataValidator_Simple_Plugin_Standard::_check_date($data);
    }
    
    function TIME (&$valid,&$name,&$data) {
        return DataValidator_Simple_Plugin_Standard::_check_datetime('19801222'.$data);
    }
    
    function DATETIME (&$valid,&$name,&$data) {
        return DataValidator_Simple_Plugin_Standard::_check_datetime($data);
    }
    
    function SEP_DATE (&$valid,&$name,&$data) {
        $date = preg_split('/\D+/',$data);
        return DataValidator_Simple_Plugin_Standard::list_DATE($valid,$name,$date);
    }
    
    function SEP_DATETIME (&$valid,&$name,&$data) {
        $date = preg_split('/\D+/',$data);
        return DataValidator_Simple_Plugin_Standard::list_DATETIME($valid,$name,$date);
    }
    
    function list_DATE (&$valid,&$name,&$data) {
        switch( count($data) ) {
            case  3: return DataValidator_Simple_Plugin_Standard::_check_array_date($data);
            case  2: return DataValidator_Simple_Plugin_Standard::_check_array_date($data[0],$data[1],1);
            default: return false;
        }
    }
    
    function list_TIME (&$valid,&$name,&$data) {
        switch( count($data) ) {
            case  3: return DataValidator_Simple_Plugin_Standard::_check_array_datetime(1980,12,22,$data[0],$data[1],$data[2]);
            case  2: return DataValidator_Simple_Plugin_Standard::_check_array_datetime(1980,12,22,$data[0],$data[1],0       );
            case  1: return DataValidator_Simple_Plugin_Standard::_check_array_datetime(1980,12,22,$data[0],0       ,0       );
            default: return false;
        }
    }
    
    function list_DATETIME (&$valid,&$name,&$data) {
        switch( count($data) ) {
            case  6: return DataValidator_Simple_Plugin_Standard::_check_array_datetime($data);
            case  5: return DataValidator_Simple_Plugin_Standard::_check_array_datetime($data[0],$data[1],$data[2],$data[3],$data[4],0);
            case  4: return DataValidator_Simple_Plugin_Standard::_check_array_datetime($data[0],$data[1],$data[2],$data[3],0       ,0);
            case  3: return DataValidator_Simple_Plugin_Standard::_check_array_datetime($data[0],$data[1],$data[2],0       ,0       ,0);
            case  2: return DataValidator_Simple_Plugin_Standard::_check_array_datetime($data[0],$data[1],1       ,0       ,0       ,0);
            default: return false;
        }
    }
    
    function list_DUPLICATION (&$valid,&$name,&$data) {
        return strcmp($data[0],$data[1]) == 0  ? true : false;
    }
    
    function list_DATE_CROSSING (&$valid,&$name,&$data) {
        return $data[0] <= $data[1] ? true : false;
    }
    
    function list_GREATER_THAN (&$valid,&$name,&$data) {
        return $data[0] > $data[1] ? true : false;
    }
    
    function list_LESS_THAN (&$valid,&$name,&$data) {
        return $data[0] < $data[1] ? true : false;
    }
    
    function factory_EQUAL (&$valid,&$name,&$data,&$args) {
        return strcmp($data,$args[0]) == 0 ? true : false;
    }
    
    function factory_NOT_EQUAL (&$valid,&$name,&$data,&$args) {
        return strcmp($data,$args[0]) != 0 ? true : false;
    }
    
    function factory_NUMS (&$valid,&$name,&$data,&$args) {
        if( isset($args[1]) ){
            list($n,$m) = $args;
            return preg_match('/^\d{'."$n,$m".'}$/',$data) ? true : false;
        }else{
            list($n) = $args;
            return preg_match('/^\d{'.$n.'}$/',$data) ? true : false;
        }
    }
    
    function factory_RANGE_NUMS (&$valid,&$name,&$data,&$args) {
        if( isset($args[1]) ){
            list($n,$m) = $args;
            return ( preg_match("/^\d+$/",$data) && $n <= $data && $data <= $m ) ? true : false;
        }else{
            list($n) = $args;
            return ( preg_match("/^\d+$/",$data) && $n == $data ) ? true : false;
        }
    }
    
    function factory_LE_NUMS (&$valid,&$name,&$data,&$args) {
        return ( preg_match("/^\d+$/",$data) && $data <= $args[0] ) ? true : false;
    }
    
    function factory_GE_NUMS (&$valid,&$name,&$data,&$args) {
        return ( preg_match("/^\d+$/",$data) && $args[0] <= $data ) ? true : false;
    }
    
    function factory_ALPHANUMS (&$valid,&$name,&$data,&$args) {
        if( isset($args[1]) ){
            list($n,$m) = $args;
            return preg_match('/^[A-Za-z\d]{'."$n,$m".'}$/',$data) ? true : false;
        }else{
            list($n) = $args;
            return preg_match('/^[A-Za-z\d]{'.$n.'}$/',$data) ? true : false;
        }
    }
    
    function factory_LENGTH (&$valid,&$name,&$data,&$args) {
        if( isset($args[1]) ){
            list($n,$m) = $args;
            $len = strlen($data);
            return ($n <= $len && $len <= $m) ? true : false;
        }else{
            list($n) = $args;
            return strlen($data) == $n ? true : false;
        }
    }
    
    function factory_IN (&$valid,&$name,&$data,&$args) {
        return in_array($data,$args) ? true : false;
    }
    
    function factory_REGEX (&$valid,&$name,&$data,&$args) {
        $regex =& $args[0];
        return preg_match($regex,$data) ? true : false;
    }
    
    /**
    * YYYYMMDD形式の日付をチェックします。
    * <pre>
    * if( _check_date($yyyymmdd) ) {}
    * </pre>
    * @access private
    * @return boolean 真偽値
    */
    function _check_date($ymd) {
        if ( !preg_match('/^\d{8}$/',$ymd) ) return false;
        list($year,$month,$day) = sscanf($ymd,"%04s%02s%02s");
        return checkdate($month,$day,$year) ? true : false;
    }
    
    /**
    * YYYYMMDDHH24MISS形式の日付をチェックします。
    * <pre>
    * if( _check_datetime($yyyymmddhhmiss) ) {}
    * </pre>
    * @access private
    * @return boolean 真偽値
    */
    function _check_datetime($ymdhms) {
        if ( !preg_match('/^\d{14}$/',$ymdhms) ) return false;
        list($year,$month,$day,$hour,$minute,$second) = sscanf($ymdhms,"%04s%02s%02s%02s%02s%02s");
        if( !checkdate($month,$day,$year) ) return false;
        if( $hour   > 23 ) return false;
        if( $minute > 59 ) return false;
        if( $second > 59 ) return false;
        return true;
    }
    
    /**
    * 日付(年月日)をチェックします。
    * <pre>
    * // どちらでもOK
    * if( _check_array_date($year,$month,$day) ) {}
    * if( _check_array_date(array($year,$month,$day)) ) {}
    * </pre>
    * @access private
    * @param mixed
    * @return boolean 真偽値
    */
    function _check_array_date() {
        $dt = func_get_args();
        if( count($dt) == 1 && is_array($dt[0]) ) $dt =& $dt[0];
        if( count($dt) != 3 ) return false;
        foreach($dt as $i){
            if( !preg_match('/^\d+$/',$i) ) return false;
        }
        if( !checkdate($dt[1],$dt[2],$dt[0]) ) return false;
        return true;
    }
    
    /**
    * 日付(年月日時分秒)をチェックします。
    * <pre>
    * // どちらでもOK
    * if( _check_array_datetime($year,$month,$day,$hour,$minute,$second) ) {}
    * if( _check_array_datetime(array($year,$month,$day,$hour,$minute,$second)) ) {}
    * </pre>
    * @access private
    * @param mixed
    * @return boolean 真偽値
    */
    function _check_array_datetime () {
        $dt = func_get_args();
        if( count($dt) == 1 && is_array($dt[0]) ) $dt =& $dt[0];
        if( count($dt) != 6 ) return false;
        foreach($dt as $i){
            if( !preg_match('/^\d+$/',$i) ) return false;
        }
        if( !checkdate($dt[1],$dt[2],$dt[0]) ) return false;
        if( $dt[3] > 23 ) return false;
        if( $dt[4] > 59 ) return false;
        if( $dt[5] > 59 ) return false;
        return true;
    }
}
