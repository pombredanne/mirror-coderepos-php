<?PHP

require_once 'DataValidator/Simple/Plugin.class.php';

class DataValidator_Simple_Plugin_Utf8JP extends DataValidator_Simple_Plugin {
    
    var $HIRAGANA  = 'ぁあぃいぅうぇえぉおかがきぎくぐけげこごさざしじすずせぜそぞただちぢっつづてでとどなにぬねのはばぱひびぴふぶぷへべぺほぼぽまみむめもゃやゅゆょよらりるれろゎわゐゑをんー';
    var $ZKATAKANA = 'ァアィイゥウェエォオカガキギクグケゲコゴサザシジスズセゼソゾタダチヂッツヅテデトドナニヌネノハバパヒビピフブプヘベペホボポマミムメモャヤュユョヨラリルレロヮワヰヱヲンヴヵヶー';
    var $HKATAKANA = 'ｦｧｨｩｪｫｬｭｮｯｰｱｲｳｴｵｶｷｸｹｺｻｼｽｾｿﾀﾁﾂﾃﾄﾅﾆﾇﾈﾉﾊﾋﾌﾍﾎﾏﾐﾑﾒﾓﾔﾕﾖﾗﾘﾙﾚﾛﾜﾝﾞﾟ';
    
    function HIRAGANA (&$valid,&$name,&$data,&$args) {
        $encode  = mb_regex_encoding('UTF-8');
        $boolean = preg_match('/^['.$HIRAGANA.']+$/',$data) ? true : false;
        mb_regex_encoding($encode);
        return $boolean;
    }
    
    function KATAKANA (&$valid,&$name,&$data,&$args) {
        $encode  = mb_regex_encoding('UTF-8');
        $boolean = preg_match('/^['.$ZKATAKANA.$HKATAKANA.']+$/',$data) ? true : false;
        mb_regex_encoding($encode);
        return $boolean;
    }
    
    function HKATAKANA (&$valid,&$name,&$data,&$args) {
        $encode  = mb_regex_encoding('UTF-8');
        $boolean = preg_match('/^['.$HKATAKANA.']+$/',$data) ? true : false;
        mb_regex_encoding($encode);
        return $boolean;
    }
    
    function ZKATAKANA (&$valid,&$name,&$data,&$args) {
        $encode  = mb_regex_encoding('UTF-8');
        $boolean = preg_match('/^[]+$/',$data) ? true : false;
        mb_regex_encoding($encode);
        return $boolean;
    }
    
    function factory_JLENGTH (&$valid,&$name,&$data,&$args) {
        if( isset($args[1]) ){
            list($n,$m) = $args;
            $len = mb_strlen($data,'UTF-8');
            return ($n <= $len && $len <= $m) ? true : false;
        }else{
            list($n) = $args;
            return mb_strlen($data,'UTF-8') == $n ? true : false;
        }
    }
    
}
