<?PHP
/**
* DataValidator/Simple.class.php
* @author ittetsu miyazaki<ittetsu.miyazaki _at_ gmail.com>
* @package DataValidator
*/

error_reporting( E_ALL );
require_once 'DataValidator/Simple/Result.class.php';

/** 
* DataValidator_Simple
* 値の検証を単純化してくれるクラス
* 
* <pre>
*  $valid =& new DataValidator_Simple();
*  $valid->load_plugin(array('Standard'));
*  $valid->add_check_table(array(
*      'name'    , array('NOT_NULL',array('LENGTH',1,50)),
*      'mail'    , array('NOT_NULL','MOBILE_MAIL',array('LENGTH',1,128)),
*      'pass'    , array('NOT_NULL',array('ALPHANUMS',1,8)),
*      're_pass' , array('NOT_NULL',array('ALPHANUMS',1,8)),
*      array( 'pass_eq'  , array('pass','re_pass')     ) , array('DUPLICATION'),
*      array( 'birthday' , array('year','month','day') ) , array('DATE')
*  ));
*  $valid->add_data_by_param( $cgi ); // $cgiはPerlのCGI.pmのようにparamメソッドを持っていることを前提としている。
*  $result =& $valid->validate();
*  
*  if ( $result->has_error() ) {
*      if ( $result->is_error('name') ) {
*          print '名前に誤りがあります。';
*      }
*  }
* </pre>
* 
*  また、check()メソッドを使えばPerlのFormValidator::Simpleのような動作も可能です。
*  
* <pre>
*  $result =& $valid->check($cgi,array(
*      'name'    , array('NOT_NULL',array('LENGTH',1,50)),
*      'mail'    , array('NOT_NULL','MOBILE_MAIL',array('LENGTH',1,128)),
*      'pass'    , array('NOT_NULL',array('ALPHANUMS',1,8)),
*      're_pass' , array('NOT_NULL',array('ALPHANUMS',1,8)),
*      array( 'pass_eq'  , array('pass','re_pass')     ) , array('DUPLICATION'),
*      array( 'birthday' , array('year','month','day') ) , array('DATE')
*  ));
* </pre>
* 
* 履歴
* 2006/03/06 ver 0.00 
* 2007/07/27 ver 0.01 少し手直し
* 2007/10/18 ver 0.02 ドキュメント少し整備
* 2007/11/16 ver 0.03 任意項目の処理を追加(NOT_NULLの場合に特殊処理する)
* 2008/03/27 ver 0.04 add_data_by_paramのバグ修正
*                     文字コードをUTF8に変更
* 
* @version 0.04
* @package DataValidator
* @access public
*/
class DataValidator_Simple {
    
    var $_prefix      = 'STD';
    var $_data        = array();
    var $_plugin_list = array();
    var $_check_table = array(
            'param' => array(),
            'binds' => array(),
        );
    
    /**
    * プラグインのチェック用コードの接頭辞を設定/取得します。
    * デフォルトでは'STD'が設定されています。
    * 例えばSTDが設定されている状態なら、下記のようにSTDという接頭辞を省略することが可能です。
    * <pre>
    *  $valid->default_plugin_prefix('STD');
    *  $valid->add_check_table(array(
    *      'hoge',array('STD::NOT_NULL'), // 本来はこう書く
    *      'muge',array('NOT_NULL'),      // STDが設定されているので省略してもSTD::NOT_NULLが指定されたことになる
    *  ));
    * </pre>
    * @access public
    * @param  string 接頭辞(省略可)
    * @return string 接頭辞
    */
    function default_plugin_prefix($prefix=null) {
        if( !is_null($prefix) ) $this->_prefix = $prefix;
        return $this->_prefix;
    }
    /**
    * プラグインを読み込んで新しいチェック用のコードを使えるようにします。
    * 
    * <pre>
    *  $valid->load_plugin(array('Standard'));
    * </pre>
    * 上記のようにすると先頭にDataValidator_Simple_PluginをくっつけてDataValidator_Simple_Plugin_Standardが読み込まれます。
    * 
    * そしてDataValidator_Simple_Plugin_Standardに定義されているチェック用のコードが利用できるようになります。
    * <pre>
    *  // DataValidator_Simple_Plugin_Standardで
    *  // 提供されているチェック用コードが使えるようになる(NOT_NULLとかNUMとか)
    *  $valid->load_plugin(array('Standard'));
    *  $valid->add_check_table(array(
    *      'hoge',array('NOT_NULL','NUM'),
    *      'muge',array('NOT_NULL,array('LENGTH',1,10)),
    *  ));
    * </pre>
    * またプラグインを複数読み込んだ際に同じ名前のチェック用のコードがあった場合は後で読み込んだ方のチェック用コードが利用されます。
    * <pre>
    *  $valid->load_plugin(array('PluginA','PluginB'));
    *  $valid->load_plugin(array('PluginC'));
    *  $valid->add_check_table(array(
    *      'hoge',array('TEST_PLUGIN'),
    *  ));
    * </pre>
    * 例えば上記のように設定した場合、まずPluginC.class.phpにTEST_PLUGINというチェック用コードがあるかどうか検査し、
    * なければPluginB.class.phpを検査し、そこにもなければPluginA.class.phpを検査するという流れになります。
    * そして何処にもチェック用コードが定義されていなかった場合は例外を投げます。
    * 
    * また、プラグイン名の先頭に+を付けて渡すと接頭辞を付けずに読み込みます。
    * 
    * @access public
    * @param  array プラグイン名
    */
    function load_plugin ($plugins) {
        $this->_load_plugin('DataValidator_Simple_Plugin_',$plugins);
    }
    
    /**
    * プラグインを読み込んで新しいチェック用のコードを使えるようにします。
    * 
    * load_plugin()との違いは先頭にDataValidator_Simple_Pluginをくっつけないことです。
    * つまり引数で渡されたパッケージ名がそのまま読み込まれるようになります。
    * 
    * 独自のプラグインを作成した場合に使いましょう。
    * 
    * また、load_pluginメソッドで+を付けてプラグインを渡すとこのメソッドと同じ処理になります。
    * 
    * @access public
    * @param  array プラグイン名
    */
    function load_full_plugin ($plugins) {
        $this->_load_plugin('',$plugins);
    }
    
    function _load_plugin ($prefix,&$plugins) {
        
        if( !is_array($plugins) )
            trigger_error('load_(full)?_plugin method is "array()" only',E_USER_ERROR);
        
        foreach( $plugins as $plugin ){
            
            if( is_null($plugin) || $plugin == '' )
                trigger_error('plugin undefined error.',E_USER_ERROR);
            
            if ( strpos($plugin,'+') === 0 ) {
                $plugin = substr($plugin,1);
            }
            else {
                $plugin = $prefix.$plugin;
            }
            
            if( !class_exists($plugin) ){
                require_once( str_replace('_','/',$plugin) . '.class.php' );
                
                // クラス名を元に継承してるかチェックすることが
                // できないんでメソッドが存在するかチェックで代用。
                if( !is_callable(array($plugin,'plugin_prefix')) )
                    trigger_error("$plugin isn't ISA DataValidator_Simple_Plugin",E_USER_ERROR);
            }
            
            $this->_plugin_list[call_user_func(array($plugin,'plugin_prefix'))][] = $plugin;
        }
        
    }
    
    /**
    * チェック処理を定義します。
    * 
    * チェック処理を定義するにはまずはload_plugin()を使ってチェック用のコードを読み込みます。
    * 
    * <pre>
    *  class HOGE_DataValidator_Simple_Test extends DataValidator_Simple {
    *      function HOGE_DataValidator_Simple_Test () {
    *          $this->load_plugin(array('Standard'));
    *          $this->add_check_table(array(
    *              'hoge',array('STD::NUM'),
    *              'muge',array('STD::NOT_NULL'),
    *          ));
    *      }
    *  }
    *  $valid =& new HOGE_DataValidator_Simple_Test;
    *  $valid->add_data(array( 'hoge' => 'iii' , 'muge' => 'aaa' ));
    *  $result =& $valid->validate();
    *  $result->is_error('hoge'); // hogeは数値じゃないので真が返る
    *  $result->is_error('muge'); // mugeは空じゃないので偽が返る
    * </pre>
    * hogeにはSTD::NUMチェックを行い、mugeにはSTD::NOT_NULLチェックを行います。
    * これらはStandardプラグインで提供されるチェック用コードです。
    * 
    * また、一つのチェックキーに対して同じ名前のチェック処理を定義することはできません。
    * <pre>
    *  $valid->add_check_table(array(
    *      'hoge',array('STD::NUM','STD::NUM') // NUMチェックを二回定義できない
    *  ));
    * </pre>
    * ちなみにチェック用コードの先頭についている'STD'は省略することができます。
    * <pre>
    *  // STD::つけなくてもいい
    *  $valid->add_check_table(array(
    *      'hoge',array('NUM'),
    *      'muge',array('NOT_NULL'),
    *  ));
    * </pre>
    * 接頭辞が何故省略できるかどうかについてはdefault_plugin_prefix()を参照してください。
    * 
    * また、チェック用コードの先頭に!を付けると否定の処理を行うことも可能です。
    * 
    * <pre>
    *  $valid->add_check_table(array(
    *      'hoge',array('NUM'),  // 数値ならOK
    *      'hoge',array('!NUM'), // 数値じゃないものならOK
    *  ));
    * </pre>
    * 
    * @access public
    * @param  array チェック定義
    */
    function add_check_table ($table) {
        if( !is_array($table) )      trigger_error("add_check_table() params is ARRAY reference",E_USER_ERROR);
        if( count($table) < 2 )      trigger_error("add_check_table() takes over two arguments",E_USER_ERROR);
        if( count($table) % 2 != 0 ) trigger_error('add_check_table( param => value ) parameter only',E_USER_ERROR);
        
        $i = 0;
        while( $i < count($table) ) {
            $param     = $table[$i];
            $check_val = $table[$i+1];
            
            $list = '';
            if( is_array($param) ) {
                // array('date',array('year','month','day')) というような形式の場合
                if( !is_array($param) ) trigger_error('params is ARRAY reference',E_USER_ERROR);
                list($param,$vals) = $param;
                $list = 'list_';
                $this->_delete_bind($param);
                $this->_check_table['param'][$param]['binds'] = $vals;
                foreach($vals as $val){
                    $this->_check_table['binds'][$val][$param] = 0;
                }
            }else{
                $this->_delete_bind($param);
            }
            
            $this->_add_check($param,$check_val,$list);
            
            $i += 2;
        }
    }
    
    function _add_check (&$param,&$check_val,&$list) {
        
        $same = array();
        foreach ($check_val as $code) {
            $prefix = null;
            $subs   = null;
            if( is_array($code) ){
                $func = array_shift($code);
                list($prefix,$not_flag,$subs) = $this->_parse_funcname($func);
                $factory = 'factory_'.$list.$subs;
                $this->_check_table['param'][$param]['check'][] = array($prefix.'::'.$subs,$this->_dispatch_plugin($prefix,$factory),$code,$not_flag);
            }else{
                list($prefix,$not_flag,$subs) = $this->_parse_funcname($code);
                $this->_check_table['param'][$param]['check'][] = array($prefix.'::'.$subs,$this->_dispatch_plugin($prefix,$list.$subs),null,$not_flag);
            }
            
            // 同じ名前の処理を二度以上設定できない
            if( array_key_exists($prefix.'::'.$subs,$same) )
                trigger_error("check name same error '${prefix}::$subs'",E_USER_ERROR);
            
            $same[$prefix.'::'.$subs] = 0;
        }
        
    }
    
    /**
    * チェックしたいデータをハッシュ形式で渡します。
    * <pre>
    *  $valid->add_data(array(
    *     'aaa' => 111 ,
    *     'bbb' => 'hoge'
    *  ));
    * </pre>
    * add_check_table()でチェック処理を定義していないキー名が渡された場合に例外を投げます。
    * <pre>
    *  $valid->add_check_table(array(
    *      'aaa',array('NUM'),
    *      array('date',array('year','month','day')),array('DATE')
    *  );
    *  $valid->add_data(array(
    *     'aaa'  => 111 ,
    *     'bbb'  => 'hoge', // bbbはadd_check_table()で定義されていないので例外を投げる。
    *  ));
    * </pre>
    * ただし、上記のような状態でyear,month,dayをadd_data()に渡しても例外にはなりません。
    * <pre>
    *  // これは例外にはならない。dateというチェックが走る
    *  $valid->add_data(array(
    *      'year'  , 2005,
    *      'month' , 12,
    *      'day'   , 9
    *  ));
    * </pre>
    * @access public
    * @param  array チェック値
    */
    function add_data ($data_array) {
        foreach ($data_array as $param => $data) {
            if(!( isset($this->_check_table['param'][$param]) || isset($this->_check_table['binds'][$param]) ))
                trigger_error('['.$param.'] unknown check table key',E_USER_ERROR);
            
            $this->_data[$param] = $data;
        }
    }
    
    /**
    * CGI.pmのparam()ようなメソッドを持つオブジェクトからチェックする値の追加を行う
    * <pre>
    *  $cgi = new CGI;
    *  $valid->add_data_by_param($cgi,array('aaa','year','month','day'));
    * </pre>
    * ちなみに上記例はadd_data()を使った場合下記と同じ意味になる。
    * <pre>
    *  $valid->add_data(array(
    *      'aaa'   => $cgi->param('aaa'),
    *      'year'  => $cgi->param('year'),
    *      'month' => $cgi->param('month'),
    *      'day'   => $cgi->param('day'),
    *  ));
    * </pre>
    * @access public
    * @param  object param()メソッドを持つオブジェクト
    * @param  array  チェックのキー名
    */
    function add_data_by_param (&$param_obj,$keys=null) {
        if ( is_null($keys) ) {
            foreach ( $this->_check_table['param'] as $key => $val ) {
                if ( isset($val['binds']) ) {
                    $keys = array_merge($keys,$val['binds']);
                }
                else {
                    $keys []= $key;
                }
            }
        }
        foreach ($keys as $key){
            if(!( isset($this->_check_table['param'][$key]) || isset($this->_check_table['binds'][$key]) ))
                trigger_error('['.$key.'] unknown check table key',E_USER_ERROR);
            
            $this->_data[$key] = $param_obj->param($key);
        }
    }
    
    /**
    * 内部で保持しているチェックするデータをクリアします。
    * 
    * このメソッドを使うことにより同じオブジェクトで違うデータをチェックすることができます。
    * <pre>
    *  $valid = new HOGE_DataValidator_Simple_Test;
    *  $valid->add_data(array( 'hoge' => 1 ));
    *  $result =& $valid->validate(); // 検証
    * 　
    *  // ここで削除して
    *  $valid->data_clear();
    * 　
    *  $valid->add_data(array( 'muge' => 1 ));
    *  $result =& $valid->validate() // 別の値で検証(hogeのチェックは行われない)
    * </pre>
    * @access public
    */
    function data_clear() {
        $this->_data = array();
    }
    
    function _dispatch_plugin($prefix,$method) {
        if( array_key_exists($prefix,$this->_plugin_list) ) {
            foreach( array_reverse($this->_plugin_list[$prefix]) as $plugin ) {
                if( is_callable(array($plugin,$method)) ){
                    return $plugin.'::'.$method;
                }
            }
        }
        trigger_error("Can't exists plugin method \"$method\"",E_USER_ERROR);
    }
    
    function _parse_funcname($func) {
        preg_match('/^(?:([A-Z_\d:]+)::)?(\!)?([A-Z_\d]+)$/',$func,$matches);
        array_shift($matches);
        $prefix   = array_shift($matches);
        $not_flag = array_shift($matches);
        $subs     = array_shift($matches);
        
        if( !$not_flag ) $not_flag = '';
        
        if( is_null($subs) )
            trigger_error("check name '$func' format error [A-Z_0-9] or ![A-Z_0-9]",E_USER_ERROR);
        
        return array($prefix ? $prefix : $this->default_plugin_prefix(),$not_flag,$subs);
    }
    
    function _delete_bind ($param) {
        
        // bind関係を解除する
        if( isset($this->_check_table['param'][$param]['binds']) ){
            foreach($this->_check_table['param'][$param]['binds'] as $val){
                unset( $this->_check_table['binds'][$val][$param] );
                if( !count($this->_check_table['binds'][$val]) )
                    unset( $this->_check_table['binds'][$val]);
            }
        }
        
        $this->_check_table['param'][$param] = null;
    }
    
    /**
    * 全ての値のチェック処理を行います。
    * add_data()で設定されている値に対してadd_check_table()で設定したチェック処理を行います。
    * 戻り値はDataValidator_Simple_Resultオブジェクトです。
    * @access public
    * @return object DataValidator_Simple_Resultオブジェクト
    */
    function &validate() {
        $result     = array();
        $next_param = array();
        foreach ( $this->_data as $key => $val ){
            if( array_key_exists($key,$next_param) ) continue;
            $next_param[$key] = 1;
            
            if( isset($this->_check_table['param'][$key]) ) {
                if( isset($this->_check_table['param'][$key]['binds']) ) {
                    // おかしい？
                }else{
                    if( is_null($val) ) $val = '';
                    foreach( $this->_check_table['param'][$key]['check'] as $check ) {
                        $result[$key]['errors'][$check[0]] = call_user_func_array(split('::',$check[1]),array(&$this,&$key,&$val,&$check[2]));
                        if ( !$check[3] ) $result[$key]['errors'][$check[0]] = !$result[$key]['errors'][$check[0]];
                    }
                    $result[$key]['error'] = in_array(true,$result[$key]['errors']) ? true : false;
                    $result[$key]['blank'] = ( is_null($val) || $val === '' ) ? true : false;
                    $result[$key]['data' ] = $val;
                    $result[$key]['error'] = $this->_not_null_error($result[$key]);
                }
            }
            
            // bindsがなければこれで終わり
            if( !array_key_exists($key,$this->_check_table['binds']) ) continue;
            
            foreach( $this->_check_table['binds'][$key] as $bind_param => $i ){
                if( array_key_exists($bind_param,$next_param) ) continue;
                $next_param[$bind_param] = 1;
                
                $bdata = array();
                foreach( $this->_check_table['param'][$bind_param]['binds'] as $bparam ){
                    if( !array_key_exists($bparam,$this->_data) ) break;
                    if( is_null($this->_data[$bparam]) ) $this->_data[$bparam] = '';
                    $bdata[$bparam] =& $this->_data[$bparam];
                }
                // bindsが全部揃ってなければ次へ
                if( count($bdata) != count($this->_check_table['param'][$bind_param]['binds']) ) continue;
                
                foreach( $this->_check_table['param'][$bind_param]['check'] as $check ){
                    $bdata_val = array_values($bdata);
                    $result[$bind_param]['errors'][$check[0]] = call_user_func_array(split('::',$check[1]),array(&$this,&$key,&$bdata_val,&$check[2]));
                    if ( !$check[3] ) $result[$bind_param]['errors'][$check[0]] = !$result[$bind_param]['errors'][$check[0]];
                }
                $result[$bind_param]['error'] = in_array(true,$result[$bind_param]['errors']) ? true : false;
                $result[$bind_param]['blank'] = $this->_is_blank_array($bdata);
                $result[$bind_param]['data' ] = $bdata;
                $result[$bind_param]['error'] = $this->_not_null_error($result[$bind_param]);
            }
        }
        
        $result =& new DataValidator_Simple_Result($this,$result);
        return $result;
    }
    
    /**
    * PerlのFormValidator::Simpleのcheckと同じような動作をします。
    * 
    * <pre>
    * $result =& $valid->check($cgi,array(
    *     'name'    , array('NOT_NULL',array('LENGTH',1,50)),
    *     'mail'    , array('NOT_NULL','MOBILE_MAIL',array('LENGTH',1,128)),
    *     'pass'    , array('NOT_NULL',array('ALPHANUMS',1,8)),
    *     're_pass' , array('NOT_NULL',array('ALPHANUMS',1,8)),
    *     array( 'pass_eq'  , array('pass','re_pass')     ) , array('DUPLICATION'),
    *     array( 'birthday' , array('year','month','day') ) , array('DATE')
    * ));
    * </pre>
    * 
    * @access public
    * @param  object param()メソッドを持つオブジェクト
    * @param  array チェック定義
    */
    function &check (&$param_object,$table) {
        $this->add_check_table($table);
        if ( is_array($param_object) ) {
            $this->add_data($param_object);
        }
        else {
            $this->add_data_by_param($param_object);
        }
        return $this->validate();
    }
    
    function _is_blank_array(&$data) {
        foreach( $data as $val ){
            if( !is_null($val) && $val !== '' ) return false;
        }
        return true;
    }
    
    function _not_null_error (&$result) {
        if ( $result['error'] === false ) return false;
        if (
            !isset($result['errors']['STD::NOT_NULL']) && 
            $result['blank'] === true
        ) {
            return false;
        }
        else {
            return true;
        }
    }
}
