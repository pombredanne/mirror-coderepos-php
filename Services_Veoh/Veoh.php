<?php
/**
 * A class using Veoh(http://www.veoh.com/) API
 *
 * PHP versions 4 and 5
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_0.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @categories Services
 * @package    Services_Veoh
 * @author     Tsuyoshi Maekawa <main.xcezx@gmail.com>
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @see        http://www.veoh.com/rest/v2/doc.html
 * @version    0.01
 */

/**
 * Uses PEAR class for error management.
 */
require_once 'PEAR.php';

/**
 * Uses HTTP_Request class to send and recive data from Veoh web servers.
 */
require_once 'HTTP/Request.php';

/**
 * Uses XML_Unserializer class to parse data recived from Veoh.
 */
require_once 'XML/Unserializer.php';

/**
 * Define API base url.
 */
if ( !defined('SERVICES_VEOH_BASE_URL') ) {
    define('SERVICES_VEOH_BASE_URL', 'http://www.veoh.com/rest/v2/execute.xml');
}

/**
 * Define API base url used SSL.
 */
if ( !defined('SERVICES_VEOH_BASE_SSL_URL') ) {
    define('SERVICES_VEOH_BASE_SSL_URL', 'https://www.veoh.com/rest/v2/execute.xml');
}

/**
 * Class for accessing and retrieving information from Veoh API.
 *
 * @package Services_Veoh
 * @author  Tsuyoshi Maekawa <main.xcezx@gmail.com>
 * @access  public
 * @version Release: @package_version@
 * @uses    PEAR
 * @uses    HTTP_Request
 * @uses    XML_Unserializer
 */
class Services_Veoh
{
    var $base_url = SERVICES_VEOH_BASE_URL;
    var $api_key;
    var $params = array();
    var $use_ssl;
    var $methods_map = array(
                             'reflection' => array(
                                                   'getAvailableCategories'         => array( 'verb' => 'GET', ),
                                                   'getAvailableContentRatings'     => array( 'verb' => 'GET', ),
                                                   'getAvailableCountries'          => array( 'verb' => 'GET', ),
                                                   'getAvailableLanguages'          => array( 'verb' => 'GET', ),
                                                   'getAvailableSyndicatationSites' => array( 'verb' => 'GET', ),
                                                   'getMethod'                      => array( 'verb' => 'GET', ),
                                                   'getMethods'                     => array( 'verb' => 'GET', ),
                                                   ),
                             'collection' => array(
                                                   'getActivePurchase'  => array( 'verb' => 'GET',  ),
                                                   'removeComment'      => array( 'verb' => 'POST', ),
                                                   'addComment'         => array( 'verb' => 'POST', ),
                                                   'getComments'        => array( 'verb' => 'GET',  ),
                                                   'getMediaLicenses'   => array( 'verb' => 'GET',  ),
                                                   'getSubscribers'     => array( 'verb' => 'GET',  ),
                                                   'removeSubscription' => array( 'verb' => 'POST', ),
                                                   'getSubscriptions'   => array( 'verb' => 'GET',  ),
                                                   'addVideo'           => array( 'verb' => 'POST', ),
                                                   'getVideos'          => array( 'verb' => 'GET',  ),
                                                   'cancel'             => array( 'verb' => 'POST', ),
                                                   'createChannel'      => array( 'verb' => 'POST', ),
                                                   'createSeries'       => array( 'verb' => 'POST', ),
                                                   'findById'           => array( 'verb' => 'GET',  ),
                                                   'findByPermalink'    => array( 'verb' => 'GET',  ),
                                                   'subscribe'          => array( 'verb' => 'POST', ),
                                                   'updateChannel'      => array( 'verb' => 'POST', ),
                                                   'updateSeries'       => array( 'verb' => 'POST', ),
                                                   ),
                             'search' => array(
                                               'getRecommended' => array( 'verb' => 'GET', ),
                                               'getSimilar'     => array( 'verb' => 'GET', ),
                                               'search'         => array( 'verb' => 'GET', ),
                                               ),
                             'billing' => array(
                                                'getBillingDetails'   => array( 'verb' => 'GET', 'use_ssl' => true, ),
                                                'clearCreditCardInfo' => array( 'verb' => 'POST', ),
                                                'purchase'            => array( 'verb' => 'POST', 'use_ssl' => true, ),
                                                'saveBillingDetails'  => array( 'verb' => 'POST', 'use_ssl' => true, ),
                                                'saveCreditCardInfo'  => array( 'verb' => 'POST', 'use_ssl' => true, ),
                                                ),
                             'license' => array(
                                                'getAvailableLicenses' => array( 'verb' => 'GET', ),
                                                'create'               => array( 'verb' => 'POST', ),
                                                'findById'             => array( 'verb' => 'GET', ),
                                                ),
                             'video' => array(
                                              'getActivePurchase'   => array( 'verb' => 'GET',  ),
                                              'removeAllTags'       => array( 'verb' => 'POST', ),
                                              'getChannels'         => array( 'verb' => 'GET',  ),
                                              'addComment'          => array( 'verb' => 'GET',  ),
                                              'removeComment'       => array( 'verb' => 'POST', ),
                                              'getComments'         => array( 'verb' => 'GET',  ),
                                              'getFavorited'        => array( 'verb' => 'GET',  ),
                                              'removeFromFavorites' => array( 'verb' => 'POST', ),
                                              'addMediaLicense'     => array( 'verb' => 'POST', ),
                                              'getMediaLicenses'    => array( 'verb' => 'GET',  ),
                                              'getPrimarySeries'    => array( 'verb' => 'GET',  ),
                                              'addTag'              => array( 'verb' => 'POST', ),
                                              'removeTag'           => array( 'verb' => 'POST', ),
                                              'getTags'             => array( 'verb' => 'GET',  ),
                                              'addToFavorites'      => array( 'verb' => 'POST', ),
                                              'cancel'              => array( 'verb' => 'POST', ),
                                              'create'              => array( 'verb' => 'POST', ),
                                              'findById'            => array( 'verb' => 'GET',  ),
                                              'findByPermalink'     => array( 'verb' => 'GET',  ),
                                              'flag'                => array( 'verb' => 'POST', ),
                                              'postToBlog'          => array( 'verb' => 'POST', ),
                                              'rateIt'              => array( 'verb' => 'POST', ),
                                              'updateAudibleMagic'  => array( 'verb' => 'POST', ),
                                              'updateInfo'          => array( 'verb' => 'POST', ),
                                              ),
                             'people' => array(
                                               'removeComment'          => array( 'verb' => 'POST', ),
                                               'addComment'             => array( 'verb' => 'POST', ),
                                               'getComments'            => array( 'verb' => 'GET',  ),
                                               'getConfirmedFriends'    => array( 'verb' => 'GET',  ),
                                               'getDownloads'           => array( 'verb' => 'GET',  ),
                                               'getFavorites'           => array( 'verb' => 'GET',  ),
                                               'getFriendRequests'      => array( 'verb' => 'GET',  ),
                                               'getFriendsChannels'     => array( 'verb' => 'GET',  ),
                                               'getFriendsFavorites'    => array( 'verb' => 'GET',  ),
                                               'getFriendsSeries'       => array( 'verb' => 'GET',  ),
                                               'getFrindsSubscriptions' => array( 'verb' => 'GET',  ),
                                               'getFriendsVideos'       => array( 'verb' => 'GET',  ),
                                               'removeMessage'          => array( 'verb' => 'POST', ),
                                               'getMessageById'         => array( 'verb' => 'GET',  ),
                                               'getPendingFriends'      => array( 'verb' => 'GET',  ),
                                               'getPublishedChannels'   => array( 'verb' => 'GET',  ),
                                               'getPublishedSeries'     => array( 'verb' => 'GET',  ),
                                               'getPublishedVideos'     => array( 'verb' => 'GET',  ),
                                               'getPurchaseHistory'     => array( 'verb' => 'GET',  ),
                                               'getReceivedMessages'    => array( 'verb' => 'GET',  ),
                                               'removeRecentSearch'     => array( 'verb' => 'POST', ),
                                               'getRecentSearches'      => array( 'verb' => 'GET',  ),
                                               'getSentMessages'        => array( 'verb' => 'GET',  ),
                                               'removeSiteAccount'      => array( 'verb' => 'POST', ),
                                               'getSiteAccounts'        => array( 'verb' => 'GET',  ),
                                               'getSubscribers'         => array( 'verb' => 'GET',  ),
                                               'removeSubscription'     => array( 'verb' => 'POST', ),
                                               'getSubscriptions'       => array( 'verb' => 'GET',  ),
                                               'authenticate'           => array( 'verb' => 'POST', 'use_ssl' => true, ),
                                               'createSiteAccount'      => array( 'verb' => 'POST', 'use_ssl' => true, ),
                                               'findByEmail'            => array( 'verb' => 'GET',  ),
                                               'findByUserName'         => array( 'verb' => 'GET',  ),
                                               'sendMessage'            => array( 'verb' => 'POST', ),
                                               'signUp'                 => array( 'verb' => 'POST', 'use_ssl' => true, ),
                                               'subscribe'              => array( 'verb' => 'POST', ),
                                               'updateInfo'             => array( 'verb' => 'POST', ),
                                               'updatePassword'         => array( 'verb' => 'POST', 'use_ssl' => true, ),
                                               ),
                             );

    /**
     * Constructor
     *
     * @access public
     * @param  string $api_key
     */
    function Services_Veoh($api_key)
    {
        $this->api_key = $api_key;
    }

    /**
     * Retrieves the current version of this class API.
     *
     * @access public
     * @static
     * @return string the API version
     */
    function getVersion()
    {
        return '0.01';
    }

    /**
     * Sends the request to Veoh API.
     *
     * @access public
     * @param  string  $method
     * @param  array   $params
     * @param  boolean $use_ssl
     * @return mixed   Returns a PEAR_Error on error and an array of response data on success.
     */
    function sendRequest($method, $params = array(), $use_ssl = false)
    {
        $prefix = 'veoh';
        list($type, $action) = split('\.', $method);
        $verb = $this->methods_map[$type][$action]['verb'];
        if ( isset($this->methods_map[$type][$action]['use_ssl']) && $this->methods_map[$type][$action]['use_ssl'] == true ) {
            $this->use_ssl = true;
        } else {
            $this->use_ssl = $use_ssl;
        }

        if ( $this->use_ssl ) {
            $this->base_url = SERVICES_VEOH_BASE_SSL_URL;
        }

        if ( $verb === 'GET' ) {
            $url = $this->base_url . '?method=' . $prefix . '.' . $method . '&apiKey=' . $this->api_key;
            if ( count($params) > 0 ) {
                foreach ( $params as $key => $value ) {
                    $url .= '&' . $key . '=' . urlencode($value);
                }
            }
        } else {
            $url = $this->base_url;
            $body = 'method=' . $prefix . '.' . $method . '&apiKey=' . $this->api_key;
            if ( count($params) > 0 ) {
                foreach ( $params as $key => $value ) {
                    $body .= '&' . $key . '=' . urlencode($value);
                }
            }
        }

        $http =& new HTTP_Request($url);
        $http->setMethod($verb);
        $http->addHeader('User-Agent', 'Services_Veoh/' . $this->getVersion());
        if ( $verb === 'POST' ) {
            $http->addHeader('content-type', 'application/x-www-form-urlencoded');
            $http->setBody($body);
        }

        $http->sendRequest();

        if ( $http->getResponseCode() != 200 ) {
            return PEAR::raiseError('Veoh returnd invalid HTTP response code ' . $http->getResponseCode());
        }

        $result = $http->getResponseBody();

        $xml =& new XML_Unserializer();
        $xml->setOption(XML_UNSERIALIZER_OPTION_ATTRIBUTES_PARSE, true);
        $xml->unserialize($result, false);
        $data = $xml->getUnserializedData();
        if ( PEAR::isError($data) ) {
            return $data;
        }
        if ( isset($data['stat']) && $data['stat'] !== 'ok' ) {
            $error = $data['errorList']['error'];
            $error_msg = sprintf('%s: %s', $error['errorMessage'], $error['errorCode']);
            return PEAR::raiseError($error_msg);
        }

        return $data;
    }
}
?>
