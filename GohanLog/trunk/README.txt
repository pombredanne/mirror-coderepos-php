GohanLog version 0.1
================================================================================
■1■このソフトウェアはなにか
生物の栄養補給活動を記録し、その記録を分析し、
分析結果により生物の行動に何らかの修正を誘発するソフトウェアです。

ライセンスはPHPライセンスです。

■2■ソフトウェアのゴール
1) より多くの生物が栄養補給活動を記録すること。
2) より正確に栄養補給活動を記録すること。
3) より多様な分析結果を示すこと。
4) より生物の行動の修正を誘発すること。

■3■インストール方法
1) ソース設置
2) DB構築
	CREATE DATABASE `DBNAME` DEFAULT CHARACTER SET utf8 ;
	mysql -u DBUSER -p --default-character-set=utf8 DBNAME < app/config/sql/install.sql
	
3) 設定ファイルの設定
	cp app/config/database.php.default app/config/database.php
	vi app/config/database.php
4) パーミション変更
	chmod -R 777 app/tmp/
	
5) 投稿スクリプト設定
	cp app/script/config.php.sample app/script/config.php
	vi app/script/config.php
	
■4■実行方法
1) 投稿スクリプト
	cd app/script/
	php gohan_import_new.php
