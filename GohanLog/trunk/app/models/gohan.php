<?php

class Gohan extends AppModel
{
    var $name = 'Gohan';

    function getDateList()
    {
        $conditions = '';
        $fields = array('DISTINCT DATE_FORMAT(created, "%Y-%m-%d")');
        $order = 'created DESC';
        $list = $this->find($conditions, $fields, $order);

        $datelist = array();
        foreach ($list as $date) {
            $datelist[] = array_shift(array_shift($date));
        }

        unset($list);

        return $datelist;
    }
    function save_img($image_data){
        $md5 = md5($image_data);
        {
            $conditions = 'checksum = '."'$md5'";
            $fields = '';
            $order = '';
            $result = $this->find($conditions, $fields, $order);
        }
        if($result){
            return;
        }
        {
            $time = time();
            $newfilename = $md5."_".$time.".jpg";
            $path = DIR_NAME_SAVE.'/'.$newfilename;
            $this->log('path='.$path);
        }
        $handle = fopen($path, 'a');
        fwrite($handle, $image_data);
        fclose($handle);
        chmod($path, 0755);
        
        return $path;
    }

    function add_gohan($path){
    	$info = pathinfo($path);
        $filename = $info['basename'];
		$exif = exif_read_data($path, 0, true);
		$exif_time = $exif['EXIF']['DateTimeOriginal'];
		$filetime = $this->exiftime2unixtime($exif_time);
		$date_string = date ("Y-m-d H:i:s", $filetime);
		$md5 = $this->path2md5($path);
		$data = array(
                'Gohan' => array(
                'filename' => $filename,
                'checksum' => $md5,
                'date_time_original' => $date_string,
                )
        );
        $res = $this->create();
        $res = $this->save($data);
        $this->log('res='.$res);
        
        return $path;
    }
    function exiftime2unixtime( $exiftime ){
        $head = substr($exiftime,0,10);
        $head = str_replace(":","/",$head);
        $gnutime = substr_replace ($exiftime, $head, 0,10);
        return strtotime($gnutime);
    }
    
    function path2md5($path){
        $fp = fopen($path, "rb");
		$image_data = fread($fp, filesize($path));
		fclose($fp);
		$md5 = md5($image_data);
		return $md5;
    }
}

?>