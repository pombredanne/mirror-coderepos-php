<?php

class UsersController extends AppController
{
    var $name = 'Users';
	var $components = array('Pager','Openid');
	var $helpers = array('Html','Form');
	
    function login()
    {
        $returnTo = 'http://'.$_SERVER['SERVER_NAME'].'/users/login';

        if (!empty($this->data)) {
            try {
                $this->Openid->authenticate($this->data['OpenidUrl']['openid'], $returnTo, 'http://'.$_SERVER['SERVER_NAME']);
            } catch (InvalidArgumentException $e) {
                $this->setMessage('Invalid OpenID');
            } catch (Exception $e) {
                $this->setMessage($e->getMessage());
            }
        } elseif (count($_GET) > 1) {
            $response = $this->Openid->getResponse($returnTo);

            if ($response->status == Auth_OpenID_CANCEL) {
                $this->setMessage('Verification cancelled');
            } elseif ($response->status == Auth_OpenID_FAILURE) {
                $this->setMessage('OpenID verification failed: '.$response->message);
            } elseif ($response->status == Auth_OpenID_SUCCESS) {
                $this->Session->write('openid.identity', $response->signed_args['openid.identity']);
                $this->Session->setFlash('認証成功');
                $this->redirect('/users/login');
            }
        }
    }
    function setting()
    {
        if(!empty($this->data)) {
            $openid_identity = $this->Session->read('openid.identity');
            $nickname = $this->data['User']['nickname'];
            $this->User->add_user($openid_identity,$nickname);
            $this->Session->setFlash('更新成功');
            $this->redirect('/users/setting');
        }
    }
    function logout()
    {
        $this->Session->delete('openid.identity');
    }
    private function setMessage($message) {
        $this->set('message', $message);
    }
    
}

?>