CREATE TABLE gohans (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `filename` text NOT NULL,
    `checksum` text NOT NULL,
    `date_time_original` datetime NOT NULL default '0000-00-00 00:00:00',
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE users (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `openid_identity` varchar(128) NOT NULL default '',
    `nickname` text NOT NULL,
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL,
  UNIQUE KEY `openid_identity` (`openid_identity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE tags (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `target_table` varchar(128) NOT NULL default '',
    `target_id` int(11) NOT NULL default '0',
    `name` varchar(128) NOT NULL default '',
    `user_id` int(11) NOT NULL default '0',
    `master_tag_id` int(11) NOT NULL default '0',
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
