ALTER TABLE tags ADD COLUMN master_tag_id INTEGER NOT NULL DEFAULT 0 AFTER `modified`;

CREATE TABLE master_tags (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `name` varchar(128) NOT NULL default '',
    `user_id` int(11) NOT NULL default '0',
    created DATETIME DEFAULT NULL,
    modified DATETIME DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO master_tags (name) SELECT name from tags group by name;
UPDATE tags,master_tags SET tags.master_tag_id = master_tags.id WHERE master_tags.name = tags.name;
ALTER TABLE `tags` DROP COLUMN `name`;

