<?php
/**
 * PHP$B%W%m!*$N(Bnews$B$G2?2sB>$N%K%e!<%9%5%$%H$+$i(B
 * $B0zMQ$7$F$$$k$+%A%'%C%/$9$k%9%/%j%W%H(B
 *
 * @author halt feits <halt.feits@gmail.com>
 *
 */
require_once 'HTTP/Client.php';

$g_target_url = 'http://www.phppro.jp/news/';
$g_start = 215;

function getNews($url)
{
    $matches = array();

    $client = new HTTP_Client();
    $client->get($url);
    $response = $client->currentResponse();
    $pattern = '/id=\"news-\">(.*?)<!-- <\/dl> -->/s';
    preg_match($pattern, $response['body'], $matches);
    return $matches[1];
}

$quoted_count = array();
$news_site_words = array('gigazine', 'phpspot', 'hotphpper');
for ($i = 1; $i <= $g_start; $i++) {

    print("get {$g_target_url}{$i}\n");

    $body = getNews($g_target_url . $i);
    sleep(1); //wait 1 sec

    foreach ($news_site_words as $keyword) {
        if (stripos($body, $keyword) !== false) {
            $quoted_count[$i] = $keyword;
        }
    }
}
$php = "<?php " . var_export($quoted_count, true) . "; ?>";
file_put_contents('quoted_count.php', $php);
?>
