<?php
//php -f moetenWizard.php
require_once 'Zend/Http/Client.php';
require_once 'Zend/Validate/LessThan.php';
require_once 'Diggin/Debug.php'; //http://diggin.googlecode.com/svn/trunk/library/Diggin/Debug.php

class Diggin_Console_Interactive
{
    /**
     * @param string $xmlStr
     * @param string $xpathValue
     * @param string $xpathQuery
     * @param string $showMessage
     * @return string
     */
    function select ($xmlStr, $xpathValue, $xpathQuery, $showMessage = "選択してください")
    {
        
        $iterator = new SimpleXMLIterator($xmlStr);
        $find = $iterator->xpath($xpathValue);
        $hit = count($find);
        
        foreach ($find as $key => $value){
            echo mb_convert_encoding($key, 'SJIS', 'utf8');
            echo ":";
            echo mb_convert_encoding($value, 'SJIS', 'utf8');
            echo "\n";
        }  
        
        $validator = new Zend_Validate_LessThan($hit);
        
        while (TRUE) {
            echo mb_convert_encoding($showMessage." ", 'SJIS', 'utf8');
          
            $input = trim(fgets(STDIN));
    
            if ($validator->isValid($input)) {
                $key = $iterator->xpath($xpathQuery);
                $return = (string) $key[$input];            
                break;
            } else {
                foreach ($validator->getMessages() as $message) {
                    echo "$message\n";
                }
            }
        }
        
        return $return;
    }
    
    /**
     * Y(yes) or N (no) 
     *
     * @param string
     * @return boolean
     */
    function yesNo ($showMessage)
    {
        
        while (TRUE) {
            echo mb_convert_encoding($showMessage." ", 'SJIS', 'utf8');
          
            $input = strtolower(trim(fgets(STDIN)));
    
            if (strcmp($input, 'y') === 0){
                $boolean =TRUE;
                break;
            } elseif (strcmp($input, 'n') === 0) {
                $boolean =FALSE;
                break;
            }
            
        }
        
        return $boolean;
    }
}

$client = new Zend_Http_Client();
$interactive = new Diggin_Console_Interactive();
$moeten ='http://moeten.info/maidcafe/';

$query = array("m"=>"api");

//if ($interactive->yesNo("情報のタイプを指定しますか (y/N)")) {
    @$query["type"] = "list";
    $client->setUri($moeten."?".http_build_query($query));
    @$query["type"] = $interactive->select($client->request()->getBody(), '/channel/item/type', '/channel/item/keytype');
    $client->setUri($moeten."?".http_build_query($query));
//}
if ($interactive->yesNo("都道府県名を指定しますか (y/N)")) {
    @$query["tid"] = "list";
    $client->setUri($moeten."?".http_build_query($query));
    @$query["tid"] = $interactive->select($client->request()->getBody(), '/channel/item/todouhuken', '/channel/item/tid');
    $client->setUri($moeten."?".http_build_query($query));
}
if ($interactive->yesNo("カテゴリを指定しますか (y/N)")) {
    @$query["cid"] = "list";
    $client->setUri($moeten."?".http_build_query($query));
    @$query["cid"] = $interactive->select($client->request()->getBody(), '/channel/item/category', '/channel/item/cid');
    $client->setUri($moeten."?".http_build_query($query));
}
if ($interactive->yesNo("ショップを指定しますか (y/N)")) {
    @$query["sid"] = "list";
    $client->setUri($moeten."?".http_build_query($query));
    @$query["sid"] = $interactive->select($client->request()->getBody(), '/channel/item/shopname', '/channel/item/sid');
    $client->setUri($moeten."?".http_build_query($query));
}

//print_r($client->getUri());
Diggin_Debug::dump(strip_tags($client->request()->getBody()));