<?php
class OptionPricing {

    static $S;
    static $C;
    static $type;
    static $ep; 
    static $n;
    static $R;
    static $P;

    /**
     * @param $type EuropeanType(0) or AmericanType(1)
     * @param $op original price
     * @param $ep exercise price of option
     * @param $vo price volatility
     * @param $t  period of time (year)
     * @param $n  parted of time
     * @param $r  interest rates
     * @param $y  earnings yield
     * @return
     */
    static function calc($type, $op, $ep, $vo = .1, $t = 1.0, $n = 35, $r = 0, $y = 0) {
        self::$type = $type;
        self::$S = self::$C = array();
        self::$S[1] = $op;

        self::$ep = $ep;
        self::$n = $n;

        $u = exp($vo * sqrt($t / self::$n));
        $d = 1 / $u;
        self::$R = exp($r * $t / self::$n);
        self::$P = (exp(($r - $y) * $t / self::$n) - $d) / ($u - $d);

        for ($i=2;$i<=self::$n+1;$i++) {
            $pp = self::pos($i - 1);
            $p =  self::pos($i);
            for ($j=1;$j<=$i-1;$j++) {
               self::$S[$p+$j] =  self::$S[$pp+$j] * $u;
            }
            self::$S[$p+$j] = self::$S[$pp+$j-1] * $d;
        }

        return self::back_calc(1, 1);
     }
     
     static private function pos($n) {
        return ($n * ($n - 1) / 2) ;
     }

     static private function back_calc($k,$l) {
 
         $p = self::pos($k) + $l;
         if (isset(self::$C[$p])) {
             return self::$C[$p];
         }
         if ($k> self::$n) {
             self::$C[$p] = max(self::$S[$p] - self::$ep, 0);
             return self::$C[$p];
         }
         $pp = self::pos($k+1) + $l;
         if (!isset(self::$C[$pp])) {
             self::$C[$pp] = self::back_calc($k+1, $l);
         }
         $Cu = self::$C[$pp];
         $pp = self::pos($k+1) + $l + 1;
         if (!isset(self::$C[$pp])) {
             self::$C[$pp] = self::back_calc($k+1, $l+1);
         }
         $Cd = self::$C[$pp];
         $c = (self::$P * $Cu + (1 - self::$P) * $Cd) / self::$R;
         if (self::$type == 1) {
             // AmericanType
             $c = max($c, self::$S[$p] - self::$ep);
         }
         self::$C[$p] = $c;
         return $c;
     }
}


