<?php

require_once "HTTP/Request.php";
require_once "XML/Unserializer.php";

define( "BASIC_USER_NAME", "kefir" );
define( "BASIC_PASSWORD", "" );
define( "USER_AGENT", "PHP/" . phpversion());

$timeline = getTimeLine();
if( !$timeline ) die();

$timeline = mb_convert_encoding( $timeline, "UTF-8", "HTML-ENTITIES" );

$send_to = array();

$us = new XML_Unserializer(
	array(
		"complexType" => "array"
	)
);

$result = $us->unserialize( $timeline, false );
if( $result )
{
	$data = $us->getUnserializedData();
	
	if( !isset( $data['status'][0] ) )
	{
		$data['status'] = array( $data['status'] );
	}
	
	foreach( $data['status'] as $item )
	{
		if( isset( $item['text'] ) &&
			preg_match( '/(?:か|ね|の)[?？]/', $item['text'] ) )
		{
			$send_to[] = $item['user']['screen_name'];
		}
	}
}

if( sizeof( $send_to ) > 0 )
{
	$req =& new HTTP_Request(
		"http://twitter.com/statuses/update.xml",
		array(
			"method" => "POST",
			"http" => "1.1",
			"timeout" => 3
		)
	);
	$req->addHeader( "User-Agent", USER_AGENT );
	$req->addHeader( "Content-Type", "application/x-www-form-urlencoded" );
	$req->setBasicAuth( BASIC_USER_NAME, BASIC_PASSWORD );
	
	foreach( $send_to as $user )
	{
		$pad = str_repeat( "　", rand(1, 10) );
		$status = "@".$user." いいえ、ケフィアです。".$pad;
		
		$req->addPostData( "status", $status );
		$response = $req->sendRequest();
	}
}

function getTimeLine()
{
	$url = 'http://twitter.com/statuses/friends_timeline.xml';
	
	$time = ( isset( $_SERVER['REQUEST_TIME'] ) ? $_SERVER['REQUEST_TIME'] : time() ) - 60 * 3;
	$date = gmdate( DATE_RFC1123, $time );
	
	$req =& new HTTP_Request( $url );
	$req->addHeader( 'User-Agent', USER_AGENT );
	$req->addHeader( 'If-Modified-Since', $date );
	$req->setBasicAuth( BASIC_USER_NAME, BASIC_PASSWORD );
	$req->addQueryString( 'since', $date );
	$req->addQueryString( 'count', '200' );
	$response = $req->sendRequest();
	
	if( PEAR::isError( $response ) )
	{
		return false;
	} else {
		return $req->getResponseBody();
	}
}


?>
