<?php

/**
 * @author nowelium
 */
interface HermitSqlCreator {
    public function createSql(PDO $pdo);
}
