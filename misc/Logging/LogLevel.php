<?php

class LogLevel {

    const OFF_INT = 0x00;
    const FATAL_INT = 0x01;
    const ERROR_INT = 0x04;
    const WARN_INT = 0x08;
    const INFO_INT = 0x10;
    const DEBUG_INT = 0x20;
    const ALL_INT = 0x2F;

    public static $OFF;
    public static $FATAL;
    public static $ERROR;
    public static $WARN;
    public static $INFO;
    public static $DEBUG;
    public static $ALL;

    private $name;
    private $level;

    private function __construct($name, $level){
        $this->name = $name;
        $this->level = $level;
    }

    public static function init(){
        self::$OFF = new self('OFF', self::OFF_INT);
        self::$FATAL = new self('FATAL', self::FATAL_INT);
        self::$ERROR = new self('ERROR', self::ERROR_INT);
        self::$WARN = new self('WARN', self::WARN_INT);
        self::$INFO = new self('INFO', self::INFO_INT);
        self::$DEBUG = new self('DEBUG', self::DEBUG_INT);
        self::$ALL = new self('ALL', self::ALL_INT);
    }

    public function getName(){
        return $this->name;
    }

    public function getLevel(){
        return $this->level;
    }

    public function toInt(){
        return $this->level;
    }

    public function __toString(){
        return $this->name;
    }
}

LogLevel::init();
