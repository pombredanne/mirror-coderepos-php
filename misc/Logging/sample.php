<?php

require_once 'Logger.php';
require_once 'LoggingEvent.php';
require_once 'LogLevel.php';
require_once 'Appender.php';
require_once 'LogLayout.php';
require_once dirname(__FILE__) . '/layout/SimpleLayout.php';
require_once dirname(__FILE__) . '/appender/ConsoleAppender.php';
require_once dirname(__FILE__) . '/appender/SyslogAppender.php';

$logger = new Logger;
//$logger->debug('%s %s', 'hello', 'world');
$logger->addAppender(new ConsoleAppender);

$logger->debug('%s %s', 'hello', 'world');

$syslogAppender = new SyslogAppender('logsample', 0, SyslogFacility::LOG_USER);
$logger->addAppender($syslogAppender);
$logger->info('%s %d', 'hoge', 777);
