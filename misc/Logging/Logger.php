<?php

class Logger {

    private $name;
    private $level;
    private $appenders = array();
    private $starttime;
    
    public function __construct($name = '') {
        $this->name = $name;
        $this->level = LogLevel::$OFF;
        $this->starttime = new DateTime;
    }
    
    public function setLevel(LogLevel $level) {
        $this->level = $level;
    }

    public function getName(){
        return $this->name;
    }
    
    public function getLevel() {
        return $this->level;    
    }

    public function getStartTime() {
        return $this->starttime;
    }
    
    public function log(LogLevel $level, $format, array $params) {
        if($level->toInt() <= $this->level->toInt()){
            return ;
        }
        $c = count($this->appenders);
        if($c < 1){
            return ;
        }

        $message = vsprintf($format, $params);
        $event = new LoggingEvent($this, $level, $message);
        for ($i = 0; $i < $c; ++$i) {
            $this->appenders[$i]->append($event);
        }
    }

    public function addAppender(Appender $appender) {
        $this->appenders[] = $appender;
    }

    public function fatal(){
        $args = func_get_args();
        $this->log(LogLevel::$FATAL, array_shift($args), $args);
    }
    public function error(){
        $args = func_get_args();
        $this->log(LogLevel::$ERROR, array_shift($args), $args);
    }
    public function warn(){
        $args = func_get_args();
        $this->log(LogLevel::$WARN, array_shift($args), $args);
    }
    public function info(){
        $args = func_get_args();
        $this->log(LogLevel::$INFO, array_shift($args), $args);
    }
    public function debug(){
        $args = func_get_args();
        $this->log(LogLevel::$DEBUG, array_shift($args), $args);
    }
}

?>
