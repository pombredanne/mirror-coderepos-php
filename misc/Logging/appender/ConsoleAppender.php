<?php

class ConsoleAppender implements Appender {
    private $layout;
    public function __construct(LogLayout $layout = null){
        if($layout === null){
            $this->layout = new SimpleLayout;
        } else {
            $this->layout = $layout;
        }
    }

    public function append(LoggingEvent $event){
        echo $this->layout->format($event);
    }
}
