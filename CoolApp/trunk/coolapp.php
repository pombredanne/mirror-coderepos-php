<?php
/**
 * CoolApp.php
 * CoolApp
 *
 * 2008/06/16
 */

$app = new CoolApp();
$app->service();

/**
 * 未定義のクラスを作るときに、同名のPHPファイルを読み込みます。
 *
 * @param $name クラス名/ファイル名
 */
function __autoload($name)
{
	$inc = BIN_DIR.DS.$name.'.php';
	if (is_file($inc)) require_once($inc);
	$inc = LIB_DIR.DS.$name.'.php';
	if (is_file($inc)) require_once($inc);
}

/**
 * CoolApp コア
 */
class CoolApp
{
	private $methods = array();

	public function CoolApp()
	{
		define('APP_DIR', 'app');
		define('BIN_DIR', 'bin');
		define('LIB_DIR', 'lib');
		define('RSRC_DIR', 'rsrc');
		define('DS', DIRECTORY_SEPARATOR);
		define('ROOT', dirname(__FILE__));
		define('WEBROOT_DIR', 'webroot');
		define('WWW_ROOT', ROOT . DS . APP_DIR . DS . WEBROOT_DIR . DS);

		// デフォルトメソッドを登録
		$this->methods['GET'] = $this->doGet;
	}

	public function service()
	{
		$req = new CoolRequest();

		$args = $req->getArgs();
		$app_name = array_shift($args);
		if (isset($app_name) && !empty($app_name))
		{
//			print "call: $app_name";
			$app = new $app_name;
//			$app->hello();
		}
		else
		{
			$app = $this;
		}
		$html = RSRC_DIR.DS.$app_name.'.html';

		$res = new CoolResponse($html);
		$method = 'on'.strtolower($req->getMethod());
//		print "(method $method)";
//		$method = 'hello';
		$callback = array($app, $method);
		if (is_callable($callback))
		{
			call_user_func($callback, $req, $res);
			$res->show();
		}
	}

	protected function onget($args)
	{
	}

	protected function onpost($args)
	{
	}

	protected function onput($args)
	{
	}

	protected function ondelete($args)
	{
	}
}

/**
 * リクエストを処理します。
 */
class CoolRequest
{
	private $app_base;

	private $app_name;

	private $app_args;

	private $app_opts;

	public function CoolRequest()
	{
		// PATH_INFO の前後に分割します
		$path_info = $this->getPathInfo();
		if (isset($path_info))
		{
			list($this->app_base, $this->app_opts) = split($path_info, $this->getRequestURI(), 2);
		}

		// PATH_INFO を '/' で分割します
		$this->app_args = split('/', $this->getPathInfo());
		array_shift($this->app_args);

		// APP_BASE を設定します
		$this->app_name = $this->app_args[0];
		$this->app_base = $this->app_base.'/'.$this->app_name;

		// 先頭の '?' を削除します
		if (substr($this->app_opts, 0, 1) == '?') $this->app_opts = substr($this->app_opts, 1);
		parse_str($this->app_opts, $this->app_opts);
	}

	public function getMethod()
	{
		return $_SERVER['REQUEST_METHOD'];
	}

	public function getRequestURI()
	{
		return $_SERVER['REQUEST_URI'];
	}

	public function getPathInfo()
	{
		return $_SERVER['PATH_INFO'];
	}

	public function getAppName()
	{
		return $this->app_name;
	}

	public function getArgs()
	{
		return $this->app_args;
	}

	public function getOptions()
	{
//		return $_REQUEST;
		return $this->app_opts;
	}

	public function createLink($path)
	{
//		return $this->app_base.'/'.$path;
		$args = func_get_args();
		array_unshift($args, $this->app_base);
		$path = join('/', $args);
		return $path;
	}
}

/**
 * レスポンスを処理します。
 */
class CoolResponse
{
	private $file;

	private $status = 200;

	private $params = array();

	public function CoolResponse($file)
	{
		$this->file = $file;
		$this->params = $_REQUEST;
	}

	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function put($key, $value)
	{
		$this->params[$key] = $value;
	}

	public function get($key, $def = null)
	{
		$value = $this->params[$key];
		if ($def != null && empty($value))
		{
			$value = $def;
			$this->put($key, $value);
		}
		return $value;
	}

	public function getContents()
	{
		$tmpl = file_get_contents($this->file);
		return $tmpl;
	}

	public function show()
	{
		// テンプレートを読み込む
		$tmpl = $this->getContents();

		// 変数を展開する
//		$search = array('$hello');
//		$replace = array('へろーへろー');
		$search = array_keys($this->params);
		$search = array_map(create_function('$value', 'return "$$value";'), $search);
		$replace = array_values($this->params);
		$tmpl = str_replace($search, $replace, $tmpl);

		// 出力する
		if (false)
		{
			print "<h3>WebResponse::show()</h3>";
			print "<p>RES: ".print_r($res, TRUE)."</p>";
			print "<p>METHODS: ".print_r($this->methods, TRUE)."</p>";
			print "<pre><code>";
			var_dump($this);
			print "</code></pre>";
			print "<p>ROOT: ".ROOT."</p>";
		}
		print $tmpl;
	}
}

?>
