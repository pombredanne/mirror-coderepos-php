<?php
require_once '../HTML/CSS/Mobile.php';

$document = file_get_contents('sample.html');
try {
	echo HTML_CSS_Mobile::getInstance()->setBaseDir('./')->setMode('strict')->apply($document);
} catch (RuntimeException $e) {
	var_dump($e);
} catch (Exception $e) {
	var_dump($e->getMessage());
}

