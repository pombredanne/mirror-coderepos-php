<?php
/**
 * HatenaSyntax
 *
 * PHP version 5.3
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author    anatoo <study.anatoo@gmail.com> 
 * @license   http://www.opensource.org/licenses/mit-license.php The MIT License
 * @version   0.0.6
 * @link      http://d.hatena.ne.jp/anatoo/
 */
namespace Guevara::Text::Parser::Hatena;
require_once 'Guevara/Text/Parser/Hatena/FirstCharSyntaxInterface.php';
use Guevara::Text::Parser::Hatena::FirstCharSyntaxInterface as FirstCharSyntaxInterface;

class ListSyntax implements FirstCharSyntaxInterface
{
  protected $list = array();
  protected $identifier;
  public function __construct($orderedFlag = false)
  {
    $this->identifier = $orderedFlag ? '+' : '-';
  }
  public function getIdentifier()
  {
    return $this->identifier;
  }
  public function parse($line)
  {
    $level = $this->countLevel($line);
    $list =& $this->list;
    
    for($i = 0; $i < $level; $i++) {
      if(count($list) > 0) {
        if(!is_array($list[count($list) - 1])) $list[] = array();
        $list =& $list[count($list) - 1];
      }
      else {
        $list =& $list[];
      }
    }
    
    $list[] = substr($line, $level);
  }
  /**
   * $lineの先頭の+,-の深さを数える
   */
  public function countLevel($line)
  {
    $level = 0;
    $len = strlen($line);
    for($i = 0; $i < $len; $i++) {
      if($line[$i] === '-' || $line[$i] === '+') $level++;
      else break;
    }
    
    return $level - 1;
  }
  
  public function getResult()
  {
    $result = $this->buildList($this->list);
    $this->list = array();
    
    return $result;
  }
  
  /**
   * 配列からリストを表現するタグを生成
   */
  public function buildList($arr)
  {
    if(count($arr) > 0 && !is_array($arr[0])) $tagName = (substr($arr[0], 0, 1) === '+') ? 'ol': 'ul';
    else $tagName = $this->identifier === '+' ? 'ol': 'ul';
    
    $result = '<' . $tagName . '>' . PHP_EOL;
    foreach($arr as $item) {
      if(is_array($item)) $result .= $this->buildList($item);
      else {
        $item = substr($item, 1);
        $result .= '<li>' . $item . '</li>' . PHP_EOL;
      }
    }
    $result .= '</' . $tagName . '>' . PHP_EOL;
    return $result;
  }
}