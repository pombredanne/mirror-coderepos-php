<?php
/**
 * HatenaSyntax
 *
 * PHP version 5.3
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @author    anatoo <study.anatoo@gmail.com> 
 * @license   http://www.opensource.org/licenses/mit-license.php The MIT License
 * @version   0.0.6
 * @link      http://d.hatena.ne.jp/anatoo/
 */
namespace Guevara::Text::Parser;

require_once 'Guevara/Text/Parser/Hatena/Head.php';
require_once 'Guevara/Text/Parser/Hatena/Table.php';
require_once 'Guevara/Text/Parser/Hatena/DefinitionList.php';
require_once 'Guevara/Text/Parser/Hatena/DefaultSyntax.php';
require_once 'Guevara/Text/Parser/Hatena/Blockquote.php';
require_once 'Guevara/Text/Parser/Hatena/ListSyntax.php';
require_once 'Guevara/Text/Parser/Hatena/Pre.php';
require_once 'Guevara/Text/Parser/Hatena/Footnote.php';
require_once 'Guevara/Text/Parser/Hatena/Link.php';

use Guevara::Text::Parser::Hatena::FirstCharSyntaxInterface as FirstCharSyntaxInterface;
use Guevara::Text::Parser::Hatena::MarkupSyntaxInterface as MarkupSyntaxInterface;
use Guevara::Text::Parser::Hatena::InlineSyntaxInterface as InlineSyntaxInterface;

use Guevara::Text::Parser::Hatena::Head as Head;
use Guevara::Text::Parser::Hatena::Head as Table;
use Guevara::Text::Parser::Hatena::DefinitionList as DefinitionList;
use Guevara::Text::Parser::Hatena::DefaultSyntax as DefaultSyntax;
use Guevara::Text::Parser::Hatena::Blockquote as Blockquote;
use Guevara::Text::Parser::Hatena::ListSyntax as ListSyntax;
use Guevara::Text::Parser::Hatena::Pre as Pre;
use Guevara::Text::Parser::Hatena::Footnote as Footnote;
use Guevara::Text::Parser::Hatena::Link as Link;


class Hatena
{
  protected $firstCharSyntaxes = array();
  protected $inlineSyntaxes = array();
  protected $footnoteSyntax;
  protected $markupSyntaxes = array();
  protected $openingTags = array();
  protected $closingTags = array();
  protected $options;
  
  public function __construct($options = array())
  {
    if(!is_array($options)) throw new Exception('argument is not array.');
    
    $this->options = $options;
    
    // 汎用文法の追加
    // 汎用といってもDefaultとBlockquoteはちょっと特殊です
    // 理由はgetFirstCharSyntaxIdentifierを参照
    $this->addFirstCharSyntax(new Head($this->getOption('headlevel', 3)))
         ->addFirstCharSyntax(new Table())
         ->addFirstCharSyntax(new DefinitionList())
         ->addFirstCharSyntax(new DefaultSyntax())
         ->addFirstCharSyntax(new Blockquote(false))
         ->addFirstCharSyntax(new Blockquote(true))
         ->addFirstCharSyntax(new ListSyntax())
         ->addFirstCharSyntax(new ListSyntax(true));
    
    $this->addMarkupSyntax(new Pre(false, $this->getOption('htmlescape', false)))
         ->addMarkupSyntax(new Pre(true));
    
    $this->addInlineSyntax(new Link());
    
    // 非汎用文法
    // applyInlineSyntaxでついでにこの文法も適用される。
    $this->footnoteSyntax = new Footnote($this->getOption('id', ''));
  }
  protected function getOption($key, $default = false)
  {
    if(!isset($this->options[$key])) return $default;
    return $this->options[$key];
  }
  public function addMarkupSyntax(MarkupSyntaxInterface $syntax)
  {
    $this->markupSyntaxes[$syntax->getOpeningIdentifier()] = $syntax;
    $this->openingTags[$syntax->getOpeningIdentifier()] = true;
    $this->closingTags[$syntax->getClosingIdentifier()] = true;
    
    return $this;
  }
  public function addFirstCharSyntax(FirstCharSyntaxInterface $syntax)
  {
    $this->firstCharSyntaxes[$syntax->getIdentifier()] =& $syntax;
    return $this;
  }
  public function addInlineSyntax(InlineSyntaxInterface $syntax)
  {
    $this->inlineSyntaxes[] =& $syntax;
    return $this;
  }
  public function parseStructure($contents)
  {
    $openingTags =& $this->openingTags;
    $closingTags =& $this->closingTags;
    $markupSyntaxes =& $this->markupSyntaxes;
    $firstCharSyntaxes =& $this->firstCharSyntaxes;
    
    $lines = $this->getLines($contents);
    $result = array();
    for($i = 0, $length = count($lines); $i < $length; $i++) {
      $block = array('lines' => array());
      
      // ブロック文法の種類の確認
      // MarkupSyntaxだったら
      if(isset($openingTags[$lines[$i]])) {
        $block['syntax'] =& $markupSyntaxes[$lines[$i]];
        $block['type'] = 'markup';
        
        for($i++; $i < $length; $i++) {
          if(isset($closingTags[$lines[$i]])) break;
          $block['lines'][] = $lines[$i];
        }
      }
      // FirstCharSyntaxだったら
      else {
        $firstChar = $this->getFirstCharSyntaxIdentifier($lines[$i]);
        
        $block['syntax'] =& $firstCharSyntaxes[$firstChar];
        $block['type'] = 'firstchar';
        
        for(; $i < $length; $i++) {
          if($firstChar !== $this->getFirstCharSyntaxIdentifier($lines[$i])) break;
          $block['lines'][] = $lines[$i];
        }
        $i--;
      }
      
      $result[] = $block;
    }
    
    return $result;
  }
  public function parse($contents) {
    $structure = $this->parseStructure($contents);
    $result = array();
    $htmlEscape = $this->getOption('htmlescape');
    
    $result[] = '<div class="section">' . PHP_EOL;
    foreach($structure as $block) {
      if($block['type'] === 'markup')
        foreach($block['lines'] as $line) $block['syntax']->parse($line);
      else
        foreach($block['lines'] as $line) {
          if($htmlEscape) $line = htmlspecialchars($line, ENT_QUOTES);
          $line = $this->applyInlineSyntax($line);
          $block['syntax']->parse($line);
        }
      
      $result[] = $block['syntax']->getResult();
    }
    $result[] = '</div>' . PHP_EOL . PHP_EOL;
    
    $result[] = $this->getFootnoteSyntaxResult();
    
    return implode('', $result);
  }
  public function getLines($contents)
  {
    return split("\r\n|\n|\r", $contents);
  }
  public function getFootnoteSyntaxResult()
  {
    return $this->footnoteSyntax->getResult();
  }
  public function applyInlineSyntax($line)
  {
    foreach($this->inlineSyntaxes as $syntax) {
      $line = $syntax->parse($line);
    }
    $line = $this->footnoteSyntax->parse($line);
    return $line;
  }
  protected function getFirstCharSyntaxIdentifier($line)
  {
    if($line === '<<') return '<<';
    elseif($line === '>>') return '>>';
    
    $char = substr($line, 0, 1);
    
    if(isset($this->firstCharSyntaxes[$char])) return $char;
    
    if(isset($this->openingTags[ $line])) return false;
    
    return 'default';
  }
}
