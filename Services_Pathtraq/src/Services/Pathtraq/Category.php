<?php
final class Services_Pathtraq_Category
{
    const POLITICS = 'politics';
    const BUSINESS = 'business';
    const SOCIETY = 'society';
    const SHOWBIZ = 'showbiz';
    const MUSIC = 'music';
    const MOVIE = 'movie';
    const ANIME = 'anime';
    const GAME = 'game';
    const SPORTS = 'sports';
    const MOTOR = 'motor';
    const EDUCATION = 'education';
    const READING = 'reading';
    const SCIENCE = 'science';
    const ART = 'art';
    const FOODS = 'foods';
    const TRAVEL = 'travel';
    const MOBILE = 'mobile';
    const COMPUTER = 'computer';
    const WEB = 'web';

    public static function has($key)
    {
        return defined('Services_Pathtraq_Category::' . strtoupper($key));
    }
}
