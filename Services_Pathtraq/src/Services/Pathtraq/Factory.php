<?php

/**
 * The factory class for Services_Pathtraq
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Pathtraq
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.1
 * @link      http://pear.php.net/package/Services_Pathtraq
 */

/**
 * The factory class for Services_Pathtraq
 *
 * @category  Services
 * @package   Services_Pathtraq
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.php.net/package/Services_Pathtraq
 * @see http://pathtraq.com/developer
 */
class Services_Pathtraq_Factory {

    /**
     * 'news' API
     */
    const API_NEWS_RANKING = 'news_ranking';

    /**
     * 'category' API
     */
    const API_CATEGORY_RANKING = 'category_ranking';

    /**
     * 'keyword' API
     */
    const API_KEYWORD_SEARCH = 'keyword_search';

    /**
     * 'normalise url' API
     */
    const API_NORMALIZE_URL = 'normalize_url';

    /**
     * 'page counter' API
     */
    const API_PAGE_COUNTER = 'page_counter';

    /**
     * 'page chart' API
     */
    const API_PAGE_CHART = 'page_chart';


    /**
     * constructor
     *
     * @return void
     * @access private
     */
    private function __construct() {
    }

    /**
     * create and return an service instance
     *
     * @param  string    $api    the API
     * @return mixed     an service instance
     * @access public
     * @throws Exception throws Exception if any errors occur
     * @static
     */
    public static function getInstance($api) {
        if (self::has($api)) {
            $filename = '';
            foreach (split('_', $api) as $name) {
                $filename .= ucfirst($name);
            }
            require_once 'Services/Pathtraq/' . $filename . '.php';
            $classname = 'Services_Pathtraq_' . $filename;
            if (class_exists($classname)) {
                return new $classname();
            } else {
                throw new Exception('missing the class "' . $classname . '"');
            }
        } else {
            throw new Exception('Invalid API "' . $api . '"');
        }
    }

    public static function has($key)
    {
        return defined('Services_Pathtraq_Factory::API_' . strtoupper($key));
    }
}
