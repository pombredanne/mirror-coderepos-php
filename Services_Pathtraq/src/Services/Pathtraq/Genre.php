<?php
final class Services_Pathtraq_Genre
{
    const NATIONAL = 'national';
    const SPORTS = 'sports';
    const BUSINESS = 'business';
    const POLITICS = 'politics';
    const INTERNATIONAL = 'international';
    const ACADEMIC = 'academic';
    const CULTURE = 'culture';

    public static function has($key)
    {
        return defined('Services_Pathtraq_Genre::' . strtoupper($key));
    }
}
