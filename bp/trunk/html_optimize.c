/*
   +----------------------------------------------------------------------+
   | PHP version 4.0                                                      |
   +----------------------------------------------------------------------+
   | Copyright (c) 1997, 1998, 1999, 2000, 2001 The PHP Group             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 2.02 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available at through the world-wide-web at                           |
   | http://www.php.net/license/2_02.txt.                                 |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
   | Authors:                                                             |
   |                                                                      |
   +----------------------------------------------------------------------+
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "ext/standard/info.h"
#include "php_bp.h"

#include "html_optimize.h"

#define LIST_NEST_MAX 256

/*
HTMLを最適化する
 */
PHP_FUNCTION(bp_html_optimize) {
	unsigned char *html = NULL, *html_now = NULL, *html_now_p1 = NULL, *html_now_p2 = NULL, *html_now_p3 = NULL;
	size_t buff_size =  1024 * 4 * sizeof( char);
	long bp_level = 0;
	zval **parm_html_array = NULL, **parm_bp_level = NULL;

	int i;
	int parts;/* 配列数 */

	zval **list;/* 配列の構造 */
	zval **list_tmp;/* 配列の要素のテンポラリ */
	zval **element;/* 属性配列 */
	unsigned char *text;/* テキスト */
	unsigned char *tag_name;/* タグ名 */
	unsigned char last_tag_name[32];/* 直前のタグ名 */

	int tag_elements = 0;
	int f_close = 0;

	int f_pre = 0;/* PREタグの中にいるか */
	int f_frameset = 0;
    int f_script = 0;
    int f_style = 0;
	int f_a = 0;
	int f_select = 0;
	int f_textarea = 0;

	int list_next = 0;/* UL OL DL DIR にてネストされている数 */
	char list_next_char[LIST_NEST_MAX];/* 現在 どのタグが対称になっているかを示す 
							  * u = UL/ o = OL/ d = DL/ u = DIR
							  */
	int list_next_num[LIST_NEST_MAX];/* 現在 リストが何番目かを示す */

	int f_last_tag_level = 0, f_last_tag_level_t = 0;/* 直前のタグの影響力
								center/div/marquee/form = 1;
								H?/P/FORM = 2
							 */


    int i_tmp_1;
	unsigned char *c_tmp_1, *c_tmp_2;

	last_tag_name[0] = '\0';
	list_next_char[0] = '\0';

	/*
	  引数を取得
	 */
	if( ZEND_NUM_ARGS() != 2) {
		zend_error( E_WARNING, "Use bp_html_optimize( $html_array, $bp_level)");
		WRONG_PARAM_COUNT;		
	}
	if( zend_get_parameters_ex( 2, &parm_html_array, &parm_bp_level) == FAILURE) {
				WRONG_PARAM_COUNT;
	}
	convert_to_long_ex( parm_bp_level);
	bp_level = Z_LVAL_PP( parm_bp_level);

	/*
	  バッファのメモリ確保
	 */
	html = emalloc( buff_size);
	if( html == NULL) {
		zend_error( E_WARNING, "html_optimize emalloc Error");
		return;
	}
	html_now = html;
	*html_now = '\0';

	/*
	  配列を解析
	 */
	parts = zend_hash_num_elements( Z_ARRVAL_PP( parm_html_array));
	for( i = 0; i < parts; i++) {
		zend_hash_index_find( Z_ARRVAL_PP( parm_html_array), i, (void **) &list);

		zend_hash_find( Z_ARRVAL_PP( list), "text", 5, (void **) &list_tmp);
		text = Z_STRVAL_PP( list_tmp);

		while(1){
			/* バッファが足りなくなりそうなら事前にメモリ確保  */
			if( buff_size < ( html_now - html) + Z_STRLEN_PP( list_tmp) + 16){
				i_tmp_1 = html_now - html;/* reallocによってメモリの番地が変わる可能性がある */
				buff_size *= 2;
				html = erealloc( html, buff_size);
				if( html == NULL) {
					zend_error( E_WARNING, "html_optimize erealloc Error");
					return;
				}
				html_now = ( html + i_tmp_1);
			}else{
				break;
			}
		}

		zend_hash_find( Z_ARRVAL_PP( list), "f_type", 7, (void **) &list_tmp);

		if( Z_LVAL_PP( list_tmp) == BP_IS_TAG) {
			/* タグ解析開始  */
			zend_hash_find( Z_ARRVAL_PP( list), "f_close", 8, (void **) &list_tmp);		
			f_close = Z_LVAL_PP( list_tmp);
			/* コメント */
			if( f_close == 2)
				continue;
			zend_hash_find( Z_ARRVAL_PP( list), "tag_name", 9, (void **) &list_tmp);
			tag_name = Z_STRVAL_PP( list_tmp);
			zend_hash_find( Z_ARRVAL_PP( list), "elements", 9, (void **) &list_tmp);
			tag_elements = Z_LVAL_PP( list_tmp);
			zend_hash_find( Z_ARRVAL_PP( list), "element", 8, (void **) &list_tmp);		
			element = Z_ARRVAL_PP( list_tmp);

			/*
			 * タグ毎の処理
			 */
			f_last_tag_level_t = 0;
			if( bp_level) {
				switch ( tag_name[0]) {
				case 'a':
					if( tag_name[1] == '\0') {
						/* A */
						*html_now++ = '<';
						if( f_close) {
							f_a--;
							*html_now++ = '/';
						}
						bp_stradd( html_now, tag_name);
						if( ! f_close) {
							f_a++;
							bp_make_tag_element( a, html_now, element, tag_elements, bp_level);
						}
						*html_now++ = '>';
					}
					break;
				case 'b':
					if( tag_name[1] == 'r' && tag_name[2] == '\0' && ! f_close) {
						/* BR */
						*html_now++ = '\n';
						f_last_tag_level_t = 1;
					} else if( strcmp( tag_name, "body") == 0 && ! f_close) {
						/* BODY */
						html_now_p1 = html_now;
						*html_now++ = '<';
						bp_stradd( html_now, tag_name);
						html_now_p2 = html_now;
						bp_make_tag_element( body, html_now, element, tag_elements, bp_level);
						html_now_p3 = html_now;
						*html_now++ = '>';
						if( html_now_p2 == html_now_p3) html_now = html_now_p1;
					} else if( strcmp( tag_name, "blink") == 0) {
						/* BLINK */
						*html_now++ = '<';
						if( f_close)
							*html_now++ = '/';
						bp_stradd( html_now, tag_name);
						*html_now++ = '>';
					}

					break;
				case 'c':
					if( strcmp( tag_name, "center") == 0) {
						/* CENTER 直前にH?/P/FORM/center/div/marquee/formがあるときは前改行しない */
						if( bp_level == 10) {
							*html_now++ = '<';
							if( f_close)
								*html_now++ = '/';
							bp_stradd( html_now, tag_name);
							*html_now++ = '>';
						} else {
							if( f_last_tag_level < 1)
								*html_now++ = '\n';
						}
						f_last_tag_level_t = 1;
					}
					break;
				case 'd':
					if( tag_name[1] == 'i' && tag_name[2] == 'v' && tag_name[3] == '\0') {
						/* DIV 直前にH?/P/FORM/ center/div/marquee/formがあるときは前改行しない */
						if( bp_level == 10) {
							*html_now++ = '<';
							if( f_close)
								*html_now++ = '/';
							bp_stradd( html_now, tag_name);
							if( ! f_close)
								bp_make_tag_element( div, html_now, element, tag_elements, bp_level);
							*html_now++ = '>';
						} else {
							if( f_last_tag_level < 1)
								*html_now++ = '\n';
						}
						f_last_tag_level_t = 1;
					} else if( tag_name[1] == 'l' && tag_name[2] == '\0' && bp_level < 40) {
						/* DL */
						if( f_close && list_next > 0){
							list_next--;
						} else {
							list_next++;
							if( list_next < LIST_NEST_MAX) {
								/* ネスト制限以内 */
								list_next_num[list_next] = 0;
								list_next_char[list_next] = 'd';
							}
						}
						if( f_last_tag_level < 1)
							*html_now++ = '\n';
						f_last_tag_level_t = 1;
					} else if( ( tag_name[1] == 't' || tag_name[1] == 'd') && tag_name[2] == '\0' && bp_level < 40) {
						/* DT DD */
						*html_now++ = '\n';
						if( f_last_tag_level < 1)
							*html_now++ = '\n';
						f_last_tag_level_t = 1;
					} else if( tag_name[1] == 'i' && tag_name[2] == 'r' && tag_name[2] == '\n' && bp_level < 40) {
						/* DIR */
						if( f_close && list_next > 0){
							list_next--;
						} else {
							list_next++;
							if( list_next < LIST_NEST_MAX) {
								/* ネスト制限以内 */
								list_next_num[list_next] = 0;
								list_next_char[list_next] = 'u';
							}
						}
						if( f_last_tag_level < 1)
							*html_now++ = '\n';
						f_last_tag_level_t = 1;
					}

					break;
				case 'e':
					break;
				case 'f':
					if( strcmp( tag_name, "form") == 0) {
						/* FORM 直前にH?/P/FORMがあるとき改行1つ 改行2つ */
						if( f_last_tag_level == 2 && *(html_now - 1) == '\n')
							html_now--;

						*html_now++ = '<';
						if( f_close)
							*html_now++ = '/';
						bp_stradd( html_now, tag_name);
						if( ! f_close)
							bp_make_tag_element( form, html_now, element, tag_elements, bp_level);
						*html_now++ = '>';
						f_last_tag_level_t = 2;
					} else if( strcmp( tag_name, "frameset") == 0) {
						/* framesetタグ */
						if( f_close) {
							f_frameset--;
						} else {
							f_frameset++;
						}
					} else if( strcmp( tag_name, "font") == 0 && bp_level == 10) {
						/* FONT */
						*html_now++ = '<';
						if( f_close)
							*html_now++ = '/';
						bp_stradd( html_now, tag_name);
						if( ! f_close)
							bp_make_tag_element( font, html_now, element, tag_elements, bp_level);
						*html_now++ = '>';
					}
					break;
				case 'g':
					break;
				case 'h':
					if( ( tag_name[1] >= '1' && tag_name[1] <= '7') && tag_name[2] == '\0' && bp_level < 40) {
						/* H* 直前にH?/P/FORMがあるときは前改行しない */
						if( f_last_tag_level < 2)
							*html_now++ = '\n';
						*html_now++ = '\n';
						f_last_tag_level_t = 2;
					}else if( tag_name[1] == 'r' && tag_name[2] == '\0' && ! f_close && bp_level < 40) {
						/* HR 直前にH?/P/FORM/center/div/marquee/formがあるときは前改行しない */
						if( strcmp( last_tag_name, "hr") != 0) {
							if( bp_level == 10) {
								*html_now++ = '<';
								bp_stradd( html_now, tag_name);
								bp_make_tag_element( hr, html_now, element, tag_elements, bp_level);
								*html_now++ = '>';
							} else {
								if( f_last_tag_level < 1)
									*html_now++ = '\n';
								*html_now++ = '-';
								*html_now++ = '\n';
							}
						}
						f_last_tag_level_t = 1;
					}
					break;
				case 'i':
					if( ( strcmp( "image", tag_name) == 0 || tag_name[1] == 'm' && tag_name[2] == 'g' && tag_name[3] == '\0') && ! f_close) {
						/* IMG */
						if( bp_level == 10) {
							html_now_p1 = html_now;
							*html_now++ = '<';
							if( tag_name[2] != 'g') {
								/* image */
								*html_now++ = 'i';
								*html_now++ = 'm';
								*html_now++ = 'g';
							} else {
								bp_stradd( html_now, tag_name);
							}
							html_now_p2 = html_now;
							bp_make_tag_element( img, html_now, element, tag_elements, bp_level);
							html_now_p3 = html_now;
							*html_now++ = '>';
							if( html_now_p2 == html_now_p3) html_now = html_now_p1;
						} else if( f_a) {
							/* A タグに囲まれている時のみALTを取り出す */
							c_tmp_1 = bp_get_element( element, tag_elements, "alt", 0, &i_tmp_1);
							if( c_tmp_1 == NULL) {
								/* ALTがないのでファイル名 */
								c_tmp_1 = bp_get_element( element, tag_elements, "src", 0, &i_tmp_1);
								if( c_tmp_1 == NULL) {
									/* ファイル名も無い */
								} else {
									/* ファイル名追加 */
									c_tmp_2 = c_tmp_1;
									c_tmp_1 += strlen( c_tmp_1) - 1;
									while(1){
										if( *c_tmp_1 == '/') {
											c_tmp_1++;
											break;
										}
										if( c_tmp_1 == c_tmp_2) break;
										c_tmp_1--;
									}
									*html_now++ = '[';
									bp_stradd( html_now, c_tmp_1);
									*html_now++ = ']';
								}
							} else {
								/* ALT 追加 */
								unsigned char *c_tmp_img = estrdup( c_tmp_1);
								*html_now++ = '[';
								bp_text_copy( html_now, c_tmp_img, 0, 40);
								*html_now++ = ']';
								//if( c_tmp_img != NULL)
								//	efree( c_tmp_img);
							}
						}
					} else if( strcmp( tag_name, "input") == 0 && ! f_close) {
						/* INPUT */
						*html_now++ = '<';
						bp_stradd( html_now, tag_name);
						bp_make_tag_element( input, html_now, element, tag_elements, bp_level);
						*html_now++ = '>';
					}
					break;
				case 'j':
					break;
				case 'k':
					break;
				case 'l':
					if( tag_name[1] == 'i' && tag_name[2] == '\0' && ! f_close && bp_level < 40) {
						/* LI */
						if( f_last_tag_level < 1)
							*html_now++ = '\n';

						if( list_next < LIST_NEST_MAX && list_next > 0) {
							/* リスト処理中 */
							if( list_next_char[list_next] == 'u') {
								/* UL DIR */
								*html_now++ = 129;
								*html_now++ = 69;
								//*html_now++ = 149;
							} else if( list_next_char[list_next] == 'o') {
								/* OL */
								list_next_num[list_next]++;
								i_tmp_1 = zend_sprintf( html_now, "%d", list_next_num[list_next]);
								html_now += i_tmp_1;
								*html_now++ = '.';
							} else {
								/* 単発 */
								*html_now++ = 129;
								*html_now++ = 155;
								//*html_now++ = 149;
							}
						} else {
							/* 単発 */
							*html_now++ = 129;
							*html_now++ = 155;
							//*html_now++ = 149;
						}
						f_last_tag_level_t = 1;
					}
					break;
				case 'm':
					if( strcmp( tag_name, "marquee") == 0) {
						/* MARQUEE 直前にH?/P/FORM/center/div/marquee/formがあるときは前改行しない */
						if( f_last_tag_level == 1 && *(html_now - 1) == '\n')
							html_now--;

						if( bp_level <= 20) {
							*html_now++ = '<';
							if( f_close)
								*html_now++ = '/';
							bp_stradd( html_now, tag_name);
							if( ! f_close)
								bp_make_tag_element( marquee, html_now, element, tag_elements, bp_level);
							*html_now++ = '>';
						} else {
							if( f_last_tag_level < 1)
								*html_now++ = '\n';
						}
					}
					break;
				case 'n':
					break;
				case 'o':
					if( strcmp( tag_name, "option") == 0 && ! f_close) {
						/* OPTION */
						*html_now++ = '<';
						bp_stradd( html_now, tag_name);
						bp_make_tag_element( option, html_now, element, tag_elements, bp_level);
						*html_now++ = '>';
					} else if( strcmp( tag_name, "object") == 0 && bp_level < 40) {
						/* OBJECT */
						*html_now++ = '<';
						if( f_close)
							*html_now++ = '/';
						bp_stradd( html_now, tag_name);
						if( ! f_close)
							bp_make_tag_element( object, html_now, element, tag_elements, bp_level);
						*html_now++ = '>';
					} else if( tag_name[1] == 'l' && tag_name[2] == '\0' && bp_level < 40) {
						/* OL */
						if( f_close && list_next > 0){
							list_next--;
						} else {
							list_next++;
							if( list_next < LIST_NEST_MAX) {
								/* ネスト制限以内 */
								list_next_num[list_next] = 0;
								list_next_char[list_next] = 'o';
							}
						}
						if( f_last_tag_level < 1)
							*html_now++ = '\n';
						f_last_tag_level_t = 1;
					}

					break;
				case 'p':
					if( tag_name[1] == '\0') {
						/* P 直前にH?/P/FORMがあるときは改行しない 改行2つ */
						if( bp_level == 10) {
							*html_now++ = '<';
							if( f_close)
								*html_now++ = '/';
							bp_stradd( html_now, tag_name);
							if( ! f_close)
								bp_make_tag_element( p, html_now, element, tag_elements, bp_level);
							*html_now++ = '>';
						} else {
							if( f_last_tag_level < 2)
								*html_now++ = '\n';
							*html_now++ = '\n';
						}
						f_last_tag_level_t = 2;
					}else if( strcmp( tag_name, "pre") == 0) {
						/* pre タグ */
						if( f_close) {
							f_pre--;
						} else {
							f_pre++;
						}
						if( f_last_tag_level < 1)
							*html_now++ = '\n';
						*html_now++ = '\n';
						f_last_tag_level_t = 2;
					} else if( strcmp( tag_name, "param") == 0 && bp_level < 40) {
						/* PARAM */
						*html_now++ = '<';
						if( f_close)
							*html_now++ = '/';
						bp_stradd( html_now, tag_name);
						if( ! f_close)
							bp_make_tag_element( param, html_now, element, tag_elements, bp_level);
						*html_now++ = '>';
					}
					break;
				case 'q':
					break;
				case 'r':
					break;
				case 's':
					if( strcmp( tag_name, "script") == 0) {
						/* scriptタグ */
						if( f_close) {
							f_script--;
						} else {
							f_script++;
						}
					} else if( strcmp( tag_name, "style") == 0) {
						/* STYLE */
						if( f_close) {
							f_style--;
						} else {
							f_style++;
						}
					} else if( strcmp( tag_name, "select") == 0) {
						/* SELECT */
						*html_now++ = '<';
						if( f_close){
							f_select--;
							*html_now++ = '/';
						}
						bp_stradd( html_now, tag_name);
						if( ! f_close){
							f_select++;
							bp_make_tag_element( select, html_now, element, tag_elements, bp_level);
						}
						*html_now++ = '>';
					}
					break;
				case 't':
					if( tag_name[1] == 'r' && tag_name[2] == '\0' && f_close) {
						/* TR */
						*html_now++ = '\n';
					} else if( strcmp( tag_name, "title") == 0) {
						/* TITLE */
						*html_now++ = '<';
						if( f_close)
							*html_now++ = '/';
						bp_stradd( html_now, tag_name);
						*html_now++ = '>';
					} else if( strcmp( tag_name, "textarea") == 0) {
						/* TEXTAREA */
						*html_now++ = '<';
						if( f_close){
							f_textarea--;
							*html_now++ = '/';
						}
						bp_stradd( html_now, tag_name);
						if( ! f_close){
							f_textarea++;
							bp_make_tag_element( textarea, html_now, element, tag_elements, bp_level);
						}
						*html_now++ = '>';
					}

					break;
				case 'u':
					if( tag_name[1] == 'l' && tag_name[2] == '\0' && bp_level < 40) {
						/* UL */
						if( f_close && list_next > 0){
							list_next--;
						} else {
							list_next++;
							if( list_next < LIST_NEST_MAX) {
								/* ネスト制限以内 */
								list_next_num[list_next] = 0;
								list_next_char[list_next] = 'u';
							}
						}
						if( f_last_tag_level < 1)
							*html_now++ = '\n';
						f_last_tag_level_t = 1;
					}
					break;
				case 'v':
					break;
				case 'w':
					break;
				case 'x':
					break;
				case 'y':
					break;
				}
				strncpy( last_tag_name, tag_name, 32);			
			} else {
				/* ほぼそのまま(スクリプトのみ無効化) */
				switch ( tag_name[0]) {
				case 's':
					if( strcmp( tag_name, "script") == 0) {
						/* scriptタグ */
						if( f_close) {
							f_script--;
						} else {
							f_script++;
						}
					} else if( strcmp( tag_name, "style") == 0) {
						/* STYLE */
						if( f_close) {
							f_style--;
						} else {
							f_style++;
						}
					} else {
						if( strcmp( tag_name, "select") == 0) {
							if( f_close) {
								f_select--;
							} else {
								f_select++;
							}
						}
						*html_now++ = '<';
						if( f_close)
							*html_now++ = '/';
						bp_stradd( html_now, tag_name);
						bp_make_tag_element_all( html_now, element, tag_elements);
						*html_now++ = '>';
					}
					break;
				case 't':
					if( strcmp( tag_name, "textarea") == 0) {
						if( f_close) {
							f_textarea--;
						} else {
							f_textarea++;
						}
					}
					*html_now++ = '<';
					if( f_close)
						*html_now++ = '/';
					bp_stradd( html_now, tag_name);
					bp_make_tag_element_all( html_now, element, tag_elements);
					*html_now++ = '>';					
					break;
				default:
					*html_now++ = '<';
					if( f_close)
						*html_now++ = '/';
					bp_stradd( html_now, tag_name);
					bp_make_tag_element_all( html_now, element, tag_elements);
					*html_now++ = '>';
					break;
				}
			}
			f_last_tag_level = f_last_tag_level_t;
		} else if( Z_LVAL_PP( list_tmp) == BP_IS_TEXT) {
			/* ただの文字列 */
			c_tmp_1 = html_now;
			if( ! f_script && ! f_style && ! f_frameset){
				/* フレームやスクリプトタグ style内は無視 */
				if( f_select || f_textarea) {
					/* SELECT/TEXTAREA タグの中 */
					bp_stradd( html_now, text);
				} else {
					bp_text_copy( html_now, text, f_pre, bp_level);
				}
			}
			c_tmp_2 = html_now;
			if( c_tmp_1 != c_tmp_2){
				/* 文字が追加できたらフラグをクリアする */
				f_last_tag_level = 0;
				last_tag_name[0] = '\0';
			}
		}
	}

	*html_now = '\0';

   	RETVAL_STRING( html, 1);
	efree( html);
	return;
}


/*
 * 属性配列の指定された属性のポインタを戻す
 * name=value型でない場合は、属性名のポインタを戻す
 */
unsigned char *bp_get_element( HashTable *element, int elements, char *name, int no_value, int *in_esc) {
	int i;
	zval **list, **data;
    *in_esc = 0;

	for( i = 0; i < elements; i++) {
		zend_hash_index_find( element, i, (void **) &list);
		zend_hash_find( Z_ARRVAL_PP( list), "name", 5, (void **) &data);
		if( strcmp( Z_STRVAL_PP( data), name) == 0) {
			zend_hash_find( Z_ARRVAL_PP( list), "f_novalue", 10, (void **) &data);
			if( no_value == 1 || ( Z_LVAL_PP( data) == 0 && no_value == 0)) {
				zend_hash_find( Z_ARRVAL_PP( list), "in_esc", 7, (void **) &data);
				*in_esc = Z_LVAL_PP( data);
				if( no_value) {
					/*  属性名のみ */
					return name;
				} else {
					/* 属性値も */
					zend_hash_find( Z_ARRVAL_PP( list), "value", 6, (void **) &data);
					return Z_STRVAL_PP( data);
				}
			}
		}
	}

	return NULL;
}



/*
  上記関数で戻されるデータフォーマット
  
  struct z_html_array {
    int f_type;//タグかただのテキストか  1 = タグ / 2 = テキスト
	char *text;//解析された文字列の全体
	char *tag_name;//タグの名前が入る(a br inputとか)
	int f_close;//閉じタグの場合は1が入り、コメントなら2で、それ以外は0
	int elements;//属性の数
	int values;//name=value形式の属性の数
	struct element {//属性
	  char *name;//属性名
	  char *value;//値
	  int f_novalue;//name=value形式の属性でない場合は1
	  int in_esc;//valueの各種記号が入っているかどうか、入っていればそれぞれbit単位でフラグが立つ
	  //" 1
	  //' 2
	  //< 4
	  //> 8
	  //スペース 16
	  //TAB 32
	  //\n  64
	  //\r 128
	  //
	  //
	}
  }
 */



/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: sw=4 ts=4 tw=78 fdm=marker
 * vim<600: sw=4 ts=4 tw=78
 */


