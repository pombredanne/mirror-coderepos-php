/*
   +----------------------------------------------------------------------+
   | PHP version 4.0                                                      |
   +----------------------------------------------------------------------+
   | Copyright (c) 1997, 1998, 1999, 2000, 2001 The PHP Group             |
   +----------------------------------------------------------------------+
   | This source file is subject to version 2.02 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available at through the world-wide-web at                           |
   | http://www.php.net/license/2_02.txt.                                 |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
   | Authors:                                                             |
   |                                                                      |
   +----------------------------------------------------------------------+
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "ext/standard/info.h"
#include "php_bp.h"



/* If you declare any globals in php_bp.h uncomment this:
ZEND_DECLARE_MODULE_GLOBALS(bp)
*/

/* True global resources - no need for thread safety here */
static int le_bp;

/* {{{ bp_functions[]
 *
 * Every user visible function must have an entry in bp_functions[].
 */
function_entry bp_functions[] = {
	PHP_FE(bp_html_parse,	NULL)
	PHP_FE(bp_html_optimize,NULL)
	{NULL, NULL, NULL}	/* Must be the last line in bp_functions[] */
};
/* }}} */

/* {{{ bp_module_entry
 */
zend_module_entry bp_module_entry = {
	STANDARD_MODULE_HEADER,
	"bp Html Parser",
	bp_functions,
	PHP_MINIT(bp),
	PHP_MSHUTDOWN(bp),
	PHP_RINIT(bp),		/* Replace with NULL if there's nothing to do at request start */
	PHP_RSHUTDOWN(bp),	/* Replace with NULL if there's nothing to do at request end */
	PHP_MINFO(bp),
    "0.1", /* Replace with version number for your extension */
	STANDARD_MODULE_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_BP
ZEND_GET_MODULE(bp)
#endif

/* {{{ PHP_INI
 */
/* Remove comments and fill if you need to have entries in php.ini
PHP_INI_BEGIN()
    STD_PHP_INI_ENTRY("bp.value",      "42", PHP_INI_ALL, OnUpdateInt, global_value, zend_bp_globals, bp_globals)
    STD_PHP_INI_ENTRY("bp.string", "foobar", PHP_INI_ALL, OnUpdateString, global_string, zend_bp_globals, bp_globals)
PHP_INI_END()
*/
/* }}} */

/* {{{ php_bp_init_globals
 */
/* Uncomment this function if you have INI entries
static void php_bp_init_globals(zend_bp_globals *bp_globals)
{
	bp_globals->value = 0;
	bp_globals->string = NULL;
}
*/
/* }}} */

/* {{{ PHP_MINIT_FUNCTION
 */
PHP_MINIT_FUNCTION(bp)
{
	/* If you have INI entries, uncomment these lines 
	ZEND_INIT_MODULE_GLOBALS(bp, php_bp_init_globals, NULL);
	REGISTER_INI_ENTRIES();
	*/


	/*
	  定数を設定する
	 */
	/* それぞれ、大文字小文字を区別する*/
	REGISTER_LONG_CONSTANT( "BP_IS_TAG" , BP_IS_TAG , CONST_CS);
	REGISTER_LONG_CONSTANT( "BP_IS_TEXT", BP_IS_TEXT, CONST_CS);

	return SUCCESS;
}
/* }}} */

/* {{{ PHP_MSHUTDOWN_FUNCTION
 */
PHP_MSHUTDOWN_FUNCTION(bp)
{
	/* uncomment this line if you have INI entries
	UNREGISTER_INI_ENTRIES();
	*/
	return SUCCESS;
}
/* }}} */

/* Remove if there's nothing to do at request start */
/* {{{ PHP_RINIT_FUNCTION
 */
PHP_RINIT_FUNCTION(bp)
{
	return SUCCESS;
}
/* }}} */

/* Remove if there's nothing to do at request end */
/* {{{ PHP_RSHUTDOWN_FUNCTION
 */
PHP_RSHUTDOWN_FUNCTION(bp)
{
	return SUCCESS;
}
/* }}} */

/* {{{ PHP_MINFO_FUNCTION
 */
PHP_MINFO_FUNCTION(bp)
{
	php_info_print_table_start();
	php_info_print_table_header(2, "bp support", "enabled");
	php_info_print_table_row( 2, "function", "bp_html_parse( $html)");
	php_info_print_table_end();

	/* Remove comments if you have entries in php.ini
	DISPLAY_INI_ENTRIES();
	*/
}
/* }}} */


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: sw=4 ts=4 tw=78 fdm=marker
 * vim<600: sw=4 ts=4 tw=78
 */


