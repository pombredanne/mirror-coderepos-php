<?php
/**
 * Services_HTML2PDF
 *
 * @author halt feits <halt.feits at gmail.com>
 * @package Services_HTML2PDF
 */

require_once 'PEAR.php';

/**
 * Services_HTML2PDF
 *
 * @author halt feits <halt.feits at gmail.com>
 * @package Services_HTML2PDF
 */
class Services_HTML2PDF
{
    /**
     * convert
     *
     * @access public
     * @param string $url
     * @param string $type
     */
    function convert($url, $type = 'PDF')
    {
        $type = strtoupper($type);

        $send_url = "http://html2pdf.biz/api?url={$url}&ret={$type}";

        $support_type = array(
            'JSON',
            'PDF',
            'PNG'
        );

        if (!in_array($type, $support_type)) {
            return PEAR::raiseError("invalid supprt type : {$type}");
        }

        return call_user_func(array('Services_HTML2PDF', "convert{$type}"), $send_url);
    }

    /**
     * fetch
     *
     * @access protected
     * @param string $url
     * @return string
     */
    function fetch($url)
    {
        if (ini_get('allow_url_fopen') == 1) {
            $result = file_get_contents($url);
        } else {
            //implement you.
            //include_once 'HTTP/Request.php';
            //untara kantara
            return PEAR::raiseError("allow_url_fopen disabled");
        }

        if (strlen($result) < 200 && (strpos($result, "ERROR:") !== false)) {
            return PEAR::raiseError(trim($result));
        }

        return $result;
    }

    function convertJSON($url)
    {
        $result = Services_HTML2PDF::fetch($url);

        if (PEAR::isError($result)) {
            return $result;
        }

        if (function_exists('json_decode')) {
            return json_decode($result);
        } else {
            //implement you.
            //include_once 'Jsphon/Jsphon.php';
            //untara kantara
            return PEAR::raiseError("json_decode not found.");
        }
    }

    function convertPNG($url)
    {
        return Services_HTML2PDF::fetch($url);
    }

    function convertPDF($url)
    {
        return Services_HTML2PDF::fetch($url);
    }

}
?>
