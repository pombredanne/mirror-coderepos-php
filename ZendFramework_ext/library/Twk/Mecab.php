<?php
// require_once 'Twk/Process.php';

/**
 * Mecab executor
 * 
 * Call proc_open for mecab command each time (so this is slow)
 * Add mecab or mecab.exe path, or pass the path to the constructor
 * 
 * Requires Twk_Process and mb extension
 * 
 * @see http://mecab.sourceforge.net/
 * 
 * @author twk (http://nonn-et-twk.net/twk/)
 * @version 20080628
 */
class Twk_Mecab {
	private static $_defaultCommand = null;
	private static $_nowinDefaultCommand = 'mecab';
	private static $_winDefaultCommand = 'mecab.exe';
	
	// The dictionary default encoding is EUC-JP in Unix and Shift_JIS in Windows
	// @see http://mecab.sourceforge.net/#charset
	private static $_defaultEncoding = null;
	private static $_nowinDefaultEncoding = 'EUC-JP';
	private static $_winDefaultEncoding = 'Shift_JIS';
	
	private $_command;
	private $_encoding;

	/**
	 * @param Zend_Config $options path and encoding
	 */
	public function __construct(Zend_Config $options = null)
	{
		if (!is_null($options))
		{
			$this->_command = $options->get('path', self::getDefaultCommand());
			$this->_encoding = $options->get('encoding', self::getDefaultEncoding());
		}
		else
		{
			$this->_command = self::getDefaultCommand();
			$this->_encoding = self::getDefaultEncoding();
		}
	}
	
	/**
	 * @return String
	 */
	public static function getDefaultCommand()
	{
		return self::$_defaultCommand ? self::$_defaultCommand
			: (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' ? self::$_winDefaultCommand : self::$_nowinDefaultCommand);
	}
	
	/**
	 * @return String
	 */
	public static function getDefaultEncoding()
	{
		return self::$_defaultEncoding ? self::$_defaultEncoding
			: (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN' ? self::$_winDefaultEncoding : self::$_nowinDefaultEncoding);
	}
	
	/**
	 * @param String $defaultCommand
	 */
	public static function setDefaultCommand($defaultCommand)
	{
		self::$_defaultCommand = $defaultCommand;
	}

	/**
	 * @param String $defaultEncoding
	 */
	public static function setDefaultEncoding($defaultEncoding)
	{
		self::$_defaultEncoding = $defaultEncoding;
	}
	
	/**
	 * @param Zend_Config $options path and encoding
	 */
	public static function setDefaults(Zend_Config $options)
	{
		if (!is_null($options))
		{
			self::setDefaultCommand($options->get('path', self::getDefaultCommand()));
			self::setDefaultEncoding($options->get('encoding', self::getDefaultEncoding()));
		}
	}
	
	/**
	 * depends on `mecab -v` output format
	 * # mecab -v
	 * mecab of 0.97
	 * 
	 * @return float 0.97 from "mecab of 0.97"
	 */
	public function getVersion()
	{
		$strVersion = $this->exec('-v');
		return substr($strVersion, strrpos($strVersion, ' ') + 1);
	}

	/**
	 * get String for proc_open
	 *
	 * @return String
	 */
	private function _getShellCommand()
	{
		$command = $this->_command;
		// for space-including directory
		return strpos($command, ' ') === FALSE ? $command : '"' . $command. '"';
	}
	
	/**
	 * @return String
	 */
	public function getEncoding()
	{
		return $this->_encoding;
	}
	
	/**
	 * This is internal API.
	 * DO NOT CALL from the end user application
	 * Use Twk_MeCab_Tagger::parse or Twk_MeCab_Tagger::parseToNodes
	 */
	public function exec($strArg = '', $input = null)
	{
		// Mecab requires alnum in half pitch and katakana in full pitch
		$input = mb_convert_kana($input, 'KVa');
		$input = mb_convert_encoding($input, $this->_encoding);
		
		// for *Unix command line (single backslash to double backslashes)
		if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN')
			$strArg = mb_ereg_replace('\\\\', '\\\\', $strArg);
		
		$output = Twk_Process::open($this->_getShellCommand() . ' ' . $strArg, $input);
		$output = mb_convert_encoding($output, mb_internal_encoding(), $this->_encoding);
		// echo $output;
		
		return $output;
	}
}