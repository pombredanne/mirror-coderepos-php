<?php
/**
 * Requires mb_string environment
 * Not tested ;)
 * @todo Should use MobilePictogramConverter? http://php-develop.org/MobilePictogramConverter/
 * 
 * @author twk (http://nonn-et-twk.net/twk/)
 */
//require_once 'Zend/Filter/Interface.php';

/**
 * Japanese mobile phone pictograph "Emoji" cut filter
 * @see "http://ke-tai.org/blog/2007/11/30/sb_emoji_cut/"
 */
class Twk_Filter_AntiEmoji implements Zend_Filter_Interface
{
	/** SoftBank Web Code形式の正規表現。Fixed Array */
	private static $_softBankWebCodeRegex = '/[\x1B][\x24][G|E|F|O|P|Q][\x21-\x7E]+([\x0F]|$)/';
	
    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws Zend_Filter_Exception If filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
    	$value = $this->filterSjisEmoji($value);
    	$value = $this->filterSoftBankWebCode($value);
    	return $value;
    }

    /**
     * SJIS形式の絵文字の除去
     *
     * @param String $value
     * @return String
     * @see "http://ke-tai.org/blog/2007/11/28/simple_emojicut/"
     */
    public function filterSjisEmoji($value)
    {
    	$substrchar = mb_substitute_character();
    	
		mb_substitute_character('none');
		$value = mb_convert_encoding($value, 'SJIS', 'SJIS');
		
		mb_substitute_character($substrchar);
		
    	return $value;
    }
    
    /**
     * Softbank Web Codeの除去
     *
     * @param String $value
     * @return String
     * @see "http://ke-tai.org/blog/2007/11/30/sb_emoji_cut/"
     */
    public function filterSoftBankWebCode($value)
    {
		$arr = array();
		$matchCount = preg_match_all(self::$_softBankWebCodeRegex, $value, $arr);
		if (!$matchCount) return $value;
		
		// $arr[0]に対象絵文字が格納されるので、空文字で置換
		return str_replace($arr[0], array(), $value);
	}
}
