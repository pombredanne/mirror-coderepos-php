<?php
/**
 * @see Zend_Validate_NotEmpty
 */
require_once 'Zend/Validate/NotEmpty.php';


/**
 * Zend_Validate_NotEmpty with Japanese messages
 * 
 * @category   Twk
 * @package    Twk_Validate
 * @copyright  twk (http://nonn-et-twk.net/)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Twk_Validate_Ja_NotEmpty extends Zend_Validate_NotEmpty
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::IS_EMPTY => "値が空ですが、空ではない値が必要です"
    );
}
