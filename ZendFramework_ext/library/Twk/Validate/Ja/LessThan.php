<?php
/**
 * @see Zend_Validate_LessThan
 */
require_once 'Zend/Validate/LessThan.php';


/**
 * Zend_Validate_LessThan with Japanese messages
 * 
 * @category   Twk
 * @package    Twk_Validate
 * @copyright  twk (http://nonn-et-twk.net/)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Twk_Validate_Ja_LessThan extends Zend_Validate_LessThan
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_LESS => "「%value%」は %max% 未満ではありません"
    );
}
