<?php
/**
 * @see Zend_Validate_Int
 */
require_once 'Zend/Validate/Int.php';


/**
 * Zend_Validate_Int with Japanese messages
 * 
 * @category   Twk
 * @package    Twk_Validate
 * @copyright  twk (http://nonn-et-twk.net/)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Twk_Validate_Ja_Int extends Zend_Validate_Int
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_INT => "「%value%」は整数ではないようです"
    );
}
