<?php
/**
 * @see Zend_Validate_Ccnum
 */
require_once 'Zend/Validate/Ccnum.php';


/**
 * Zend_Validate_Ccnum with Japanese messages
 * 
 * @category   Twk
 * @package    Twk_Validate
 * @copyright  twk (http://nonn-et-twk.net/)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Twk_Validate_Ja_Ccnum extends Zend_Validate_Ccnum
{
    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::LENGTH   => "「%value%」は13桁から19桁の数字でなければなりません",
        self::CHECKSUM => "「%value%」はルーンアルゴリズムでのチェックに失敗しました"
    );
}
