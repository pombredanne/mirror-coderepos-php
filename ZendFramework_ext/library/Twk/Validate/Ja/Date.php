<?php
/**
 * @see Zend_Validate_Date
 */
require_once 'Zend/Validate/Date.php';


/**
 * Zend_Validate_Date with Japanese messages
 * 
 * @category   Twk
 * @package    Twk_Validate
 * @copyright  twk (http://nonn-et-twk.net/)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Twk_Validate_Ja_Date extends Zend_Validate_Date
{
    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_YYYY_MM_DD => "「%value%」が YYYY-MM-DD 形式ではありません",
        self::INVALID        => "「%value%」は正しい日付ではないようです",
    );
}