<?php
/**
 * @see Zend_Validate_Float
 */
require_once 'Zend/Validate/Float.php';


/**
 * Zend_Validate_Float with Japanese messages
 * 
 * @category   Twk
 * @package    Twk_Validate
 * @copyright  twk (http://nonn-et-twk.net/)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Twk_Validate_Ja_Float extends Zend_Validate_Float
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_FLOAT => "「%value%」は数値ではないようです"
    );
}
