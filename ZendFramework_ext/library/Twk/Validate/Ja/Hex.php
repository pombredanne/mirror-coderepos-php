<?php
/**
 * @see Zend_Validate_Hex
 */
require_once 'Zend/Validate/Hex.php';


/**
 * Zend_Validate_Hex with Japanese messages
 * 
 * @category   Twk
 * @package    Twk_Validate
 * @copyright  twk (http://nonn-et-twk.net/)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Twk_Validate_Ja_Hex extends Zend_Validate_Hex
{
    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_HEX => "「%value%」は16進文字列以外を含みます"
    );
}
