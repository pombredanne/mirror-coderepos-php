<?php
/**
 * @see Zend_Validate_Ip
 */
require_once 'Zend/Validate/Ip.php';


/**
 * Zend_Validate_Ip with Japanese messages
 * 
 * @category   Twk
 * @package    Twk_Validate
 * @copyright  twk (http://nonn-et-twk.net/)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class Twk_Validate_Ja_Ip extends Zend_Validate_Ip
{
    /**
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_IP_ADDRESS => "「%value%」は IP アドレスではないようです"
    );
}
