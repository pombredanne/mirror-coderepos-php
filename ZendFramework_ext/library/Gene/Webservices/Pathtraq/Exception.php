<?php
/**
 * Exception
 *
 * PHP version 5.2
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 *
 * @category Gene
 * @package Gene_Webservices
 * @version $id$
 * @copyright 2008 Heavens hell
 * @author Heavens hell <heavenshell.jp@gmail.com>
 * @license New BSD License
 */

require_once 'Zend/Exception.php';

/**
 * Exception
 *
 * @category Gene
 * @package Gene_Webservices
 * @version $id$
 * @copyright 2008 Heavens hell
 * @author Heavens hell <heavenshell.jp@gmail.com>
 * @license New BSD License
 */
class Gene_Webservices_Pathtraq_Exception extends Zend_Exception
{
}
