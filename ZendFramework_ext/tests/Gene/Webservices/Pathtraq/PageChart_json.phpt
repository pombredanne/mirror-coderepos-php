--TEST--
Pathtraq PageChart api test for json
--FILE--
<?php
require_once 'Base.php';
$pathtraq = new Gene_Webservices_Pathtraq();
$params = array(
    'api'   => 'json',
    'url'   => urlencode('http://narabete.com/'),
    'scale' => '24h'
);
$response = $pathtraq->pageChart($params);
var_dump($response instanceof Zend_Http_Response);
var_dump($response->getBody());
?>
--EXPECT--
bool(true)
string(166) "{ step: 1, plots: [ 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0 ], url: "http://narabete.com/", start: "Fri, 25 Jul 2008 17:00:00 GMT" }"
