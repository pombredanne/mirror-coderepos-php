--TEST--
Pathtraq PageCounter api test for json
--FILE--
<?php
require_once 'Base.php';
$pathtraq = new Gene_Webservices_Pathtraq();
$params = array(
    'api' => 'json',
    'm'   => 'upcoming',
    'url' => urlencode('http://cybozu.co.jp/')
);
$response = $pathtraq->pageCounter($params);
var_dump($response instanceof Zend_Http_Response);
var_dump($response->getBody());
?>
--EXPECT--
bool(true)
string(59) "{ count: 0, mode: "upcoming", url: "http://cybozu.co.jp/" }"
