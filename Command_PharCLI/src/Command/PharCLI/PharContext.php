<?php

/**
 * Command_PharCLI_PharContext
 *
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Command
 * @package   Command_PharCLI
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2008 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.1.0
 * @link      http://pear.doyouphp.jp/package/Command_PharCLI
 */

/**
 * Command_PharCLI_PharContext class
 *
 *
 * @category  Command
 * @package   Command_PharCLI
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2008 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.doyouphp.jp/package/Command_PharCLI
 */
class Command_PharCLI_PharContext {

    /**
     * an array of command options
     * @var    array
     * @access private
     */
    private $options;

    /**
     * command arguments
     * @var    array
     * @access private
     */
    private $args;

    /**
     * constructor
     *
     * @param  array  $options parsed parameters in array
     * @return void
     * @access public
     */
    public function __construct(array $options) {
        if (isset($options[0]) && is_array($options[1]) &&
            isset($options[0]) && is_array($options[1])) {
            $this->options = $options[0];
            $this->args = $options[1];
        }
    }

    /**
     * Short description for function
     *
     * @return object Return description (if any) ...
     * @access public
     */
    public function getPhar() {
        return new Phar($this->getFile());
    }

    /**
     * Short description for function
     *
     * @return string Return description (if any) ...
     * @access public
     */
    public function hasVersion() {
        return $this->_has('version');
    }

    /**
     * Short description for function
     *
     * @return string Return description (if any) ...
     * @access public
     */
    public function isVerbose() {
        return $this->_has('verbose');
    }

    /**
     * Short description for function
     *
     * @return unknown Return description (if any) ...
     * @access public
     */
    public function hasCommand() {
        return $this->hasVersion() ||
               $this->hasCreateCommand() ||
               $this->hasExtractCommand() ||
               $this->hasListCommand();
    }

    /**
     * Short description for function
     *
     * @return string Return description (if any) ...
     * @access public
     */
    public function hasCreateCommand() {
        return $this->_has('create');
    }

    /**
     * Short description for function
     *
     * @return string Return description (if any) ...
     * @access public
     */
    public function hasExtractCommand() {
        return $this->_has('extract');
    }

    /**
     * Short description for function
     *
     * @return string Return description (if any) ...
     * @access public
     */
    public function hasListCommand() {
        return $this->_has('list');
    }

    /**
     * Short description for function
     *
     * @return string Return description (if any) ...
     * @access public
     */
    public function hasCompressionGZ() {
        return $this->_has('gzip');
    }

    /**
     * Short description for function
     *
     * @return string Return description (if any) ...
     * @access public
     */
    public function hasCompressionBZIP2() {
        return $this->_has('bzip');
    }

    /**
     * Short description for function
     *
     * @return string Return description (if any) ...
     * @access public
     */
    public function hasChdir() {
        return $this->_has('directory');
    }

    /**
     * Short description for function
     *
     * @return string Return description (if any) ...
     * @access public
     */
    public function hasFile() {
        return $this->_has('file');
    }

    /**
     * Short description for function
     *
     * @return string Return description (if any) ...
     * @access public
     */
    public function hasStub() {
        return $this->_has('stub');
    }

    /**
     * Short description for function
     *
     * @param  unknown $option Parameter description (if any) ...
     * @return unknown Return description (if any) ...
     * @access private
     */
    private function _has($option) {
        return !is_null($this->_get($option));
    }

    /**
     * Short description for function
     *
     * @param  unknown $option Parameter description (if any) ...
     * @return unknown Return description (if any) ...
     * @access private
     */
    private function _get($option) {
        $had = null;
        if (array_key_exists($option, $this->options)) {
            $had = $this->options[$option];
        }
        return $had;
    }

    /**
     * Short description for function
     *
     * @return array  Return description (if any) ...
     * @access public
     */
    public function getOptions() {
        return $this->options;
    }

    /**
     * Short description for function
     *
     * @return unknown Return description (if any) ...
     * @access public
     */
    public function getArgs() {
        return $this->args;
    }

    /**
     * Short description for function
     *
     * @return string Return description (if any) ...
     * @access public
     */
    public function getFile() {
        return $this->_get('file');
    }

    /**
     * Short description for function
     *
     * @return string Return description (if any) ...
     * @access public
     */
    public function getDirectory() {
        return $this->_get('directory');
    }

    /**
     * Short description for function
     *
     * @return string Return description (if any) ...
     * @access public
     */
    public function getStub() {
        return $this->_get('stub');
    }
}