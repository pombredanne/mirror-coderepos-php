<?php

/**
 * Short description for file
 *
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Command
 * @package   Command_PharCLI
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2008 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.1.0
 * @link      http://pear.doyouphp.jp/package/Command_PharCLI
 */

/**
 * Description for require_once
 */
require_once 'Command/PharCLI/Command.php';

/**
 * Short description for class
 *
 *
 * @category  Command
 * @package   Command_PharCLI
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2008 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.doyouphp.jp/package/Command_PharCLI
 */
class Command_PharCLI_Command_Extract implements Command {

    /**
     * Short description for function
     *
     * @param  mixed     $context Parameter description (if any) ...
     * @return void
     * @access public
     * @throws Exception Exception description (if any) ...
     * @throws Exception Exception description (if any) ...
     */
    public function execute(Command_PharCLI_PharContext $context) {
        $phar = $context->getPhar();
        foreach (new RecursiveIteratorIterator($phar) as $file) {
            $filename = str_replace('phar://' . $context->getFile() . DIRECTORY_SEPARATOR, '', $file->getPathname());

            $dir = $context->getDirectory();
            if (dirname($filename) !== '.') {
                $dir .= DIRECTORY_SEPARATOR . dirname($filename);
            }
            if (!is_dir($dir) && !mkdir($dir, 0755, true)) {
                throw new Exception('failed to create directory ' . $dir);
            }
            if (!is_writable($dir)) {
                throw new Exception('failed to write file to ' . $dir);
            }
            $output_file = $context->getDirectory() . DIRECTORY_SEPARATOR . $filename;
            if ($context->isVerbose()) {
                echo $output_file . PHP_EOL;
            }
            if (!file_put_contents($output_file, file_get_contents($phar[$filename]))) {
                echo 'failed to create file ' . $output_file;
            }
        }
    }
}