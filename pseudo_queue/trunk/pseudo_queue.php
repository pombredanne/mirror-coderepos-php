<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

require_once('single_execution.php');

class pseudoQueue
{
    var $options = array('storage' => 'sqlite');
    var $storage = null;

    function pseudoQueue($options = array())
    {
        $this->options = array_merge($this->options, $options);

        $this->storage =& $this->createStorageInstance(
            $this->options['storage']);

        register_shutdown_function(array($this, 'callSubscriber'));
    }

    /*
     * THIS METHOD MUST BE OVERWRITTEN
     */
    function subscriber($key, $message)
    {
        // sample code
        switch ($key) {
        case 'foo':
            echo "foo: {$message}\n";
            break;
        case 'bar':
            echo "bar: {$message}\n";
            break;
        default:
            echo "invalid key\n";
            break;
        }
    }

    function callSubscriber()
    {
        $pid = extension_loaded('pcntl') ? pcntl_fork() : -1;
        if ($pid > 0) {
            return;
        }

        $single = new pseudoQueueSingleExecution(array('begin' => false));
        if (!$single->lock()) {
            return;
        }

        if ($pid === -1) {
            if ($queue = $this->storage->pop()) {
                return $this->subscriber($queue['key'], $queue['message']);
            }
        }

        while (true) {
            if ($queue = $this->storage->pop()) {
                $this->subscriber($queue['key'], $queue['message']);
            } else {
                sleep(1);
            }
        }
    }

    function camelize($string)
    {
        return join('', array_map(
            array($this, 'capitalize'), split('_', $string)));
    }

    function capitalize($string)
    {
        return strtoupper($string[0]).substr($string, 1);
    }

    function &createStorageInstance($options)
    {
        if (is_array($options)) {
            $storage = array_shift($options);
            $options = array_shift($options);
        } else {
            $storage = $options;
            $options = array();
        }

        $class_file = join(
            DIRECTORY_SEPARATOR,
            array(substr(__FILE__, 0, -4), $this->underscore($storage).'.php'));
        require_once($class_file);

        $class = __CLASS__.$this->camelize($storage);
        $instance = new $class($options);
        return $instance;
    }

    function send($key, $message)
    {
        return $this->storage->push($key, $message);
    }

    function underscore($string)
    {
        return preg_replace_callback(
            '/[A-Z]/',
            create_function('$matches', 'return "_".strtolower($matches[0]);'),
            strtolower($string[0]).substr($string, 1));
    }

    /*
     * static methods
     */
    function loadExtension($extension)
    {
        if (extension_loaded($extension)) {
            return true;
        }

        if (defined('PHP_SHLIB_SUFFIX')) {
            $suffix = PHP_SHLIB_SUFFIX;
        } else if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $suffix = 'dll';
        } else {
            $suffix = 'so';
        }

        $prefix = $suffix === 'dll' ? 'php_' : '';
        $libraries = array("{$prefix}{$extension}.{$suffix}");
        $libraries[] = "{$extension}.so";
        $libraries[] = "php_{$extension}.dll";
        $libraries = array_unique($libraries);

        foreach ($libraries as $library) {
            if (@dl($library)) {
                return true;
            }
        }

        return false;
    }
}

class pseudoQueueSingleExecution extends singleExecution
{
    function failed()
    {
        return false;
    }
}

pseudoQueue::loadExtension('pcntl');

if (debug_backtrace()) {
    return;
}

/*
 * sample code
 */
class samplePseudoQueue extends pseudoQueue
{
    // overwrite subscriber method
    function subscriber($key, $message)
    {
        echo "key: {$key}, message: {$message}\n";
        flush();
        @ob_flush();
        sleep(1);
    }
}

$queue = new samplePseudoQueue();
$queue->send('foo', 'bar');
$queue->send('hoge', 'fuga');
