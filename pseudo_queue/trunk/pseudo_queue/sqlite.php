<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

class pseudoQueueSqlite
{
    var $database = null;
    var $options = array(
        'directory' => '/var/tmp/pseudo_queue',
        'file'      => 'pseudo_queue.db',
        'table'     => 'queue',
    );

    function pseudoQueueSqlite($options = array())
    {
        $this->options = array_merge($this->options, $options);

        $directory = $this->options['directory'];
        $file = $this->options['file'];

        $this->mkdir($directory);
        $this->database = sqlite_open(join(
            DIRECTORY_SEPARATOR, array($directory, $file)));

        @sqlite_query(
            $this->database,
            "create table {$this->options['table']} (
                id         integer primary key,
                key        varchar(255),
                message    blog,
                type       varchar(32),
                created_at integer,
                updated_at integer)");
    }

    function insert($table, $values)
    {
        $this->setCommonValues($values);

        $sql = sprintf(
            'insert into %s (%s) values (%s)', $table,
            join(', ', array_keys($values)),
            join(', ', array_fill(0, count($values), "'?'")));
        $sql = $this->replacePlaceHolders($sql, $values);
        return sqlite_query($this->database, $sql);
    }

    function mkdir($directory)
    {
        $parent = dirname($directory);
        if ($parent === $directory) {
            return true;
        }
        $this->mkdir(dirname($directory));
        return @mkdir($directory);
    }

    function push($key, $message)
    {
        return $this->insert($this->options['table'], array(
            'key' => $key, 'message' => $message, 'type' => 'binary'));
    }

    function pop()
    {
        $table = $this->options['table'];

        sqlite_query($this->database, 'begin');

        $sql = "select * from {$table} order by id limit 1";
        $queue = sqlite_array_query($this->database, $sql, SQLITE_ASSOC);
        if (!$queue) {
            sqlite_query($this->database, 'commit');
            return null;
        }
        $queue = $queue[0];

        $sql = "delete from {$table} where id = '?'";
        $sql = $this->replacePlaceHolders($sql, array($queue['id']));
        sqlite_query($this->database, $sql);

        sqlite_query($this->database, 'commit');
        return $queue;
    }

    function replacePlaceHolders($sql, $values)
    {
        $parts = split('\?', $sql);
        $sql = array(array_shift($parts));
        foreach ($parts as $part) {
            $sql[] = sqlite_escape_string(array_shift($values));
            $sql[] = $part;
        }
        return join($sql);
    }

    function &setCommonValues(&$values)
    {
        $now = time();

        if (!isset($values['created_at'])) {
            $values['created_at'] = $now;
        }

        if (!isset($values['updated_at'])) {
            $values['updated_at'] = $now;
        }

        return $values;
    }

    /*
     * static methods
     */
    function loadExtension($extension)
    {
        if (extension_loaded($extension)) {
            return true;
        }

        if (defined('PHP_SHLIB_SUFFIX')) {
            $suffix = PHP_SHLIB_SUFFIX;
        } else if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $suffix = 'dll';
        } else {
            $suffix = 'so';
        }

        $prefix = $suffix === 'dll' ? 'php_' : '';
        $libraries = array("{$prefix}{$extension}.{$suffix}");
        $libraries[] = "{$extension}.so";
        $libraries[] = "php_{$extension}.dll";
        $libraries = array_unique($libraries);

        foreach ($libraries as $library) {
            if (@dl($library)) {
                return true;
            }
        }

        return false;
    }
}

pseudoQueueSqlite::loadExtension('sqlite');
