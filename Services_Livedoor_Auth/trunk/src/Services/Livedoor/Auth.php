<?php
/**
 *  Service_Livedoor_Auth
 *
 *  @require    PHP 5 >= 5.2.0, PECL json:1.2.0-1.2.1
 *  @since      2008.5.19
 *  @license    http://opensource.org/licenses/bsd-license.php  New BSD License
 *  @author     Nob Funaki <nob.funaki@gmail.com>
 *  @see        http://auth.livedoor.com/guide/
 */
class Service_Livedoor_Auth
{
    const LIVEDOOR_AUTH_URL     = 'http://auth.livedoor.com/login/?';
    const LIVEDOOR_XMLRPC_URL   = 'http://auth.livedoor.com/rpc/auth?';

    const LIVEDOOR_AUTH_VERSION = '1.0';
    const LIVEDOOR_PERMS_ID     = 'id';

    protected $_apiKey     = null;
    protected $_secretKey  = null;
    protected $_perms      = null;
    protected $_lastResult = null;

    /**
     * __construct 
     * 
     * @param string $secretKey 
     * @param string $apiKey 
     * @access public
     * @return void
     */
    public function __construct($secretKey, $apiKey)
    {
        $this->_secretKey = $secretKey;
        $this->_apiKey = $apiKey;
    }

    /**
     * getSignInUrl 
     * 
     * @param string $perms 
     * @param string $userdata 
     * @access public
     * @return string
     */
    public function getSignInUrl($perms = self::LIVEDOOR_PERMS_ID, $userdata = '')
    {
        $this->_perms = $perms;
        $params = array(
            'app_key'   => $this->_apiKey,
            'perms'     => $this->_perms,
            't'         => time(),
            'v'         => self::LIVEDOOR_AUTH_VERSION,
            'userdata'  => $userdata,
                );
        $params['sig'] = $this->_makeSignature($params);
        return self::LIVEDOOR_AUTH_URL . http_build_query($params);
    }

    /**
     * validate 
     * 
     * @param string $sig 
     * @param array $returnParams 
     * @access public
     * @return bool
     */
    public function validate($sig, array $returnParams)
    {
        if ($this->_perms == self::LIVEDOOR_PERMS_ID && isset($returnParams['token'])) {
            $params = array(
                        'app_key'   => $this->_apiKey,
                        'token'     => $returnParams['token'],
                        't'         => time(),
                        'v'         => self::LIVEDOOR_AUTH_VERSION,
                    );
            $params['sig'] = $this->_makeSignature($params);

            $header = array(
                        'Content-Type: application/x-www-form-urlencoded',
                        'Content-Length: 0',
                    );
            $context = array(
                        'http' => array(
                                    'method' => 'POST',
                                    'header' => join("\r\n", $header),
                                    'content' => '',
                                ),
                    );
            $this->_lastResult = json_decode(file_get_contents(self::LIVEDOOR_XMLRPC_URL . http_build_query($params), false, stream_context_create($context)));
        }
        return ($sig == $this->_makeSignature($returnParams));
    }

    /**
     * getName 
     * 
     * @access public
     * @return string
     */
    public function getName()
    {
        if (isset($this->_lastResult->user->livedoor_id)) {
            return $this->_lastResult->user->livedoor_id;
        }
        return null;
    }

    /**
     * _makeSignature 
     * 
     * @param array $params 
     * @access protected
     * @return string
     */
    protected function _makeSignature(array $params)
    {
        ksort($params);
        $str = '';
        foreach ($params as $key => $value) {
            if ($key == 'sig') {
                continue;
            }
            $str .= "$key$value";
        }
        return hash_hmac('sha1', $str, $this->_secretKey);
    }
}
