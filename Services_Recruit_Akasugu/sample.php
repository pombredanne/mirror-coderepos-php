<?php
//ini_set("include_path", dirname(__FILE__)."/src/" . PATH_SEPARATOR . ini_get("include_path"));
require_once "Services/Recruit/Akasugu/Factory.php";

// Test
$APIKEY = 'YOUR API KEY HERE!!';
$obj = Services_Recruit_Akasugu_Factory::getInstance(Services_Recruit_Akasugu_Factory::API_MODE_LCAT, $APIKEY);
//var_dump(get_class($obj));
$obj->setFormat(Services_Recruit_Akasugu::FORMAT_JSON);
var_dump($obj->invoke());

$obj = Services_Recruit_Akasugu_Factory::getInstance(Services_Recruit_Akasugu_Factory::API_MODE_MCAT, $APIKEY);
//var_dump(get_class($obj));
$obj->addLargeCategory(2);
$obj->addLargeCategory(4);
$obj->setFormat(Services_Recruit_Akasugu::FORMAT_JSON);
var_dump($obj->invoke());

$obj = Services_Recruit_Akasugu_Factory::getInstance(Services_Recruit_Akasugu_Factory::API_MODE_SCAT, $APIKEY);
//var_dump(get_class($obj));
$obj->addLargeCategory(2);
$obj->addMiddleCategory(201);
$obj->setFormat(Services_Recruit_Akasugu::FORMAT_JSON);
var_dump($obj->invoke());


$obj = Services_Recruit_Akasugu_Factory::getInstance(Services_Recruit_Akasugu_Factory::API_MODE_AGE, $APIKEY);
//var_dump(get_class($obj));
$obj->setFormat(Services_Recruit_Akasugu::FORMAT_JSON);
var_dump($obj->invoke());

$obj = Services_Recruit_Akasugu_Factory::getInstance(Services_Recruit_Akasugu_Factory::API_MODE_ITEM, $APIKEY);
//var_dump(get_class($obj));
$obj->setFormat(Services_Recruit_Akasugu::FORMAT_JSON);
$obj->addMiddleCategory(401);
$obj->addMiddleCategory(402);
$obj->setPriceMax(5000);
$obj->setOrder(Services_Recruit_Akasugu_Item::ORDER_LOWPRICE);
$obj->setCount(20);
var_dump($obj->invoke());

//header('Content-Type: text/xml');
//echo $obj->invoke();