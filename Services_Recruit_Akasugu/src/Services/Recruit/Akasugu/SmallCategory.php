<?php

/**
 * Web api wrapper for akasugu 'SmallCategory'
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Recruit_Akasugu
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.1
 * @link      http://pear.php.net/package/Services_Recruit_Akasugu
 * @see       http://webservice.recruit.co.jp/akasugu/reference.html
 */

require_once 'Services/Recruit/Akasugu/MiddleCategory.php';

/**
 * Web api wrapper class for akasugu 'SmallCategory'
 *
 * @category  Services
 * @package   Services_Recruit_Akasugu
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.php.net/package/Services_Recruit_Akasugu
 * @see       http://webservice.recruit.co.jp/akasugu/reference.html
 */
class Services_Recruit_Akasugu_SmallCategory extends Services_Recruit_Akasugu_MiddleCategory {

    /**
     * the small category codes
     * @var    array
     * @access private
     */
	private $small_code;

    /**
     * constructor
     *
     * @param  string $apikey the apikey string
     * @return void
     * @access public
     */
    public function __construct($apikey) {
        parent::__construct($apikey);
        $this->small_code = array();
    }

    /**
     * return the API url
     *
     * @return string    the API url
     * @access protected
     */
	protected function getApiUrl() {
		return 'http://webservice.recruit.co.jp/akasugu/small_category/v1/';
	}

    /**
     * build the Query-String string
     *
     * @return string   built the Query-String string
     * @access protected
     */
	protected function buildParameters() {
		return $this->buildLargeCategoryParameters() .
		       $this->buildMiddleCategoryParameters() .
		       $this->buildSmallCategoryParameters() .
		       $this->buildKeywordParameters();
	}

    /**
     * build the 'small_code' part of Query-String string
     *
     * @return string    the 'small_code' part of Query-String string
     * @access protected
     */
	protected function buildSmallCategoryParameters() {
		$params = '';
		foreach ($this->getSmallCategory() as $key => $dummy) {
			$params .= '&small_code=' . $key;
		}
		return $params;
	}

    /**
     * replace the small category codes
     *
     * @param  array $code an array of the small category codes
     * @return void
     * @access public
     */
	public function putSmallCategory(array $code) {
		$this->small_code = $code;
	}

    /**
     * add the small category code
     *
     * @param  string $code the small category code
     * @return void
     * @access public
     */
	public function addSmallCategory($code) {
		$this->small_code[$code] = true;
	}

    /**
     * remove the small category code
     *
     * @param  string $code the small category code
     * @return void
     * @access public
     */
	public function removeSmallCategory($code) {
		if (isset($this->small_code[$code])) {
			unset($this->small_code[$code]);
		}
	}

    /**
     * return the array of the small category codes
     *
     * @return array  the array of the small category codes
     * @access public
     */
	public function getSmallCategory() {
		return $this->small_code;
	}
}
