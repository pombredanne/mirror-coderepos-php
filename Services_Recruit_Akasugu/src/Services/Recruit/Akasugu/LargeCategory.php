<?php

/**
 * Web api wrapper for akasugu 'LargeCategory'
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Recruit_Akasugu
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.1
 * @link      http://pear.php.net/package/Services_Recruit_Akasugu
 * @see       http://webservice.recruit.co.jp/akasugu/reference.html
 */

require_once 'Services/Recruit/Akasugu.php';

/**
 * Web api wrapper class for akasugu 'LargeCategory'
 *
 * @category  Services
 * @package   Services_Recruit_Akasugu
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.php.net/package/Services_Recruit_Akasugu
 * @see       http://webservice.recruit.co.jp/akasugu/reference.html
 */
class Services_Recruit_Akasugu_LargeCategory extends Services_Recruit_Akasugu {

    /**
     * constructor
     *
     * @param  string $apikey the apikey string
     * @return void
     * @access public
     */
    public function __construct($apikey) {
        parent::__construct($apikey);
    }

    /**
     * return the API url
     *
     * @return string    the API url
     * @access protected
     */
	protected function getApiUrl() {
		return 'http://webservice.recruit.co.jp/akasugu/large_category/v1/';
	}

    /**
     * build the Query-String string
     *
     * @return string   built the Query-String string
     * @access protected
     */
	protected function buildParameters() {
		$params = $this->buildLargeCategoryParameters();
		return $params;
	}

    /**
     * build the part of Query-String string
     *
     * @return string    the part of Query-String string (empty string)
     * @access protected
     */
	protected function buildLargeCategoryParameters() {
		$params = '';
		return $params;
	}
}
