<?php

/**
 * Web api wrapper for akasugu 'MiddleCategory'
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Recruit_Akasugu
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.1
 * @link      http://pear.php.net/package/Services_Recruit_Akasugu
 * @see       http://webservice.recruit.co.jp/akasugu/reference.html
 */

require_once 'Services/Recruit/Akasugu/LargeCategory.php';

/**
 * Web api wrapper class for akasugu 'MiddleCategory'
 *
 * @category  Services
 * @package   Services_Recruit_Akasugu
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.php.net/package/Services_Recruit_Akasugu
 * @see       http://webservice.recruit.co.jp/akasugu/reference.html
 */
class Services_Recruit_Akasugu_MiddleCategory extends Services_Recruit_Akasugu_LargeCategory {

    /**
     * the large category codes
     * @var    array
     * @access private
     */
	private $large_code;

    /**
     * the middle category codes
     * @var    array
     * @access private
     */
	private $middle_code;

    /**
     * the keyword of the items
     * @var    string
     * @access private
     */
	private $keyword;

    /**
     * constructor
     *
     * @param  string $apikey the apikey string
     * @return void
     * @access public
     */
    public function __construct($apikey) {
        parent::__construct($apikey);
        $this->large_code = array();
        $this->middle_code = array();
        $this->keyword = null;
    }

    /**
     * return the API url
     *
     * @return string    the API url
     * @access protected
     */
	protected function getApiUrl() {
		return 'http://webservice.recruit.co.jp/akasugu/middle_category/v1/';
	}

    /**
     * build the Query-String string
     *
     * @return string   built the Query-String string
     * @access protected
     */
	protected function buildParameters() {
		return $this->buildLargeCategoryParameters() .
		       $this->buildMiddleCategoryParameters() .
		       $this->buildKeywordParameters();
	}

    /**
     * build the 'large_code' part of Query-String string
     *
     * @return string    the 'large_code' part of Query-String string
     * @access protected
     */
	protected function buildLargeCategoryParameters() {
		$params = '';
		foreach ($this->getLargeCategory() as $key => $dummy) {
			$params .= '&large_code=' . $key;
		}
		return $params;
	}

    /**
     * build the 'middle_code' part of Query-String string
     *
     * @return string    the 'middle_code' part of Query-String string
     * @access protected
     */
	protected function buildMiddleCategoryParameters() {
		$params = '';
		foreach ($this->getMiddleCategory() as $key => $dummy) {
			$params .= '&middle_code=' . $key;
		}
		return $params;
	}

    /**
     * build the 'keyword' part of Query-String string
     *
     * @return string    the 'keyword' part of Query-String string
     * @access protected
     */
	protected function buildKeywordParameters() {
		$params = '';
		if (!is_null($this->getKeyword())) {
			$params .= '&keyword=' . $this->getKeyword();
		}
		return $params;
	}

    /**
     * replace the large category codes
     *
     * @param  array $code an array of the large category codes
     * @return void
     * @access public
     */
	public function putLargeCategory(array $code) {
		$this->large_code = $code;
	}

    /**
     * add the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
	public function addLargeCategory($code) {
		$this->large_code[$code] = true;
	}

    /**
     * remove the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
	public function removeLargeCategory($code) {
		if (isset($this->large_code[$code])) {
			unset($this->large_code[$code]);
		}
	}

    /**
     * return the array of the large category codes
     *
     * @return array  the array of the large category codes
     * @access public
     */
	public function getLargeCategory() {
		return $this->large_code;
	}

    /**
     * replace the middle category codes
     *
     * @param  array $code an array of the middle category codes
     * @return void
     * @access public
     */
	public function putMiddleCategory(array $code) {
		$this->middle_code = $code;
	}

    /**
     * add the middle category code
     *
     * @param  string $code the middle category code
     * @return void
     * @access public
     */
	public function addMiddleCategory($code) {
		$this->middle_code[$code] = true;
	}

    /**
     * remove the middle category code
     *
     * @param  string $code the middle category code
     * @return void
     * @access public
     */
	public function removeMiddleCategory($code) {
		if (isset($this->middle_code[$code])) {
			unset($this->middle_code[$code]);
		}
	}

    /**
     * return the array of the middle category codes
     *
     * @return array  the array of the middle category codes
     * @access public
     */
	public function getMiddleCategory() {
		return $this->middle_code;
	}

    /**
     * set the keyword of the items
     *
     * @param  string $keyword the keyword of the items
     * @return void
     * @access public
     */
	public function setKeyword($keyword) {
		$this->keyword = $keyword;
	}

    /**
     * return the keyword of the items
     *
     * @return string the keyword of the items
     * @access public
     */
	public function getKeyword() {
		return $this->keyword;
	}
}
