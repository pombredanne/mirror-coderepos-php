<?php

/**
 * The factory class for Services_Recruit_Akasugu
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Recruit_Akasugu
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.1
 * @link      http://pear.php.net/package/Services_Recruit_Akasugu
 */

/**
 * Short description for class
 *
 * @category  Services
 * @package   Services_Recruit_Akasugu
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.php.net/package/Services_Recruit_Akasugu
 */
class Services_Recruit_Akasugu_Factory {

    /**
     * 'Item' API mode
     * @see http://webservice.recruit.co.jp/akasugu/reference.html
     */
	const API_MODE_ITEM = 'Item';

    /**
     * 'LargeCategory' API mode
     * @see http://webservice.recruit.co.jp/akasugu/reference.html
     */
	const API_MODE_LCAT = 'LargeCategory';

    /**
     * 'MiddleCategory' API mode
     * @see http://webservice.recruit.co.jp/akasugu/reference.html
     */
	const API_MODE_MCAT = 'MiddleCategory';

    /**
     * 'SmallCategory' API mode
     * @see http://webservice.recruit.co.jp/akasugu/reference.html
     */
	const API_MODE_SCAT = 'SmallCategory';

    /**
     * 'Age' API mode
     * @see http://webservice.recruit.co.jp/akasugu/reference.html
     */
	const API_MODE_AGE = 'Age';

    /**
     * constructor
     *
     * @return void
     * @access private
     */
    private function __construct() {
    }

    /**
     * create and return an service instance
     *
     * @param  string    $api    the API mode
     * @param  string    $apikey the apikey string
     * @return mixed     an service instance
     * @access public
     * @throws Exception throws Exception if any errors occur
     * @static
     */
    public static function getInstance($api, $apikey) {
		if (is_null($apikey) || !is_string($apikey) || $apikey === '') {
	        throw new Exception('apikey must be string');
		}

        switch ($api) {
	        case self::API_MODE_ITEM:
	        case self::API_MODE_LCAT:
	        case self::API_MODE_MCAT:
	        case self::API_MODE_SCAT:
	        case self::API_MODE_AGE:
	            require_once 'Services/Recruit/Akasugu/' . $api . '.php';
	            $classname = 'Services_Recruit_Akasugu_' . $api;
	            if (class_exists($classname)) {
	                return new $classname($apikey);
	            } else {
	                throw new Exception('missing the class "' . $classname . '"');
	            }
	            break;
	        default :
	            throw new Exception('Invalid API "' . $api . '"');
        }
	}
}
