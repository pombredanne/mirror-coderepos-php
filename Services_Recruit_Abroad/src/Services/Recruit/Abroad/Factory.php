<?php

/**
 * The factory class for Services_Recruit_Abroad
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Recruit_Abroad
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.1
 * @link      http://pear.php.net/package/Services_Recruit_Abroad
 */

/**
 * The factory class for Services_Recruit_Abroad
 *
 * @category  Services
 * @package   Services_Recruit_Abroad
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.php.net/package/Services_Recruit_Abroad
 */
class Services_Recruit_Abroad_Factory {

    /**
     * 'Tour' API mode
     * @see http://webservice.recruit.co.jp/ab-road/reference.html
     */
	const API_MODE_TOUR = 'Tour';

    /**
     * 'Area' API mode
     * @see http://webservice.recruit.co.jp/ab-road/reference.html
     */
	const API_MODE_AREA = 'Area';

    /**
     * 'Country' API mode
     * @see http://webservice.recruit.co.jp/ab-road/reference.html
     */
	const API_MODE_COUNTRY = 'Country';

    /**
     * 'City' API mode
     * @see http://webservice.recruit.co.jp/ab-road/reference.html
     */
	const API_MODE_CITY = 'City';

    /**
     * 'Hotel' API mode
     * @see http://webservice.recruit.co.jp/ab-road/reference.html
     */
	const API_MODE_HOTEL = 'Hotel';

    /**
     * 'Airline' API mode
     * @see http://webservice.recruit.co.jp/ab-road/reference.html
     */
	const API_MODE_AIRLINE = 'Airline';

    /**
     * 'KODAWARI' API mode
     * @see http://webservice.recruit.co.jp/ab-road/reference.html
     */
	const API_MODE_KODAWARI = 'Kodawari';

    /**
     * constructor
     *
     * @return void
     * @access private
     */
    private function __construct() {
    }

    /**
     * create and return an service instance
     *
     * @param  string    $api    the API mode
     * @param  string    $apikey the apikey string
     * @return mixed     an service instance
     * @access public
     * @throws Exception throws Exception if any errors occur
     * @static
     */
    public static function getInstance($api, $apikey) {
		if (is_null($apikey) || !is_string($apikey) || $apikey === '') {
	        throw new Exception('apikey must be string');
		}

        switch ($api) {
	        case self::API_MODE_TOUR:
	        case self::API_MODE_AREA:
	        case self::API_MODE_COUNTRY:
	        case self::API_MODE_CITY:
	        case self::API_MODE_HOTEL:
	        case self::API_MODE_AIRLINE:
	        case self::API_MODE_KODAWARI:
	            require_once 'Services/Recruit/Abroad/' . $api . '.php';
	            $classname = 'Services_Recruit_Abroad_' . $api;
	            if (class_exists($classname)) {
	                return new $classname($apikey);
	            } else {
	                throw new Exception('missing the class "' . $classname . '"');
	            }
	            break;
	        default :
	            throw new Exception('Invalid API "' . $api . '"');
        }
	}
}
