<?php

/**
 * Web api wrapper for ab-road 'Country'
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Recruit_Abroad
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.1
 * @link      http://pear.php.net/package/Services_Recruit_Abroad
 * @see       http://webservice.recruit.co.jp/ab-road/reference.html
 */

require_once 'Services/Recruit/Abroad.php';

/**
 * Web api wrapper class for ab-road 'Country'
 *
 * @category  Services
 * @package   Services_Recruit_Abroad
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.php.net/package/Services_Recruit_Abroad
 * @see       http://webservice.recruit.co.jp/ab-road/reference.html
 */
class Services_Recruit_Abroad_Country extends Services_Recruit_Abroad {

    /**
     * an area code
     * @var array
     * @access private
     */
    private $area;

    /**
     * a country code
     * @var array
     * @access private
     */
    private $country;

    /**
     * your search keyword
     * @var array
     * @access private
     */
    private $keyword;

    /**
     * if use tours
     * @var int
     * @access private
     */
    private $in_use;

    /**
     * the sort order code
     * @var int
     * @access private
     */
    private $order;

    /**
     * the number of the first row
     * @var int
     * @access private
     */
    private $start;

    /**
     * the row number of fetching data
     * @var int
     * @access private
     */
    private $count;


    /**
     * use no tour
     */
	const IN_USE_NOTUSE = 0;

    /**
     * use all tours
     */
	const IN_USE_USE = 1;

    /**
     * the code order
     */
	const ORDER_CODE = 0;

    /**
     * the lexical order
     */
	const ORDER_NAME = 1;


    /**
     * constructor
     *
     * @param  string $apikey the apikey string
     * @return void
     * @access public
     */
    public function __construct($apikey) {
        parent::__construct($apikey);
        $this->area = array();
        $this->country = array();
        $this->keyword = array();
        $this->in_use = null;
        $this->order = null;
        $this->start = null;
        $this->count = null;
    }

    /**
     * return the API url
     *
     * @return string    the API url
     * @access protected
     */
	protected function getApiUrl() {
		return 'http://webservice.recruit.co.jp/ab-road/country/v1/';
	}

    /**
     * build the Query-String string
     *
     * @return string   built the Query-String string
     * @access protected
     */
    protected function buildParameters() {
        return '' .
               $this->buildAreaParameters() .
               $this->buildCountryParameters() .
               $this->buildKeywordParameters() .
               $this->buildIn_useParameters() .
               $this->buildOrderParameters() .
               $this->buildStartParameters() .
               $this->buildCountParameters() .
               '';
    }


    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildAreaParameters() {
        $params = '';
        foreach ($this->getArea() as $key => $dummy) {
            $params .= '&area=' . $key;
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildCountryParameters() {
        $params = '';
        foreach ($this->getCountry() as $key => $dummy) {
            $params .= '&country=' . $key;
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildKeywordParameters() {
        $params = '';
        foreach ($this->getKeyword() as $key => $dummy) {
            $params .= '&keyword=' . $key;
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildIn_useParameters() {
        $params = '';
        if (!is_null($this->getIn_use())) {
            $params .= '&in_use=' . $this->getIn_use();
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildOrderParameters() {
        $params = '';
        if (!is_null($this->getOrder())) {
            $params .= '&order=' . $this->getOrder();
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildStartParameters() {
        $params = '';
        if (!is_null($this->getStart())) {
            $params .= '&start=' . $this->getStart();
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildCountParameters() {
        $params = '';
        if (!is_null($this->getCount())) {
            $params .= '&count=' . $this->getCount();
        }
        return $params;
    }

    /**
     * replace the large category codes
     *
     * @param  array $code an array of the large category codes
     * @return void
     * @access public
     */
    public function putArea(array $code) {
        $this->area = $code;
    }

    /**
     * add the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function addArea($code) {
        $this->area[$code] = true;
    }

    /**
     * remove the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function removeArea($code) {
        if (isset($this->area[$code])) {
            unset($this->area[$code]);
        }
    }

    /**
     * return an area code
     *
     * @return type an area code
     * @access public
     */
    public function getArea() {
        return $this->area;
    }

    /**
     * replace the large category codes
     *
     * @param  array $code an array of the large category codes
     * @return void
     * @access public
     */
    public function putCountry(array $code) {
        $this->country = $code;
    }

    /**
     * add the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function addCountry($code) {
        $this->country[$code] = true;
    }

    /**
     * remove the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function removeCountry($code) {
        if (isset($this->country[$code])) {
            unset($this->country[$code]);
        }
    }

    /**
     * return a country code
     *
     * @return type a country code
     * @access public
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * replace the large category codes
     *
     * @param  array $code an array of the large category codes
     * @return void
     * @access public
     */
    public function putKeyword(array $code) {
        $this->keyword = $code;
    }

    /**
     * add the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function addKeyword($code) {
        $this->keyword[$code] = true;
    }

    /**
     * remove the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function removeKeyword($code) {
        if (isset($this->keyword[$code])) {
            unset($this->keyword[$code]);
        }
    }

    /**
     * return your search keyword
     *
     * @return type your search keyword
     * @access public
     */
    public function getKeyword() {
        return $this->keyword;
    }

    /**
     * return if use tours
     *
     * @return type if use tours
     * @access public
     */
    public function getIn_use() {
        return $this->in_use;
    }

    /**
     * set if use tours
     *
     * @param  int in_use if use tours
     * @return void
     * @access public
     */
    public function setIn_use($in_use) {
        $in_use = $this->validateDigit($in_use);
		switch ($in_use) {
		case self::IN_USE_NOTUSE:
		case self::IN_USE_USE:
			break;
		default:
			throw new Exception('Invalid in_use code "' . $in_use . '"');
		}
        $this->in_use = $in_use;
    }

    /**
     * return the sort order code
     *
     * @return type the sort order code
     * @access public
     */
    public function getOrder() {
        return $this->order;
    }

    /**
     * set the sort order code
     *
     * @param  int order the sort order code
     * @return void
     * @access public
     */
    public function setOrder($order) {
        $order = $this->validateDigit($order);
		switch ($order) {
		case self::ORDER_CODE:
		case self::ORDER_NAME:
			break;
		default:
			throw new Exception('Invalid order "' . $order . '"');
		}
        $this->order = $order;
    }

    /**
     * return the number of the first row
     *
     * @return type the number of the first row
     * @access public
     */
    public function getStart() {
        return $this->start;
    }

    /**
     * set the number of the first row
     *
     * @param  int start the number of the first row
     * @return void
     * @access public
     */
    public function setStart($start) {
        $start = $this->validateDigit($start);
        $this->start = $start;
    }

    /**
     * return the row number of fetching data
     *
     * @return type the row number of fetching data
     * @access public
     */
    public function getCount() {
        return $this->count;
    }

    /**
     * set the row number of fetching data
     *
     * @param  int count the row number of fetching data
     * @return void
     * @access public
     */
    public function setCount($count) {
        $count = $this->validateDigit($count);
        $this->count = $count;
    }

    /**
     * validate if the given value is digit, and return the int value
     *
     * @param  string    $param a parameter
     * @return int     a int value of the given parameter
     * @access private
     * @throws Exception throws when the given parameter is not digit
     */
    private function validateDigit($param) {
        if (!preg_match('/^\d+$/', $param)) {
            throw new Exception('Invalid format "' . $param . '"');
        }
        return (int)$param;
    }
}
