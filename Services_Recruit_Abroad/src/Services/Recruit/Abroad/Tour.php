<?php

/**
 * Web api wrapper for ab-road 'Tour'
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy
 * the PHP License and are unable to obtain it through the web,
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  Services
 * @package   Services_Recruit_Abroad
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   0.0.1
 * @link      http://pear.php.net/package/Services_Recruit_Abroad
 * @see       http://webservice.recruit.co.jp/ab-road/reference.html
 */

require_once 'Services/Recruit/Abroad.php';

/**
 * Web api wrapper class for ab-road 'Tour'
 *
 * @category  Services
 * @package   Services_Recruit_Abroad
 * @author    Hideyuki Shimooka <shimooka@doyouphp.jp>
 * @copyright 2007 Hideyuki Shimooka
 * @license   http://www.php.net/license/3_01.txt The PHP License, version 3.01
 * @version   Release: @package_version@
 * @link      http://pear.php.net/package/Services_Recruit_Abroad
 * @see       http://webservice.recruit.co.jp/ab-road/reference.html
 */
class Services_Recruit_Abroad_Tour extends Services_Recruit_Abroad {

    /**
     * a tour id
     * @var array
     * @access private
     */
    private $id;

    /**
     * an area code
     * @var array
     * @access private
     */
    private $area;

    /**
     * a country code
     * @var array
     * @access private
     */
    private $country;

    /**
     * a city code
     * @var array
     * @access private
     */
    private $city;

    /**
     * a hotel code
     * @var array
     * @access private
     */
    private $hotel;

    /**
     * your search keyword
     * @var array
     * @access private
     */
    private $keyword;

    /**
     * a place of departure
     * @var array
     * @access private
     */
    private $dept;

    /**
     * YYYYMM of departure
     * @var
     * @access private
     */
    private $ym;

    /**
     * YYYYMMDD of departure
     * @var
     * @access private
     */
    private $ymd;

    /**
     * the minimum price
     * @var
     * @access private
     */
    private $price_min;

    /**
     * the maximum price
     * @var
     * @access private
     */
    private $price_max;

    /**
     * the minimum term
     * @var
     * @access private
     */
    private $term_min;

    /**
     * the maximum term
     * @var
     * @access private
     */
    private $term_max;

    /**
     * a airline name
     * @var array
     * @access private
     */
    private $airline;

    /**
     * a KODAWARI code
     * @var
     * @access private
     */
    private $kodaw;

    /**
     * the sort order code
     * @var
     * @access private
     */
    private $order;

    /**
     * the number of the first row
     * @var
     * @access private
     */
    private $start;

    /**
     * the row number of fetching data
     * @var
     * @access private
     */
    private $count;


    /**
     * use a condition 'free plan'
     */
	const KODAWARI_FREEPLAN = 1;

    /**
     * use a condition 'flight in A.M.'
     */
	const KODAWARI_AM = 2;

    /**
     * use a condition 'flight in P.M.'
     */
	const KODAWARI_PM = 3;

    /**
     * use a condition 'flight in the night'
     */
	const KODAWARI_NIGHT = 4;

    /**
     * use a condition 'with a conductor'
     */
	const KODAWARI_CONDUCTOR = 5;

    /**
     * use a condition 'children discounting'
     */
	const KODAWARI_CHILD = 6;

    /**
     * use a condition 'transfer'
     */
	const KODAWARI_TRANSFER = 7;

    /**
     * use a condition 'assign an airline'
     */
	const KODAWARI_AIRLINE = 8;

    /**
     * use a condition 'assign a hotel'
     */
	const KODAWARI_HOTEL = 9;

    /**
     * use a condition 'early check-in'
     */
	const KODAWARI_CHECKIN = 10;

    /**
     * use a condition 'late check-out'
     */
	const KODAWARI_CHECKOUT = 11;


    /**
     * the newly order
     */
	const ORDER_NEW = 0;

    /**
     * the order of the price
     */
	const ORDER_LOWPRICE = 1;

    /**
     * the reversed order of the price
     */
	const ORDER_HIGHPRICE = 2;

    /**
     * the term order
     */
	const ORDER_SHORTTERM = 3;

    /**
     * the reversed term order
     */
	const ORDER_LONGTERM = 4;


    /**
     * constructor
     *
     * @param  string $apikey the apikey string
     * @return void
     * @access public
     */
    public function __construct($apikey) {
        parent::__construct($apikey);
        $this->id = array();
        $this->area = array();
        $this->country = array();
        $this->city = array();
        $this->hotel = array();
        $this->keyword = array();
        $this->dept = array();
        $this->ym = null;
        $this->ymd = null;
        $this->price_min = null;
        $this->price_max = null;
        $this->term_min = null;
        $this->term_max = null;
        $this->airline = array();
        $this->kodaw = null;
        $this->order = null;
        $this->start = null;
        $this->count = null;
    }

    /**
     * return the API url
     *
     * @return string    the API url
     * @access protected
     */
	protected function getApiUrl() {
		return 'http://webservice.recruit.co.jp/ab-road/tour/v1/';
	}

    /**
     * build the Query-String string
     *
     * @return string   built the Query-String string
     * @access protected
     */
    protected function buildParameters() {
        return '' .
               $this->buildIdParameters() .
               $this->buildAreaParameters() .
               $this->buildCountryParameters() .
               $this->buildCityParameters() .
               $this->buildHotelParameters() .
               $this->buildKeywordParameters() .
               $this->buildDeptParameters() .
               $this->buildYmParameters() .
               $this->buildYmdParameters() .
               $this->buildPrice_minParameters() .
               $this->buildPrice_maxParameters() .
               $this->buildTerm_minParameters() .
               $this->buildTerm_maxParameters() .
               $this->buildAirlineParameters() .
               $this->buildKodawParameters() .
               $this->buildOrderParameters() .
               $this->buildStartParameters() .
               $this->buildCountParameters() .
               '';
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildIdParameters() {
        $params = '';
        foreach ($this->getId() as $key => $dummy) {
            $params .= '&id=' . $key;
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildAreaParameters() {
        $params = '';
        foreach ($this->getArea() as $key => $dummy) {
            $params .= '&area=' . $key;
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildCountryParameters() {
        $params = '';
        foreach ($this->getCountry() as $key => $dummy) {
            $params .= '&country=' . $key;
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildCityParameters() {
        $params = '';
        foreach ($this->getCity() as $key => $dummy) {
            $params .= '&city=' . $key;
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildHotelParameters() {
        $params = '';
        foreach ($this->getHotel() as $key => $dummy) {
            $params .= '&hotel=' . $key;
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildKeywordParameters() {
        $params = '';
        foreach ($this->getKeyword() as $key => $dummy) {
            $params .= '&keyword=' . $key;
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildDeptParameters() {
        $params = '';
        foreach ($this->getDept() as $key => $dummy) {
            $params .= '&dept=' . $key;
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildYmParameters() {
        $params = '';
        if (!is_null($this->getYm())) {
            $params .= '&ym=' . $this->getYm();
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildYmdParameters() {
        $params = '';
        if (!is_null($this->getYmd())) {
            $params .= '&ymd=' . $this->getYmd();
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildPrice_minParameters() {
        $params = '';
        if (!is_null($this->getPrice_min())) {
            $params .= '&price_min=' . $this->getPrice_min();
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildPrice_maxParameters() {
        $params = '';
        if (!is_null($this->getPrice_max())) {
            $params .= '&price_max=' . $this->getPrice_max();
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildTerm_minParameters() {
        $params = '';
        if (!is_null($this->getTerm_min())) {
            $params .= '&term_min=' . $this->getTerm_min();
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildTerm_maxParameters() {
        $params = '';
        if (!is_null($this->getTerm_max())) {
            $params .= '&term_max=' . $this->getTerm_max();
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildAirlineParameters() {
        $params = '';
        foreach ($this->getAirline() as $key => $dummy) {
            $params .= '&airline=' . $key;
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildKodawParameters() {
        $params = '';
        if (!is_null($this->getKodaw())) {
            $params .= '&kodaw=' . $this->getKodaw();
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildOrderParameters() {
        $params = '';
        if (!is_null($this->getOrder())) {
            $params .= '&order=' . $this->getOrder();
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildStartParameters() {
        $params = '';
        if (!is_null($this->getStart())) {
            $params .= '&start=' . $this->getStart();
        }
        return $params;
    }

    /**
     * build the 'count' part of Query-String string
     *
     * @return string    the 'count' part of Query-String string
     * @access protected
     */
    protected function buildCountParameters() {
        $params = '';
        if (!is_null($this->getCount())) {
            $params .= '&count=' . $this->getCount();
        }
        return $params;
    }

    /**
     * replace the large category codes
     *
     * @param  array $code an array of the large category codes
     * @return void
     * @access public
     */
    public function putId(array $code) {
        $this->id = $code;
    }

    /**
     * add the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function addId($code) {
        $this->id[$code] = true;
    }

    /**
     * remove the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function removeId($code) {
        if (isset($this->id[$code])) {
            unset($this->id[$code]);
        }
    }

    /**
     * return a tour id
     *
     * @return type a tour id
     * @access public
     */
    public function getId() {
        return $this->id;
    }

    /**
     * replace the large category codes
     *
     * @param  array $code an array of the large category codes
     * @return void
     * @access public
     */
    public function putArea(array $code) {
        $this->area = $code;
    }

    /**
     * add the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function addArea($code) {
        $this->area[$code] = true;
    }

    /**
     * remove the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function removeArea($code) {
        if (isset($this->area[$code])) {
            unset($this->area[$code]);
        }
    }

    /**
     * return an area code
     *
     * @return type an area code
     * @access public
     */
    public function getArea() {
        return $this->area;
    }

    /**
     * replace the large category codes
     *
     * @param  array $code an array of the large category codes
     * @return void
     * @access public
     */
    public function putCountry(array $code) {
        $this->country = $code;
    }

    /**
     * add the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function addCountry($code) {
        $this->country[$code] = true;
    }

    /**
     * remove the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function removeCountry($code) {
        if (isset($this->country[$code])) {
            unset($this->country[$code]);
        }
    }

    /**
     * return a country code
     *
     * @return type a country code
     * @access public
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * replace the large category codes
     *
     * @param  array $code an array of the large category codes
     * @return void
     * @access public
     */
    public function putCity(array $code) {
        $this->city = $code;
    }

    /**
     * add the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function addCity($code) {
        $this->city[$code] = true;
    }

    /**
     * remove the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function removeCity($code) {
        if (isset($this->city[$code])) {
            unset($this->city[$code]);
        }
    }

    /**
     * return a city code
     *
     * @return type a city code
     * @access public
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * replace the large category codes
     *
     * @param  array $code an array of the large category codes
     * @return void
     * @access public
     */
    public function putHotel(array $code) {
        $this->hotel = $code;
    }

    /**
     * add the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function addHotel($code) {
        $this->hotel[$code] = true;
    }

    /**
     * remove the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function removeHotel($code) {
        if (isset($this->hotel[$code])) {
            unset($this->hotel[$code]);
        }
    }

    /**
     * return a hotel code
     *
     * @return type a hotel code
     * @access public
     */
    public function getHotel() {
        return $this->hotel;
    }

    /**
     * replace the large category codes
     *
     * @param  array $code an array of the large category codes
     * @return void
     * @access public
     */
    public function putKeyword(array $code) {
        $this->keyword = $code;
    }

    /**
     * add the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function addKeyword($code) {
        $this->keyword[$code] = true;
    }

    /**
     * remove the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function removeKeyword($code) {
        if (isset($this->keyword[$code])) {
            unset($this->keyword[$code]);
        }
    }

    /**
     * return your search keyword
     *
     * @return type your search keyword
     * @access public
     */
    public function getKeyword() {
        return $this->keyword;
    }

    /**
     * replace the large category codes
     *
     * @param  array $code an array of the large category codes
     * @return void
     * @access public
     */
    public function putDept(array $code) {
        $this->dept = $code;
    }

    /**
     * add the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function addDept($code) {
        $this->dept[$code] = true;
    }

    /**
     * remove the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function removeDept($code) {
        if (isset($this->dept[$code])) {
            unset($this->dept[$code]);
        }
    }

    /**
     * return a place of departure
     *
     * @return type a place of departure
     * @access public
     */
    public function getDept() {
        return $this->dept;
    }

    /**
     * set YYYYMM of departure
     *
     * @param   ym YYYYMM of departure
     * @return void
     * @access public
     */
    public function setYm($ym) {
        $this->ym = $ym;
    }

    /**
     * return YYYYMM of departure
     *
     * @return type YYYYMM of departure
     * @access public
     */
    public function getYm() {
        return $this->ym;
    }

    /**
     * set YYYYMMDD of departure
     *
     * @param   ymd YYYYMMDD of departure
     * @return void
     * @access public
     */
    public function setYmd($ymd) {
        $this->ymd = $ymd;
    }

    /**
     * return YYYYMMDD of departure
     *
     * @return type YYYYMMDD of departure
     * @access public
     */
    public function getYmd() {
        return $this->ymd;
    }

    /**
     * set the minimum price
     *
     * @param  int price_min the minimum price
     * @return void
     * @access public
     */
    public function setPrice_min($price_min) {
        $price_min = $this->validateDigit($price_min);
        $this->price_min = $price_min;
    }

    /**
     * return the minimum price
     *
     * @return type the minimum price
     * @access public
     */
    public function getPrice_min() {
        return $this->price_min;
    }

    /**
     * set the maximum price
     *
     * @param  int price_max the maximum price
     * @return void
     * @access public
     */
    public function setPrice_max($price_max) {
        $price_max = $this->validateDigit($price_max);
        $this->price_max = $price_max;
    }

    /**
     * return the maximum price
     *
     * @return type the maximum price
     * @access public
     */
    public function getPrice_max() {
        return $this->price_max;
    }

    /**
     * set the minimum term
     *
     * @param  int term_min the minimum term
     * @return void
     * @access public
     */
    public function setTerm_min($term_min) {
        $term_min = $this->validateDigit($term_min);
        $this->term_min = $term_min;
    }

    /**
     * return the minimum term
     *
     * @return type the minimum term
     * @access public
     */
    public function getTerm_min() {
        return $this->term_min;
    }

    /**
     * set the maximum term
     *
     * @param  int term_max the maximum term
     * @return void
     * @access public
     */
    public function setTerm_max($term_max) {
        $term_max = $this->validateDigit($term_max);
        $this->term_max = $term_max;
    }

    /**
     * return the maximum term
     *
     * @return type the maximum term
     * @access public
     */
    public function getTerm_max() {
        return $this->term_max;
    }

    /**
     * replace the large category codes
     *
     * @param  array $code an array of the large category codes
     * @return void
     * @access public
     */
    public function putAirline(array $code) {
        $this->airline = $code;
    }

    /**
     * add the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function addAirline($code) {
        $this->airline[$code] = true;
    }

    /**
     * remove the large category code
     *
     * @param  string $code the large category code
     * @return void
     * @access public
     */
    public function removeAirline($code) {
        if (isset($this->airline[$code])) {
            unset($this->airline[$code]);
        }
    }

    /**
     * return a airline name
     *
     * @return type a airline name
     * @access public
     */
    public function getAirline() {
        return $this->airline;
    }

    /**
     * return a KODAWARI code
     *
     * @return type a KODAWARI code
     * @access public
     */
    public function getKodaw() {
        return $this->kodaw;
    }

    /**
     * set a KODAWARI code
     *
     * @param  int kodaw a KODAWARI code
     * @return void
     * @access public
     */
    public function setKodaw($kodaw) {
        $kodaw = $this->validateDigit($kodaw);
		switch ($kodaw) {
        case KODAWARI_FREEPLAN:
        case KODAWARI_AM:
        case KODAWARI_PM:
        case KODAWARI_NIGHT:
        case KODAWARI_CONDUCTOR:
        case KODAWARI_CHILD:
        case KODAWARI_TRANSFER:
        case KODAWARI_AIRLINE:
        case KODAWARI_HOTEL:
        case KODAWARI_CHECKIN:
        case KODAWARI_CHECKOUT:
			break;
		default:
			throw new Exception('Invalid KODAWARI code "' . $kodaw . '"');
		}
        $this->kodaw = $kodaw;
    }

    /**
     * return the sort order code
     *
     * @return type the sort order code
     * @access public
     */
    public function getOrder() {
        return $this->order;
    }

    /**
     * set the sort order code
     *
     * @param  int order the sort order code
     * @return void
     * @access public
     */
    public function setOrder($order) {
        $order = $this->validateDigit($order);
		switch ($order) {
		case self::ORDER_RECOMMEND:
		case self::ORDER_LOWPRICE:
		case self::ORDER_HIGHPRICE:
		case self::ORDER_NAME:
			break;
		default:
			throw new Exception('Invalid order "' . $order . '"');
		}
        $this->order = $order;
    }

    /**
     * return the number of the first row
     *
     * @return type the number of the first row
     * @access public
     */
    public function getStart() {
        return $this->start;
    }

    /**
     * set the number of the first row
     *
     * @param  int start the number of the first row
     * @return void
     * @access public
     */
    public function setStart($start) {
        $start = $this->validateDigit($start);
        $this->start = $start;
    }

    /**
     * return the row number of fetching data
     *
     * @return type the row number of fetching data
     * @access public
     */
    public function getCount() {
        return $this->count;
    }

    /**
     * set the row number of fetching data
     *
     * @param  int count the row number of fetching data
     * @return void
     * @access public
     */
    public function setCount($count) {
        $count = $this->validateDigit($count);
        $this->count = $count;
    }

    /**
     * validate if the given value is digit, and return the int value
     *
     * @param  string    $param a parameter
     * @return int     a int value of the given parameter
     * @access private
     * @throws Exception throws when the given parameter is not digit
     */
    private function validateDigit($param) {
        if (!preg_match('/^\d+$/', $param)) {
            throw new Exception('Invalid format "' . $param . '"');
        }
        return (int)$param;
    }
}
