<?php
//ini_set("include_path", dirname(__FILE__)."/src/" . PATH_SEPARATOR . ini_get("include_path"));
require_once "Services/Recruit/Abroad/Factory.php";

// Test
$APIKEY = 'YOUR APIKEY HERE!!';
$obj = Services_Recruit_Abroad_Factory::getInstance(Services_Recruit_Abroad_Factory::API_MODE_TOUR, $APIKEY);
$obj->addDept('TYO');
$obj->addKeyword('ヨセミテ');
$obj->setFormat(Services_Recruit_Abroad::FORMAT_JSON);
var_dump($obj->invoke());


$obj = Services_Recruit_Abroad_Factory::getInstance(Services_Recruit_Abroad_Factory::API_MODE_AREA, $APIKEY);
$obj->setFormat(Services_Recruit_Abroad::FORMAT_JSON);
var_dump($obj->invoke());


$obj = Services_Recruit_Abroad_Factory::getInstance(Services_Recruit_Abroad_Factory::API_MODE_COUNTRY, $APIKEY);
$obj->addCountry('US');
$obj->setIn_use(Services_Recruit_Abroad_Country::IN_USE_NOTUSE);
$obj->setFormat(Services_Recruit_Abroad::FORMAT_JSON);
var_dump($obj->invoke());


$obj = Services_Recruit_Abroad_Factory::getInstance(Services_Recruit_Abroad_Factory::API_MODE_CITY, $APIKEY);
$obj->addCountry('US');
$obj->addCity('NYC');
$obj->setIn_use(Services_Recruit_Abroad_Country::IN_USE_NOTUSE);
$obj->setFormat(Services_Recruit_Abroad::FORMAT_JSON);
var_dump($obj->invoke());


$obj = Services_Recruit_Abroad_Factory::getInstance(Services_Recruit_Abroad_Factory::API_MODE_HOTEL, $APIKEY);
$obj->addCountry('US');
$obj->addCity('NYC');
$obj->setIn_use(Services_Recruit_Abroad_Country::IN_USE_NOTUSE);
$obj->setFormat(Services_Recruit_Abroad::FORMAT_JSON);
var_dump($obj->invoke());


$obj = Services_Recruit_Abroad_Factory::getInstance(Services_Recruit_Abroad_Factory::API_MODE_AIRLINE, $APIKEY);
$obj->addKeyword('ベトナム');
$obj->setFormat(Services_Recruit_Abroad::FORMAT_JSON);
var_dump($obj->invoke());


$obj = Services_Recruit_Abroad_Factory::getInstance(Services_Recruit_Abroad_Factory::API_MODE_KODAWARI, $APIKEY);
$obj->setFormat(Services_Recruit_Abroad::FORMAT_JSON);
var_dump($obj->invoke());


