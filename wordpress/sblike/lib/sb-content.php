<?php
require_once(get_template_directory() . '/lib/sb-template.php');
class sbTemplate
{
	var $cms;     // template manager object
	var $entries; // list of shown entries
	/**
		sbTemplate is a class to handle the sb style template.
	*/
	function sbTemplate()
	{
		$this->cms = new sbTemplateManager($this->get_base());
		$this->entries = array();
	}
	/**
		gets context of base template (_base_inc.php)
		@return whole context of base template
	*/
	function get_base()
	{
		return @file_get_contents(get_template_directory() . '/_base.php');
	}
	/**
		parses basic parts and output
	*/
	function finish()
	{
		$this->common();
		$this->title();
		$this->sidebar();
		echo $this->output();
	}
	/**
		output returns the parsed string.
		@return parsed string.
	*/
	function output()
	{
		return $this->cms->output();
	}
	/**
		parses common parts (non-block dependent)
	*/
	function common()
	{
		global $wp_version;
		$this->cms->num(0);
		$this->cms->tag('site_css',get_bloginfo('stylesheet_url'));
		$this->cms->tag('site_rss',get_bloginfo('rss_url'));
		$this->cms->tag('site_atom',get_bloginfo('atom_url'));
		$this->cms->tag('site_encoding',get_bloginfo('charset'));
		$this->cms->tag('site_lang',get_bloginfo('language'));
		$this->cms->tag('site_top',get_settings('home'));
		$this->cms->tag('site_search',get_bloginfo('url'));
		$this->cms->tag('site_cgi',get_bloginfo('url'));
		$this->cms->tag('site_comment',get_option('siteurl') . '/wp-comments-post.php');
		$this->cms->tag('site_parts',get_template_directory() . '/images/');
		$this->cms->tag('script_version',$wp_version);
		$this->cms->tag('script_webpage','http://wordpress.org/');
		$this->cms->tag('script_name','Wordpress');
		$title = wp_title('&raquo;',false);
		if ( !empty($title) )
			$this->cms->tag('site_title',get_bloginfo('name') . '&raquo;' . $title);
		else
			$this->cms->tag('site_title',get_bloginfo('name'));
	}
	/**
		parses title block
	*/
	function title()
	{
		$this->cms->num(0);
		$this->cms->tag('blog_name','<a href="' . get_settings('home') . '">' . get_bloginfo('name') . '</a>');
		$this->cms->tag('blog_name_only',get_bloginfo('name'));
		$this->cms->tag('blog_description',get_bloginfo('description'));
		$this->cms->block('title',1);
	}
	/**
		parses entry block
	*/
	function entry($detail = false)
	{
		if (!$detail) $this->cms->deleteBlock('sequel');
		if (!have_posts()) return;
		$count = 0;
		while (have_posts())
		{
			the_post();
			$this->cms->num($count);
			$permalink = apply_filters('the_permalink', get_permalink());
			$this->cms->tag('entry_permalink',$permalink);
			$this->cms->tag('permalink',$permalink);
			$this->cms->tag('entry_id',get_the_ID());
			$this->cms->tag('entry_title',the_title('', '', false));
			$this->cms->tag('entry_description',get_the_content());
			$this->cms->tag('category_name',get_the_category_list(',', ''));
			$this->cms->tag('user_name',get_the_author());
			$this->cms->tag('entry_disp_time',get_the_time());
			$this->cms->tag('entry_time','<a href="' . $permalink . '">' . get_the_time() . '</a>');
			$this->cms->tag('entry_date',the_date('', '', '', false));
			$this->cms->tag('trackback_auto_discovery',$this->_trackback_rdf());
			array_push($this->entries,array('title'=>the_title('', '', false),'link'=>$permalink));
			$this->_entry_comment($detail);
			if ($detail) $this->_entry_navi();
			$count++;
		}
		$this->cms->block('entry',$count);
		if ($detail)
		{
			$this->cms->block('sequel',1);
			$this->cms->block('option',1);
		}
	}
	/**
		gets comments.  the code is taken from comment-template.php.
		It is just temporary solution, probably we should consider much more for it.
	*/
	function get_comments()
	{
		global $wp_query, $withcomments, $post, $wpdb, $id, $comment, $user_login, $user_ID, $user_identity;
		// TODO: Use API instead of SELECTs.
		if ($user_ID)
		{
			$comments = $wpdb->get_results("SELECT * FROM $wpdb->comments WHERE comment_post_ID = '$post->ID' AND (comment_approved = '1' OR ( user_id = '$user_ID' AND comment_approved = '0' ) )  ORDER BY comment_date");
		}
		else if ( empty($comment_author) )
		{
			$comments = $wpdb->get_results("SELECT * FROM $wpdb->comments WHERE comment_post_ID = '$post->ID' AND comment_approved = '1' ORDER BY comment_date");
		}
		else
		{
			$author_db = $wpdb->escape($comment_author);
			$email_db  = $wpdb->escape($comment_author_email);
			$comments = $wpdb->get_results("SELECT * FROM $wpdb->comments WHERE comment_post_ID = '$post->ID' AND ( comment_approved = '1' OR ( comment_author = '$author_db' AND comment_author_email = '$email_db' AND comment_approved = '0' ) ) ORDER BY comment_date");
		}
		// keep $comments for legacy's sake (remember $table*? ;) )
		$comments = $wp_query->comments = apply_filters( 'comments_array', $comments, $post->ID );
		$wp_query->comment_count = count($wp_query->comments);
		return $comments;
	}
	/**
		parses sidebar parts
	*/
	function sidebar()
	{
		$this->cms->num(0);
		$this->_calendar();
		$this->_linklist();
		$this->_archives();
		$this->_latest_entry();
		$this->_selected_entry();
		$this->_recent_comment();
		$this->_category_list();
		$this->_user_list();
		$this->_page_list();
		$this->_link_page();
	}
	/**
		parses individual page
	*/
	function page()
	{
		if (!have_posts()) return;
		$count = 0;
		while (have_posts())
		{
			the_post();
			$this->cms->num($count);
			$this->cms->tag('page_title',the_title('', '', false));
			$this->cms->tag('page_description',get_the_content());
			$this->cms->tag('page_permalink',apply_filters('the_permalink', get_permalink()));
			$count++;
		}
		$this->cms->block('individual',$count);
	}
	// === private functions
	/**
		handles entry comments & trackbacks
	*/
	function _entry_comment($detail = false)
	{
		global $id;
		// comments
		if (comments_open())
		{
			$permalink = apply_filters('the_permalink', get_permalink());
			$number = get_comments_number($id);
			$this->cms->tag('comment_num','<a href="' . $permalink . '#comments">comments (' . $number . ')</a>');
			$this->cms->tag('comment_count',$number);
			if ($detail)
			{
				$this->cms->block('comment_area',1);
				$comments = $this->get_comments();
				$count = 0;
				foreach ($comments as $comment)
				{
					$this->cms->num($count);
					$this->cms->tag('comment_description',apply_filters('get_comment_text', $comment->comment_content));
					$this->cms->tag('comment_name',get_comment_author_link());
					$this->cms->tag('comment_type',get_comment_type());
					$this->cms->tag('comment_time',get_comment_date() . ' ' . get_comment_time());
					$count++;
				}
				$this->cms->block('comment',$count);
				$this->cms->num(0); // put back to zero
				$this->cms->tag('cookie_name',$comment_author);
				$this->cms->tag('cookie_email',$comment_author_email);
				$this->cms->tag('cookie_url',$comment_author_url);
			} // end of if ($detail)
		}
		else
		{
			$this->cms->tag('comment_num','-');
			$this->cms->tag('comment_count','-');
		}
		// trackbacks - actually nothing to do, just showing trackback url
		$this->cms->tag('trackback_num','-');
		$this->cms->tag('trackback_count','-');
		$this->cms->tag('trackback_url',trackback_url(false));
		if (pings_open() && $detail) $this->cms->block('trackback_area',1);
	}
	/**
		handles entry navigation
	*/
	function _entry_navi()
	{
		$prev = (is_attachment()) ? get_post($GLOBALS['post']->post_parent) : get_previous_post(false, '');
		$next = get_next_post(false, '');
		if ($prev)
		{
			$title = apply_filters('the_title', $prev->post_title, $prev);
			$this->cms->tag('prev_permalink',get_permalink($prev->ID));
			$this->cms->tag('prev_title',$title);
			$this->cms->tag('prev_entry','<a href="' . get_permalink($prev->ID) . '">&lt;&lt; ' . $title . '</a>');
		}
		if ($next)
		{
			$title = apply_filters('the_title', $next->post_title, $next);
			$this->cms->tag('next_permalink',get_permalink($next->ID));
			$this->cms->tag('next_title',$title);
			$this->cms->tag('next_entry','<a href="' . get_permalink($next->ID) . '">' . $title . ' &gt;&gt;</a>');
		}
	}
	/**
		gets rdf description for trackback discovery
	*/
	function _trackback_rdf()
	{
		if (stripos($_SERVER['HTTP_USER_AGENT'], 'W3C_Validator') !== false) return;
		$link = apply_filters('the_permalink', get_permalink());
		$title = str_replace('--', '&#x2d;&#x2d;', wptexturize(strip_tags(get_the_title())));
		$ping = trackback_url(0);
		return <<<_TRACKBACK_AUTO_DISCOVERY_
<!--
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns:trackback="http://madskills.com/public/xml/rss/module/trackback/">
<rdf:Description 
  rdf:about="$link"
  dc:identifier="$link"
  dc:title="$title"
  trackback:ping="$ping" />
</rdf:RDF>
-->
_TRACKBACK_AUTO_DISCOVERY_;
	}
	/**
		gets calendar
	*/
	function _calendar()
	{
		ob_start();
		get_calendar();
		$cal = ob_get_contents();
		ob_end_clean();
		$this->cms->tag('calendar',$cal);
		$this->cms->tag('calendar2',$cal);
		$this->cms->block('calendar',1);
	}
	/**
		gets link list
	*/
	function _linklist()
	{
		$list = '';
		$links = get_categories("type=link&orderby=name&order=ASC&hierarchical=0");
		if ($links)
		{
			foreach ( (array)$links as $link )
			{
				$list .= get_links($cat->cat_ID,'<li>',"</li>","\n",true,'name',false,false,-1,-1,false);
			}
		}
		if (!$list) return;
		$this->cms->tag('link_list','<ul>' . $list . '</ul>');
		$this->cms->block('link',1);
	}
	/**
		gets archive list
	*/
	function _archives()
	{
		if (!have_posts()) return; // do nothing
		ob_start();
		wp_get_archives('type=monthly&show_post_count=true');
		$this->cms->tag('archives_list','<ul>' . ob_get_contents() . '</ul>');
		$this->cms->block('archives',1);
		ob_end_clean();
	}
	/**
		gets latest entry
	*/
	function _latest_entry()
	{
		if (!have_posts()) return;
		ob_start();
		wp_get_archives('type=postbypost&limit=10');
		$this->cms->tag('latest_entry_list','<ul>' . ob_get_contents() . '</ul>');
		$this->cms->block('latest_entry',1);
		ob_end_clean();
	}
	/**
		gets selected entry
	*/
	function _selected_entry()
	{
		if (!have_posts()) return;
		if (count($this->entries) == 0) return;
		$output = '<ul>';
		foreach ($this->entries as $entry)
		{
			$output .= '<li><a href="' . $entry['link'] . '" title="' . $entry['title'] . '">' . $entry['title'] . '</a></li>';
		}
		$this->cms->tag('selected_entry_list',$output . '</ul>');
		$this->cms->block('selected_entry',1);
	}
	/**
		gets recent comment
	*/
	function _recent_comment($limit_num = 10)
	{
		global $wpdb, $tablecomments, $tableposts;
		$limit_length = 20;
		$comments = $wpdb->get_results("SELECT ID, comment_ID, comment_content, comment_author FROM $tableposts, $tablecomments WHERE $tableposts.ID=$tablecomments.comment_post_ID AND post_status = 'publish' AND post_password ='' AND comment_approved = '1' ORDER BY $tablecomments.comment_date DESC LIMIT $limit_num");
		if (!$comments) return;
		$list = '<ul>';
		foreach ( $comments as $comment )
		{
			$author = stripslashes($comment->comment_author);
			$content = stripslashes(strip_tags($comment->comment_content));
			if (mb_strlen($author) > $limit_length)
				$author = mb_substr($author, 0, $limit_length) . '...';
			if (mb_strlen($content) > $limit_length)
				$content = mb_substr($content, 0, $limit_length) . '...';
			$list .= '<li>' . $author . '<br /><a href="' . get_permalink($comment->ID) . '#comments">';
			$list .= $content . '</a></li>';
		}
		$this->cms->tag('recent_comment_list',$list . '</ul>');
		$this->cms->block('recent_comment',1);
	}
	/**
		gets category list
	*/
	function _category_list()
	{
		if (!have_posts()) return;
		ob_start();
		wp_list_categories('show_count=1&title_li=');
		$this->cms->tag('category_list','<ul>' . ob_get_contents() . '</ul>');
		$this->cms->block('category',1);
		ob_end_clean();
	}
	/**
		gets user list
	*/
	function _user_list()
	{
		if (!have_posts()) return;
		ob_start();
		wp_list_authors('exclude_admin=' . false);
		$this->cms->tag('user_list','<ul>' . ob_get_contents() . '</ul>');
		$this->cms->block('profile',1);
		ob_end_clean();
	}
	/**
		gets page list
	*/
	function _page_list()
	{
		$list = wp_list_pages(array(
			'title_li' => '',
			'echo' => false
		));
		if (!$list) return;
		$this->cms->tag('page_list','<ul>' . $list . '</ul>');
		$this->cms->block('page_list',1);
	}
	/**
		gets link page (page block)
	*/
	function _link_page()
	{
		if (!have_posts()) return;
		$link_page = '';
		ob_start();
		wp_link_pages();
		$link_page = ob_get_contents();
		ob_end_clean();
		if (!$link_page)
		$this->cms->tag('link_page',$link_page);
		$this->cms->block('page',1);
	}
} // end of sbTemplate
?>
