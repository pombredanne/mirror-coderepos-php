<?php
if (isset($_POST['account'])) {
	update_option("webclap_account",    $_POST['account']);
	update_option("webclap_default",    $_POST['default']);
	update_option("webclap_enabled",    $_POST['enabled']);
	update_option("webclap_viewtype",   $_POST['viewtype']);
	update_option("webclap_viewformat", stripslashes($_POST['viewformat']));
	echo '<div id="message" class="updated fade"><p><strong>アカウント情報が更新されました</strong></p></div>';
}
?>

<?php
function webclap_selected($name, $answer = false) {
	if( get_option($name) === $answer){
		echo('selected="selected"');
	}
}
function webclap_checked($name) {
	if( get_option($name) == '1'){
		echo('checked="checked"');
	}
}
function webclap_readonly($name) {
	if( get_option($name) != '3'){
		echo('readonly="readonly"');
	}
}
?>
<script type="text/javascript">
    function webclap_onchange_viewtype(ev) {
        var idx  = $('viewtype').getValue();
        var elem = $('viewformat');
        
        if (idx == 3) {
            elem.readOnly = false;
        } else {
            elem.readOnly = false;
            if (idx == 0) elem.update('<?php echo WP_WEBCLAP_CONF_OUT_NORMAL; ?>');
            if (idx == 1) elem.update('<?php echo WP_WEBCLAP_CONF_OUT_TEXT; ?>');
            if (idx == 2) elem.update('<?php echo str_replace("'", "\\'", WP_WEBCLAP_CONF_OUT_JS); ?>');
            elem.readOnly = true;
        }
    }
</script>
<div class="wrap">
	<h2>Web拍手プラグインの設定</h2>
    <form method="post">
        <p class="submit"><input type="submit" name="Submit" value="設定を更新 &raquo;" /></p>
        
        <table class="optiontable">
            <tr valign="top">
                <th scope="row"><label for="account">アカウント名</label></th>
                <td><input id="account" name="account" type="text" value="<?php form_option("webclap_account"); ?>" size="20" /></td>
            </tr> 
            <tr valign="top">
                <th scope="row"><label for="enabled">未指定時、ボタンを表示する</label></th>
                <td><input id="enabled" name="enabled" type="checkbox" value="1" <?php webclap_checked('webclap_enabled');?> /></td>
            </tr> 
            <tr valign="top">
                <th scope="row"><label for="default">デフォルトのボタンテキスト</label></th>
                <td><input id="default" name="default" type="text" value="<?php form_option("webclap_default"); ?>" size="40" /></td>
            </tr> 
            <tr valign="top">
                <th scope="row"><label for="viewtype">表示方法</label></th>
                <td><p><select id="viewtype" name="viewtype" onchange="webclap_onchange_viewtype();">
                    <option value="0" <?php webclap_selected('webclap_viewtype', "0"); ?>>標準</option>
                    <option value="1" <?php webclap_selected('webclap_viewtype', "1"); ?>>テキスト</option>
                    <option value="2" <?php webclap_selected('webclap_viewtype', "2"); ?>>JavaScript使用</option>
                    <option value="3" <?php webclap_selected('webclap_viewtype', "3"); ?>>カスタマイズ</option>
                </select></p>
                
                <p></p>
                <textarea id="viewformat" name="viewformat" rows="5" cols="60" <?php webclap_readonly('webclap_viewtype');?>><?php form_option("webclap_viewformat"); ?></textarea></td>
            </tr> 
        </table>
        
        <p class="submit"><input type="submit" name="Submit" value="設定を更新 &raquo;" /></p>
    </form>
</div>